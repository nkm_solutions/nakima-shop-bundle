<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Repository;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\EntityRepository;

class BrandRepository extends EntityRepository
{


    public function findByProvider($provider)
    {
        return $this->createQueryBuilder('b')
            ->join('\ShopBundle\Entity\Shop', 's', 'WITH', 's.brand = b')
            ->where('s.provider = :prov')->setParameter('prov', $provider)
            ->getQuery()
            ->getResult();
    }

    public function findLoved()
    {
        return $this->createQueryBuilder('b')
            ->where('b.position > 0')
            ->andWhere('b.enabled = true')
            ->orderBy('b.position', 'ASC')
            ->setmaxResults(9)
            ->getQuery()
            ->getResult();
    }

    public function findByProducts($products)
    {

        return $this->createQueryBuilder('b')
            ->join('\ShopBundle\Entity\Product', 'p', 'WITH', 'p.brand = b')
            ->where('p IN(:products)')
            ->setParameter('products', $products)
            ->getQuery()
            ->getResult();
    }

    public function findByCategory($category)
    {

        return $this->createQueryBuilder('b')
            ->join('\ShopBundle\Entity\Product', 'p', 'WITH', 'p.brand = b')
            ->join('p.categories', 'c', 'WITH', 'c = :cat')
            ->setParameter('cat', $category)
            ->getQuery()
            ->getResult();;
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Repository;

/**
 * @author Javier González Cuadrado <xgonzalez@nakima.es>
 */

use Doctrine\ORM\EntityRepository;

class ProductCategoryRepository extends EntityRepository
{


    public function findByProducts($products)
    {

        return $this->createQueryBuilder('c')
            ->join('\ShopBundle\Entity\Product', 'prod')
            ->where('prod IN(:products)')
            ->andWhere('prod.mainCategory=c')
            ->setParameter('products', $products)
            ->getQuery()
            ->getResult();
    }
}

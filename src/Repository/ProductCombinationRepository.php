<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Repository;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\EntityRepository;

class ProductCombinationRepository extends EntityRepository
{

    public function findByAttrbValues($productId, $attrValues = [])
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder
            ->select('pc')
            ->from('ShopBundle\Entity\ProductCombination', 'pc')
            ->leftJoin('pc.productAttributeValues', 'pav')
            ->where('pc.product = :productId')
            ->setParameter('productId', $productId);

        $attrValueExpr = $queryBuilder->expr()->orX();
        foreach ($attrValues as $key => $attrValueId) {
            $label = ":attrValueId_$key";
            $attrValueExpr->add($queryBuilder->expr()->eq('pav.id', $label));
            $queryBuilder->setParameter($label, $attrValueId);
        }
        $queryBuilder->andWhere($attrValueExpr);

        $queryBuilder->groupBy('pc.id');
        $queryBuilder->having('COUNT(pc.id) = :num_attrValues');
        $queryBuilder->setParameter('num_attrValues', count($attrValues));

        $query = $queryBuilder->getQuery();
        $prodCombinations = $query->getResult();

        return $prodCombinations;
    }

}

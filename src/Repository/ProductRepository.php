<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Repository;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{

    public function fetchPriceRange()
    {
        $ret = [];

        $p0 = $this->createQueryBuilder('p')
            ->where('p.sellable = 1')
            ->orderBy('p.price', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();;

        if (!count($p0)) {
            return [0, 1000];
        }

        $p1 = $this->createQueryBuilder('p')
            ->where('p.sellable = 1')
            ->orderBy('p.price', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();;

        return [$p0[0]->getPrice(), $p1[0]->getPrice()];
    }

    public function findSellablesByCategoryId($categoryId, $filters = [], $limit = 12, $offset = 0)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        // Core Query
        $queryBuilder
            ->select('p')
            ->from('ShopBundle\Entity\Product', 'p')
            ->leftJoin('p.categories', 'c')
            ->where('p.sellable = true AND c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        // Filters
        $whereFilter = $queryBuilder->expr()->andX();
        foreach ($filters as $key => $value) {
            $label = ":filter_$key";
            $whereFilter->add($queryBuilder->expr()->eq("p.$key", $label));
            $queryBuilder->setParameter($label, $value);
        }
        $queryBuilder->andWhere($whereFilter);

        // Result
        $queryP = $queryBuilder->getQuery();
        $products = $queryP->getResult();

        // Count for pagination information
        $queryBuilder->select('count(p.id)');
        $queryC = $queryBuilder->getQuery();
        $totalProd = $queryC->getSingleScalarResult();

        return [
            "data" => $products,
            "info" => [
                "limit"  => $limit,
                "offset" => $offset,
                "total"  => $totalProd,
            ],
        ];
    }

    public function countSellablesByBrandId($brandId)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder
            ->select('count(p.id)')
            ->from('ShopBundle\Entity\Product', 'p')
            ->where('p.sellable = true AND p.brand = :brandId')
            ->setParameter('brandId', $brandId);

        $result = $queryBuilder->getQuery()->getSingleScalarResult();

        return $result;
    }

    public function filter(
        $search,
        $categories,
        $brands,
        $attributes,
        $minPrice,
        $maxPrice,
        $order,
        $orderType,
        $featured = false,
        $withDiscounts = false
    ) {

        $map = [];
        foreach ($attributes as $key => $value) {
            $pKey = $value->getProductAttribute()->getName();
            $map[$pKey] = $filters['attributeValues'][$pKey] ?? [];
            $map[$pKey][] = $value;
        }

        $comb = [];
        foreach ($map as $key => $value) {
            $comb[] = $value;
        }
        $query = $this->createQueryBuilder('p')
            ->addSelect('-p.relevanceOrder AS HIDDEN invRelOrder')
            ->where('p.sellable = 1')
            //->andWhere('p.seller IS NOT NULL')
        ;


        if (count($categories)) {
            $query
                ->innerJoin('p.categories', 'c')
                ->andWhere('c IN (:cat)')->setParameter('cat', $categories);
        }

        if (count($brands)) {
            $query->andWhere('p.brand IN (:brands)')->setParameter('brands', $brands);
        }

        if (count($attributes)) {

            $query
                ->innerJoin('p.combinations', 'comb')
                ->innerJoin('comb.productAttributeValues', 'attrv', 'WITH', 'attrv IN (:attributes)')
                ->setParameter('attributes', $attributes);
        }

        if (count($comb)) {
            $attrValueExpr = $query->expr()->orX();
            foreach ($comb as $key => $value) {
                $label = ":attrValueId_$key";
                $attrValueExpr->add($query->expr()->eq('attrv.id', $label));
                $query->setParameter($label, $value);
            }
            $query
                ->andWhere($attrValueExpr)
                ->groupBy('comb.id')
                ->having('COUNT(comb.id) = :num_attrValues')->setParameter('num_attrValues', count($comb));
        }

        $query
            ->andWhere('p.price >= :minPrice')->setParameter('minPrice', $minPrice)
            ->andWhere('p.price <= :maxPrice')->setParameter('maxPrice', $maxPrice);

        if ($featured) {
            $query->andWhere('p.featured = 1');
        }

        if ($withDiscounts) {
            $query->andWhere('p.saleDiscount > 0');
        }

        if ($search) {
            $query->andWhere('MATCH_AGAINST(p.name, p.description, p.summary, :search) > 0')->setParameter(
                'search',
                $search
            );
        }

        if (!$order) {
            $order = "relevanceOrder";
            $orderType = "DESC";
        }
        if ($order === "relevanceOrder") {
            if($orderType === "DESC") {
                $query->orderBy('invRelOrder', 'DESC');
            } else {
                $query->orderBy('p.relevanceOrder', 'DESC');
            }
        } else {
            $query
                ->orderBy("p.$order", $orderType)
                ->addOrderBy('invRelOrder', 'DESC')
            ;
        }

        return $query->getQuery()->getResult();
    }

    public function searchByName($productName, $filters = [], $limit = 12, $offset = 0)
    {

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        // Core Query
        $queryBuilder
            ->select('p')
            ->addSelect('MATCH_AGAINST(p.name, p.description, p.summary, :productName) AS score')
            ->addSelect('-p.relevanceOrder AS HIDDEN invRelOrder')
            ->from('ShopBundle\Entity\Product', "p")
            ->where('p.sellable = true')
            ->andWhere('MATCH_AGAINST(p.name, p.description, p.summary, :productName) > 0')
            ->setParameter('productName', $productName)
            //->setFirstResult($offset * $limit)
            //->setMaxResults($limit)
            ->orderBy('score', 'ASC')
            ->addOrderBy('invRelOrder', 'DESC');

        // Filters
        $whereFilter = $queryBuilder->expr()->andX();
        foreach ($filters as $key => $value) {
            $label = ":filter_$key";
            $whereFilter->add($queryBuilder->expr()->eq("p.$key", $label));
            $queryBuilder->setParameter($label, $value);
        }
        $queryBuilder->andWhere($whereFilter);

        // Result
        $queryP = $queryBuilder->getQuery();
        $products = $queryP->getResult();


        $parsedProducts = [];
        foreach ($products as $product) {
            $parsedProducts[] = $product[0];
        }


        return $parsedProducts;
    }

    public function findByInOffer(
        $filters = [],
        $limit = 16,
        $offset = 0
    ) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        // Core Query
        $queryBuilder
            ->select('p')
            ->from('ShopBundle\Entity\Product', 'p')
            ->where('p.sellable = true AND p.saleDiscount > 0')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        // Filters
        $whereFilter = $queryBuilder->expr()->andX();
        foreach ($filters as $key => $value) {
            $label = ":filter_$key";
            $whereFilter->add($queryBuilder->expr()->eq("p.$key", $label));
            $queryBuilder->setParameter($label, $value);
        }
        $queryBuilder->andWhere($whereFilter);

        // Result
        $queryP = $queryBuilder->getQuery();
        $products = $queryP->getResult();

        // Count for pagination information
        $queryBuilder->select('count(p.id)');
        $queryC = $queryBuilder->getQuery();
        $totalProd = $queryC->getSingleScalarResult();

        return [
            "data" => $products,
            "info" => [
                "limit"  => $limit,
                "offset" => $offset,
                "total"  => $totalProd,
            ],
        ];
    }

}

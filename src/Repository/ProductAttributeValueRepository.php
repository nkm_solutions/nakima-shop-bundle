<?php
declare(strict_types=1);

namespace Nakima\ShopBundle\Repository;

/**
 * @author Javier González Cuadrado <xgonzalez@nakima.es>
 */

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ProductAttributeValueRepository extends EntityRepository
{

    public function findByProduct($product)
    {
        return $this->createQueryBuilder('attrv')
            //->leftJoin('attrv.productAttribute', 'a')
            ->leftJoin('ShopBundle\Entity\Product', 'p', 'WITH', 'p=:product')
            ->leftJoin("p.attributes", "par", "WITH", "par=attrv.productAttribute")
            ->where('p.id=:id')
            ->andWhere("par=attrv.productAttribute")
            ->setParameter('product', $product)
            ->setParameter('id', $product->getId())
            ->getQuery()
            ->getResult();
    }

    public function findByProducts($product)
    {
        return $this->createQueryBuilder('attrv')
            ->join('attrv.productCombinations', 'comb')
            ->join('comb.product', 'prod')
            ->where('prod IN(:products)')
            ->setParameter('products', $product)
            ->getQuery()
            ->getResult();
    }

    public function findByCategory($category)
    {
        return $this->createQueryBuilder('attrv')
            ->join('attrv.productCombinations', 'comb')
            ->join('comb.product', 'prod')
            ->join('prod.categories', 'cat', Expr\Join::WITH, 'cat=:cat')
            ->setParameter('cat', $category)
            ->getQuery()
            ->getResult();

    }

}

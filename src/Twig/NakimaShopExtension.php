<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Twig;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */
class NakimaShopExtension extends \Twig_Extension
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            '_featuredCategories' => new \Twig_SimpleFunction(
                '_featuredCategories', array($this, '_featuredCategories')
            ),
        ];
    }

    public function _featuredCategories()
    {
        $categoryRepo = $this->container->get('doctrine')
            ->getRepository("ShopBundle:ProductCategory");
        $categories = $categoryRepo->findBy(
            ['parent' => null],
            ['position' => 'ASC']
        );

        return $categories;
    }

}

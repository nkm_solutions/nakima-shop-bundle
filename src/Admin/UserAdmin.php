<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;

class UserAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add('password', 'password', ['required' => false])
            ->add('password2', 'password', ['mapped' => false, 'required' => false])
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled');

        return $form;
    }

    public function editFields($form)
    {
        $form
            ->add('password', 'password', ['required' => false])
            ->add('password2', 'password', ['mapped' => false, 'required' => false])
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled');

        return $form;
    }

    public function showFields($form)
    {
        $form
            ->add('username')
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled');

        return $form;
    }

    public function listFields($form)
    {
        return $form
            ->add('username')
            ->add('email');
    }

    public function allow($user, $role, $action, $entity = null)
    {
        $me = Symfony::getUser();

        if ($action == "show") {
            return true;
        }

        if ($action == "delete") {
            return false;
        }

        if ($me != $entity && $entity) {
            return false;
        }

        return true;
    }
}

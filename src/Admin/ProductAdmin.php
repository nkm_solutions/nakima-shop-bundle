<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\ShopBundle\Form\Type\DiscountType;

class ProductAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
            $brands = $this->getRepo("ShopBundle:Brand")->findAll();
        } else {
            if (Symfony::getUser()->grantsRole(['ROLE_SHOP_PROVIDER'])) {
                $brands = $this->getRepo("ShopBundle:Brand")->findByProvider(Symfony::getUser());
            } else {
                $brands = $this->getRepo("ShopBundle:Brand")->findByShopManager(Symfony::getUser());
            }
        }

        $form
            ->add("name")//, null, ['translation_domain' => 'nakima_shop'])
            ->add("description")
            ->add("summary")
            ->add("slug")
            ->add("price")//, null, ['translation_domain' => 'nakima_shop'])
            ->add("enabled")
            ->add("relevanceOrder")
            ->add("categories")
            ->add("mainCategory")
            ->add("brand", null, ['choices' => $brands])
            ->add("combinations")
            ->add("shops")
            ->add("featured")
            ->add("hto")
            ->add("sto")
            ->add("attributes")
            ->add('discount', DiscountType::class);
    }

    public function listFields($form)
    {

        if ($this->getRequest()) {
            $type = $this->getRequest()->get("type", false);

            if ($type) {
                $form
                    ->add("name")
                    ->add("description")
                    ->add("mainCategory")
                    ->add("price")
                    ->add("discount")
                    ->add("relevanceOrder");
            } else {
                $form
                    ->add("name")
                    ->add("description")
                    ->add("summary")
                    ->add("price")
                    ->add("brand")
                    ->add("enabled")
                    ->add("mainCategory")
                    ->add("relevanceOrder");
            }
        } else {
            $form
                ->add("name")
                ->add("description")
                ->add("summary")
                ->add("price")
                ->add("brand")
                ->add("enabled")
                ->add("mainCategory")
                ->add("relevanceOrder");
        }
    }

    public function preCreate($validator)
    {
        $request = $this->getRequest();
        $mediaIds = $request->get('gallery_images', []);
        $mediaRepo = $this->getRepo("MediaBundle:Media");
        $categoryRepo = $this->getRepo("ShopBundle:ProductCategory");
        $subject = $this->getSubject();

        foreach ($mediaIds as $key => $value) {
            $media = $mediaRepo->findOneById($value);
            if ($media) {
                $subject->getGallery()->addMedia($media);
            }
        }

        $categoryIds = $request->get('categories', []);

        foreach ($categoryIds as $key => $value) {
            $category = $categoryRepo->findOneById($value);
            if ($category) {
                $subject->addCategory($category);
            }
        }
    }

    public function preUpdate($validator)
    {

        $request = $this->getRequest();
        $mediaIds = $request->get('gallery_images', []);
        $mediaRepo = $this->getRepo("MediaBundle:Media");
        $categoryRepo = $this->getRepo("ShopBundle:ProductCategory");
        $subject = $this->getSubject();

        foreach ($mediaIds as $key => $value) {
            $media = $mediaRepo->findOneById($value);
            if ($media) {
                $subject->getGallery()->addMedia($media);
            }
        }

        $categoryIds = $request->get('categories', []);

        foreach ($categoryIds as $key => $value) {
            $category = $categoryRepo->findOneById($value);
            if ($category) {
                $subject->addCategory($category);
            }
        }

        $subject->updateSaleDiscount();
    }

    public function getSettings()
    {

        if ($this->action == "create" || $this->action == "edit") {
            $categories = $this->getRepo("ShopBundle:ProductCategory")->findByParent(null);
            $attrValues = $this->getRepo("ShopBundle:ProductAttributeValue")->findAll();

            if ($this->action == "edit") {
                $attrValues = $this->getRepo("ShopBundle:ProductAttributeValue")->findByProduct($this->getSubject());
            }

            return [
                'complete'               => ($this->getSubject() && count($this->getSubject()->getAttributes()) > 0),
                'categories'             => Doctrine::toArray($categories),
                'productAttributeValues' => $attrValues,
            ];
        }

        return [];
    }

    public function allow($user, $role, $action, $entity = null)
    {
        if ($action == "delete") {
            return false;
        }

        if ($this->getRequest()) {
            $type = $this->getRequest()->get("type", false);
            if ($type) {
                return $action == "list";
            }
        }

        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_PROVIDER'])) {
            return true;
        }

        if ($action == "create" || $action == "edit") {
            return false;
        }

        return true;
    }

    public function filter($query, $e)
    {

        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
        } else {
            if (Symfony::getUser()->grantsRole(['ROLE_SHOP_PROVIDER'])) {
                $query
                    ->join("$e.brand", "b")
                    ->where("b.provider = :prov")->setParameter('prov', Symfony::getUser());
            } else {
            }
        }
    }

    /*
    public function allow($user, $role, $action, $subjet=null) {
            if ($action == "list" || $action == "show") {
                return parent::allow($user, $role, $action);
            }

            return false;
        }
    */
}

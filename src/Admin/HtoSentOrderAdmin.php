<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

class HtoSentOrderAdmin extends OrderAdmin
{

    public function getTemplate($key)
    {
        if ($key == "dashboard_group_row") {
            return "NakimaShopBundle:Block:hto_sent_order_dashboard_row.html.twig";
        }

        return parent::getTemplate($key);
    }
}

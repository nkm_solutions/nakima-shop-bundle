<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;

class CustomerAdmin extends UserAdmin
{

    public function createFields($form)
    {
        $form
            ->add('name')
            ->add('surnames')
            ->add('password', 'password', ['required' => false])
            ->add('password2', 'password', ['mapped' => false, 'required' => false])
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled');

        return $form;
    }

    public function editFields($form)
    {
        $form
            ->add('name')
            ->add('surnames')
            ->add('password', 'password', ['required' => false])
            ->add('password2', 'password', ['mapped' => false, 'required' => false])
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled');

        return $form;
    }

    public function showFields($form)
    {
        $form
            ->add('name')
            ->add('surnames')
            ->add('gender')
            ->add('username')
            ->add('email')
            ->add('avatar', Image2Type::class)
            ->add('enabled')
            ->add('createdAt')
            ->add('addresses');

        return $form;
    }

    public function listFields($form)
    {
        return $form
            ->add('username')
            ->add('email');
    }

    public function allow($user, $role, $action, $entity = null)
    {
        $me = Symfony::getUser();

        if ($me->grantsRole(["ROLE_SUPER_ADMIN"])) {
            return true;
        }

        if ($action == "show") {
            return true;
        }

        if ($action == "delete") {
            return false;
        }

        if ($me != $entity && $entity) {
            return false;
        }

        return true;
    }

}

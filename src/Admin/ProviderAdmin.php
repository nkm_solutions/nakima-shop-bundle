<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;


class ProviderAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add('name')
            ->add('surnames')
            ->add('password', 'password', ['required' => false])
            ->add('email')
            ->add('enabled')
            ->add('avatar', Image2Type::class)//
        ;

        $user = Symfony::getUser();

        $b = $user->grantsRole("ROLE_SHOP_ADMIN");
        if ($b) {
            $form->add('brands');
            //$form->add('shops');
        }
    }

    public function editFields($form)
    {
        $this->createFields($form);

    }

    public function listFields($dataMapper)
    {
        $dataMapper
            ->add('name')
            ->add('surnames')
            ->add('email');
    }

    public function allow($user, $role, $action, $entity = null)
    {
        $me = Symfony::getUser();

        if ($action == "show") {
            return true;
        }

        if ($action == "delete") {
            return false;
        }

        if ($me == $entity && $entity) {
            return true;
        }

        return parent::allow($user, $role, $action, $entity);
    }

    public function prePersist($entity)
    {
        foreach ($this->getRequest()->request->all()["form"]["brands"] as $value) {
            $this->getSubject()->addBrand($this->getRepo("ShopBundle:Brand")->findOneById($value));
        }
    }

    public function preUpdate($entity)
    {
        $this->prePersist($entity);
    }

}

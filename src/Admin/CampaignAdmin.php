<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\ShopBundle\Form\Type\PromotionType;

class CampaignAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("name")
            ->add("fromDate", "date", array(
                // info: http://symfony.com/doc/2.0/reference/forms/types/date.html
                'widget' => "single_text",
            ))
            ->add("toDate", "date", array(
                'widget' => "single_text",
            ))
            ->add("enabled");
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

class DeliveredOrderAdmin extends OrderAdmin
{

    public function getTemplate($key)
    {
        if ($key == "dashboard_group_row") {
            return "NakimaShopBundle:Block:delivered_order_dashboard_row.html.twig";
        }

        return parent::getTemplate($key);
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\ShopBundle\Form\Type\ProductAttributeValueType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ProductAttributeAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("name")
            ->add('type')
            ->add(
                'attributeValues',
                CollectionType::class,
                [
                    'entry_type' => ProductAttributeValueType::class,
                    'allow_add' => true,
                ]
            );
    }

    public function editFields($form)
    {
        $form
            ->add("name")
            ->add(
                'attributeValues',
                CollectionType::class,
                [
                    'entry_type' => ProductAttributeValueType::class,
                    'allow_add' => true,
                ]
            );
    }

    public function listFields($form)
    {
        $form
            ->add("name")
            ->add('attributeValues')
            ->add('type');
    }


    public function preCreate($validator)
    {
        $i = 0;
        $attributeValues = $this->getSubject()->getAttributeValues();

        foreach ($attributeValues as $key => $value) {
            if (!($value->getValue() && $value->getAlternativeValue())) {
                $this->getSubject()->removeAttributeValue($value);
                $manager = $this->getContainer()->get('doctrine')->getEntityManager();
                $manager->remove($value);
            } else {
                $value->setProductAttribute($this->getSubject());
                $value->setPosition($i);
                $i++;
            }
        }
    }

    public function preUpdate($validator)
    {
        $i = 0;
        $attributeValues = $this->getSubject()->getAttributeValues();

        foreach ($attributeValues as $key => $value) {
            if (!($value->getValue() && $value->getAlternativeValue())) {
                $this->getSubject()->removeAttributeValue($value);
                $manager = $this->getContainer()->get('doctrine')->getEntityManager();
                $manager->remove($value);
            } else {
                $value->setProductAttribute($this->getSubject());
                $value->setPosition($i);
                $i++;
            }
        }
    }

    public function allow($user, $role, $action, $entity = null)
    {
        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
            return true;
        }

        if ($action == "list" || $action == "show") {
            return parent::allow($user, $role, $action);
        }
    }

}

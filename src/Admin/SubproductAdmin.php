<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\MediaBundle\Form\DataTransformer\FileToMediaTransformer;
use Nakima\MediaBundle\Form\Type\MediaType;

class SubproductAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("name")
            ->add("description")
            ->add("image", MediaType::Class)
            ->add("promotions")
            ->add("subproperties")
            ->add("price")
            ->add("product");

        $form
            ->get('image')
            ->addModelTransformer(
                new FileToMediaTransformer(
                    $this->getSubject(),
                    "logo",
                    "/products",
                    $this->getSubject()->getName(),
                    "image",
                    true,
                    $this->getContainer()->get('nakima.media.provider')
                )
            );
    }

    public function editFields($form)
    {
        $form
            ->add("name")
            ->add("description")
            ->add("image", MediaType::Class)
            ->add("promotions")
            ->add("subproducts")
            ->add("subcategories")
            ->add("properties")
            ->add("subproperties")
            ->add("defaultSubroduct")
            ->add("gallery");
        $form
            ->get('image')
            ->addModelTransformer(
                new FileToMediaTransformer(
                    $this->getSubject(),
                    "logo",
                    "/products",
                    $this->getSubject()->getName(),
                    "image",
                    true,
                    $this->getContainer()->get('nakima.media.provider')
                )
            );
    }

}

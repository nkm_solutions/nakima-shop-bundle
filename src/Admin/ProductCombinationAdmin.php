<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\MediaBundle\Form\DataTransformer\FileToMediaTransformer;
use Nakima\MediaBundle\Form\Type\MediaType;

class ProductCombinationAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add('product')
            ->add('productAttributeValues')
            ->add('priceImpact');

    }

    public function editFields($form)
    {
        $form
            ->add('product')
            ->add('productAttributeValues')
            ->add('priceImpact')
            ->add("logo", MediaType::Class)
            ->add("icon", MediaType::Class);

        $form
            ->get('logo')
            ->addModelTransformer(
                new FileToMediaTransformer(
                    $this->getSubject(),
                    "logo",
                    "/combinations",
                    $this->getSubject()->getId(),
                    "image",
                    true,
                    $this->getContainer()->get('nakima.media.provider')
                )
            );

        $form
            ->get('icon')
            ->addModelTransformer(
                new FileToMediaTransformer(
                    $this->getSubject(),
                    "icon",
                    "/combinations",
                    $this->getSubject()->getId().".icon",
                    "image",
                    true,
                    $this->getContainer()->get('nakima.media.provider')
                )
            );

    }

    public function listFields($form)
    {
        $form
            ->add('product')
            ->add('productAttributeValues')
            ->add('priceImpact')
            ->add("logo", MediaType::Class)
            ->add("icon", MediaType::Class);

    }

    public function preCreate($validator)
    {
        $subject = $this->getSubject();
        $productAttributeValues = $subject->getProductAttributeValues();

        $invProdAttValues = [];
        $invProdAttIds = [];
        foreach ($productAttributeValues as $pAttVal) {
            $pAttId = $pAttVal->getProductAttribute()->getId();
            if (in_array($pAttId, $invProdAttIds)) {
                array_push($invProdAttValues, $pAttVal);
            } else {
                array_push($invProdAttIds, $pAttId);
            }
        }
        foreach ($invProdAttValues as $invProdAttVal) {
            $subject->removeProductAttributeValue($invProdAttVal);
        }
    }

    public function preUpdate($validator)
    {
        $subject = $this->getSubject();
        $productAttributeValues = $subject->getProductAttributeValues();

        $invProdAttValues = [];
        $invProdAttIds = [];
        foreach ($productAttributeValues as $pAttVal) {
            $pAttId = $pAttVal->getProductAttribute()->getId();
            if (in_array($pAttId, $invProdAttIds)) {
                array_push($invProdAttValues, $pAttVal);
            } else {
                array_push($invProdAttIds, $pAttId);
            }
        }
        foreach ($invProdAttValues as $invProdAttVal) {
            $subject->removeProductAttributeValue($invProdAttVal);
        }
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;

class ProductPromotionAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("campaign")
            ->add("discount", \Nakima\ShopBundle\Form\Type\DiscountType::class)
            ->add("product");
    }

    public function listFields($dataMapper)
    {
        $dataMapper
            ->add("campaign")
            ->add("discount")
            ->add("product");
    }

    public function allow($user, $role, $action, $entity = null)
    {
        if ($action == "show") {
            return false;
        }

        return parent::allow($user, $role, $action, $entity);
    }
}

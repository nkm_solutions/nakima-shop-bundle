<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;

class OrderAdmin extends BaseAdmin
{

    public function listFields($form)
    {
        $form
            //->add("id")
            ->add('ref')
            ->add('orderNumber')
            ->add('createdAt')
            ->add('customer')
            ->add('price')
            ->add('lastUpdated')
            ->add('status');
    }

    public function showFields($form)
    {
        $form
            //->add("id")
            ->add('ref')
            ->add('orderNumber')
            ->add('customer')
            ->add('price')
            ->add('createdAt', null, ['label' => 'Realización del pedido'])
            ->add('lastUpdated')
            ->add('deliveryDate')
            ->add('status')
            ->add('orderProducts')
            //->add('trackingNumber')
            ->add('billAddress')
            ->add('shippingAddress')
            ->add('brand')
            ->add('shippingMethod')

            ->add('promotionDiscount')
            ->add('promotionCode')
        ;
    }

    public function editFields($form)
    {
        parent::editFields($form);
    }

    public function getSettings()
    {

        $statuses = Doctrine::getRepo("ShopBundle:OrderStatus")->findAll();
        $hto = $this->getRequest()->get('hto', false);

        if ($hto) {
            return [
                'statuses' => [
                    \ShopBundle\Entity\OrderStatus::load("PENDING"),
                    \ShopBundle\Entity\OrderStatus::load("PREPARING"),
                    \ShopBundle\Entity\OrderStatus::load("IN TRANSIT"),
                    \ShopBundle\Entity\OrderStatus::load("TRYING"),
                    \ShopBundle\Entity\OrderStatus::load("DELIVERED"),
                    \ShopBundle\Entity\OrderStatus::load("RETURNING"),
                    \ShopBundle\Entity\OrderStatus::load("RETURNED"),
                ],
            ];
        } else {
            return [
                'statuses' => [
                    \ShopBundle\Entity\OrderStatus::load("PENDING"),
                    \ShopBundle\Entity\OrderStatus::load("IN TRANSIT"),
                    \ShopBundle\Entity\OrderStatus::load("DELIVERED"),
                ],
            ];
        }
    }

    public function filter($query, $e)
    {
        $status = $this->getRequest()->get('status', false);
        $hto = $this->getRequest()->get('hto', false);

        if ($status) {

            $status = \ShopBundle\Entity\OrderStatus::load(strtoupper($status));

            if ($status) {
                if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
                    $query
                        ->join("$e.brand", "b")
                        ->where("$e.status = :status")->setParameter('status', $status);
                } else {
                    $query
                        ->join("$e.brand", "b")
                        ->join("b.shops", "s")
                        ->where("$e.status = :status")->setParameter('status', $status)
                        ->andWhere("s.provider = :prov")->setParameter('prov', Symfony::getUser());
                }
            }
            $query->andWhere("$e.isHto = :hto")->setParameter('hto', $hto);
        } else {
            $query->where("$e.isHto = :hto")->setParameter('hto', $hto);
        }
    }


    public function allow($user, $role, $action, $subjet = null)
    {

        if ($action == "list" || $action == "show") {
            return parent::allow($user, $role, $action);
        }

        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
            return ($action == "export");
        }

        return false;
    }

}

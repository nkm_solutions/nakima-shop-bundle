<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;

class CartPromotionAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("campaign")
            ->add("discount", \Nakima\ShopBundle\Form\Type\DiscountType::class)
            ->add("code")
            ->add("usages");
    }

    public function listFields($dataMapper)
    {
        $dataMapper
            ->add("campaign")
            ->add("discount")
            ->add("code")
            ->add("usages");
    }

    public function allow($user, $role, $action, $entity = null)
    {
        if ($action == "show") {
            return false;
        }

        return parent::allow($user, $role, $action, $entity);
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;

class ProductCategoryAdmin extends BaseAdmin
{

    public function listFields($form)
    {
        $form
            ->add("name")
            ->add("description", null, ['required' => false])
            ->add("parent");
    }

    public function createFields($form)
    {
        $form
            ->add("name")
            ->add("parent")
            ->add("description", null, ['required' => false])
            ->add("image", Image2Type::class, ['route' => 'nakima_media_category'])
            ->add("smallImage", Image2Type::class, ['route' => 'nakima_media_category']);
    }

    public function editFields($form)
    {
        $form
            ->add("name")
            ->add("parent")
            ->add("description")
            ->add("image", Image2Type::class, ['route' => 'nakima_media_category'])
            ->add("smallImage", Image2Type::class, ['route' => 'nakima_media_category'])
            ->add("position");
    }

    public function allow($user, $role, $action, $subjet = null)
    {

        if ($action == "list" || $action == "show") {
            return parent::allow($user, $role, $action);
        }

        if (Symfony::getUser()->grantsRole(['ROLE_SHOP_ADMIN'])) {
            return true;
        }


        return false;
    }

}

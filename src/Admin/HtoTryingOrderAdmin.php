<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

class HtoTryingOrderAdmin extends OrderAdmin
{

    public function getTemplate($key)
    {
        if ($key == "dashboard_group_row") {
            return "NakimaShopBundle:Block:hto_trying_order_dashboard_row.html.twig";
        }

        return parent::getTemplate($key);
    }
}

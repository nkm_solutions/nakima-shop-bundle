<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

class HtoPendingOrderAdmin extends OrderAdmin
{

    public function getTemplate($key)
    {
        if ($key == "dashboard_group_row") {
            return "NakimaShopBundle:Block:hto_pending_order_dashboard_row.html.twig";
        }

        return parent::getTemplate($key);
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;

class BrandAdmin extends BaseAdmin
{

    public function createFields($form)
    {
        $form
            ->add("name")
            ->add("description")
            ->add("logo", Image2Type::Class)
            ->add("cover", Image2Type::Class)
            ->add("icon", Image2Type::Class, ['label' => "homeLogo"])
            ->add("position")
            ->add("enabled");
    }

    public function listFields($dataMapper)
    {
        $dataMapper
            ->add("position")
            ->add("name")
            ->add("description");
    }

    public function filter($query, $e)
    {
        $user = Symfony::getUser();

        if (!$user->grantsRole("ROLE_SHOP_ADMIN")) {
            $query
                ->join("$e.shops", "s")
                ->where("s.provider = :prov")->setParameter('prov', $user);
        }
    }

    public function preUpdate($validator)
    {
        $entity = $this->getSubject();
        if ($entity->getLogo()) {
            var_dump($entity->getLogo()->getId());
        } else {
            var_dump("NULL");
        }
        if ($entity->getCover()) {
            var_dump($entity->getCover()->getId());
        } else {
            var_dump("NULL");
        }
        if ($entity->getIcon()) {
            var_dump($entity->getIcon()->getId());
        } else {
            var_dump("NULL");
        }

    }

    public function allow($user, $role, $action, $entity = null)
    {
        $user = Symfony::getUser();
        if (!$user->grantsRole("ROLE_SHOP_ADMIN")) {
            if ($action == 'show') {
                foreach ($entity->getShops() as $key => $value) {
                    if ($value->getProvider() == $user) {
                        return true;
                    }
                }

                return false;
            }

            if ($action == 'edit' || $action == 'delete') {
                return false;
            }

            if ($action == "create") {
                return false;
            }
        }

        return parent::allow($user, $role, $action, $entity);
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Admin;

use Nakima\AddressBundle\Form\Type\AddressType;
use Nakima\AdminBundle\Admin\BaseAdmin;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\MediaBundle\Form\Type\Image2Type;

class ShopAdmin extends BaseAdmin
{

    public function createFields($form)
    {

        if (Symfony::getUser()->grantsRole("ROLE_SHOP_ADMIN")) {
            $brands = Doctrine::getRepo("ShopBundle:Brand")->findAll();
            $users = Doctrine::getRepo("ShopBundle:Provider")->findAll();
        } else {
            $brands = Doctrine::getRepo("ShopBundle:Brand")->findByProvider(Symfony::getUser());
            $users = [Symfony::getUser()];
        }

        $form
            ->add('name')
            ->add('description')
            ->add('address', AddressType::class)
            ->add('telephone')
            ->add('schedule')
            ->add('email')
            ->add('web')
            ->add('icon', Image2Type::class)
            ->add('logo', Image2Type::class)
            //->add('shippingMethods')
            ->add('brand', null, ['choices' => $brands])
            //->add('products')
            ->add('enabled')
            //->add('shopManager')
            ->add('provider', null, ['choices' => $users])
            ->add('onlineShop')
            //->add('shippingMethods')
        ;

        $user = Symfony::getUser();

        if ($user->grantsRole("ROLE_SHOP_ADMIN")) {
            $form
                ->add('showeaCode');
        } else {
            $form
                ->add('showeaCode', null, ['disabled' => true]);
        }
    }

    public function editFields($form)
    {
        $this->createFields($form);
    }

    public function listFields($dataMapper)
    {
        $dataMapper
            ->add('name');
    }

    public function filter($query, $e)
    {
        $user = Symfony::getUser();

        if (!$user->grantsRole("ROLE_SHOP_ADMIN")) {
            $query
                ->where("$e.provider = :prov")->setParameter('prov', $user);
        }
    }

    public function allow($user, $role, $action, $entity = null)
    {
        $user = Symfony::getUser();
        if (!$user->grantsRole("ROLE_SHOP_ADMIN")) {
            if ($action == 'edit' || $action == 'show' || $action == 'delete') {
                return $entity->getProvider() == $user;
            }
        }

        return parent::allow($user, $role, $action, $entity);
    }

}

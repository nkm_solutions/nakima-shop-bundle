<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiHTOCartController extends BaseController
{

    /*
     * Route: "nakima_shop_api_htocart"
     * Path: "/htocart"
     */
    public function indexAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST", "GET");

        if ($this->isGET()) {
            return new JsonResponse(
                [
                    'hto_cart' => Doctrine::toArray($user->getHtoCart()),
                ]
            );
        } else {
            if ($this->isPOST()) {
                return new JsonResponse(
                    [
                        'status' => "Not Yet Implemented",
                    ]
                );
            }
        }
    }

    /*
     * Route: "nakima_shop_api_htocart_product_id"
     * Path: "/htocart/product/{id}"
     */
    public function productIdAction(Request $request, $id)
    {
        // Precondition
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $htoOrders = $this->getRepo("ShopBundle:Order")->findBy(
            ['customer' => $user, 'isHto' => true, 'inProgress' => true]
        );
        $this->assertFalse401($htoOrders);

        $prodCombRepo = $this->getRepo("ShopBundle:ProductCombination");
        $productComb = $prodCombRepo->findOneById($id);
        $this->assertTrue400($productComb, 'ProductCombination', 'id', $id);

        $product = $productComb->getProduct();
        $this->assertTrue401($product->getHto());
        $this->assertTrue400($product->getEnabled(), 'enabled');

        $found = false;
        foreach ($product->getShops() as $key => $value) {
            $found = $value->getEnabled();
            if ($found) {
                break;
            }
        }
        $this->assertTrue404($found, 'Shop', 'id', $id);
        // ! Precondition

        $htoCart = $user->getHtoCart();
        $cartProducts = $htoCart->getCartProducts();
        $this->assertTrue400(
            count($cartProducts) < $user->getHtoCartMaxProd(),
            'product'
        );

        $cartProduct = new \ShopBundle\Entity\CartProduct;
        $cartProduct->setProductCombination($productComb);
        $cartProduct->setProduct($product);
        $cartProduct->setQuantity(1);
        $htoCart->addCartProduct($cartProduct);

        Doctrine::flush();

        return new JsonResponse(
            [
                'hto_cart' => Doctrine::toArray($user->getHtoCart()),
                'product' => Doctrine::toArray($cartProduct),
            ]
        );
    }

    /**************************************************************************
     * Inner Functions                                                        *
     **************************************************************************/

    protected function emptyCartAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $manager = $this->getDoctrine()->getManager();

        foreach ($user->getHtoCart()->getCartProducts() as $key => $value) {
            $manager->remove($value);
        }

        $manager->flush();

        return new JsonResponse(
            [
                'hto_cart' => Doctrine::toArray($user->getHtoCart()),
            ]
        );
    }

    protected function getHtoCart()
    {

        $user = $this->checkUser();

        return new JsonResponse(
            [
                'hto_cart' => Doctrine::toArray($user->getHtoCart()),
            ]
        );
    }

}

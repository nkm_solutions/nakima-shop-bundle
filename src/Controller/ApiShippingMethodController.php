<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiShippingMethodController extends BaseController
{

    /*
     * Route: "nakima_shop_api_shippingmethod"
     * Path: "/shippingmethod
     */
    public function indexAction(Request $request)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET");

        $shippingMethodRepo = $this->getRepo("ShopBundle:ShippingMethod");
        $shippingMethods = $shippingMethodRepo->findAll();

        $resp = new JsonResponse(
            [
                'shippingMethods' => Doctrine::toArray($shippingMethods),
            ]
        );

        return $resp;
    }

}

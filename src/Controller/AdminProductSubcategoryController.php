<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AdminProductSubcategoryController extends BaseController
{

    public function createSubcategoryAction(Request $request, $id)
    {

        $this->checkUser('ROLE_SHOP_ADMIN');
        $this->checkMethod('POST');

        $name = $this->getParam("name");

        $category = $this->getRepo('ShopBundle:ProductCategory')->findOneById($id);
        $this->assertTrue404($category, 'Category', 'id', $id);

        $subcategory = new \ShopBundle\Entity\ProductSubcategory();
        $subcategory->setName($name);
        $subcategory->setCategory($category);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($subcategory);
        $em->flush();

        return new JsonResponse(
            [
                'subcategory' => Doctrine::toArray($subcategory),
                'category' => Doctrine::toArray($category),
            ]
        );
    }

    public function deleteSubcategoryAction(Request $request, $id)
    {

        $this->checkUser('ROLE_SHOP_ADMIN');
        $this->checkMethod('DELETE');

        $subcategory = $this->getRepo('ShopBundle:ProductSubcategory')->findOneById($id);
        $category = $subcategory->getCategory();
        $this->assertTrue404($subcategory, 'Subcategory', 'id', $id);

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($subcategory);
        $em->flush();

        return new JsonResponse(
            [
                'category' => Doctrine::toArray($category),
            ]
        );
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Adrià Llaudet < allaudet@nakima.es >
 * @author Robert Tarré < rtarre@nakima.es >
 */

use Nakima\AdminBundle\Controller\BaseAdminController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminProductController extends BaseAdminController
{

    public function editIdAction(Request $request, $entity = null)
    {
        return parent::editIdAction($request, $entity);
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends BaseController
{

    /**************************************************************************
     * SHOP                                                                   *
     **************************************************************************/

    public function postShopAction(Request $request)
    {
        $this->checkRole(['ROLE_SHOP_CREATE_BRAND']);
        $this->checkMethod("POST");

        $name = $this->getParam("name", false);
        $description = $this->getParam("description", false);

        $this->checkMissingParams();

        $shop = new \ShopBundle\Entity\Shop;
        $shop->setName($name);
        $shop->setDescription($description);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($shop);
        $manager->flush();

        return new JsonResponse(
            [
                'shop' => Doctrine::toArray($shop),
            ]
        );
    }

    public function getShopAction(Request $request)
    {
        $user = $this->checkRole('ROLE_SHOP_ADMIN');
        $shops = $this->getRepo("ShopBundle:Shop")->findAll();

        $ret = [];

        foreach ($shops as $key => $value) {
            $ret[] = Doctrine::toArray($value);
        }

        return new JsonResponse(['shops' => $ret]);
    }

    /**************************************************************************
     * BRAND                                                                  *
     **************************************************************************/
    public function postBrandAction(Request $request)
    {
        $this->checkRole(['ROLE_SHOP_CREATE_BRAND']);
        $this->checkMethod("POST");

        $name = $this->getParam("name", false);
        $description = $this->getParam("description", false);

        $this->checkMissingParams();

        $brand = new \ShopBundle\Entity\Brand;
        $brand->setName($name);
        $brand->setDescription($description);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($brand);
        $manager->flush();

        return new JsonResponse(
            [
                'brand' => Doctrine::toArray($brand),
            ]
        );
    }

    public function getBrandAction(Request $request)
    {
        $brands = $this->getRepo("ShopBundle:Brand")->findAll();

        $ret = [];

        foreach ($brands as $key => $value) {
            $ret[] = Doctrine::toArray($value);
        }

        return new JsonResponse(['brands' => $ret]);
    }

    /**************************************************************************
     * GALLERY                                                                *
     **************************************************************************/

    public function uploadMediaAction(Request $request)
    {
        $user = $this->checkRole('ROLE_SHOP_SHOP_ASSISTANT');
        $this->checkMethod("POST");
        $file = $request->files->get('media');

        $userId = $user->getId();
        $hash = Text::rstr(8);
        $extension = $file->guessExtension();
        $filename = "$userId.$hash.$extension";
        $root = Symfony::getRoot();

        $provider = $this->get('nakima.media.provider');
        $media = $provider->buildMedia($file, 'image');
        $media->setPath("/uploads/images");
        $media->setName($filename);
        $media->upload(
            "/web/uploads/images",
            $filename
        );

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($media);
        $manager->flush();

        return new JsonResponse(
            [
                'media' => Doctrine::toArray($media),
            ]
        );
    }

    public function deleteMediaAction(Request $request)
    {
        $user = $this->checkUser("ROLE_SHOP_PROVIDER");
        $this->checkMethod("DELETE");
        $file = $request->files->get('media');

        $id = $this->getParam('id');

        $media = $this->getRepo("MediaBundle:Media")->findOneById($id);
        $this->assertTrue404($media, 'Media', 'id', $id);
        $media->deleteFile();

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($media);
        $manager->flush();

        return new JsonResponse(
            [
                'media' => Doctrine::toArray($media, ["children" => false]),
            ]
        );
    }

    function refreshCombinationAction(Request $request, $id)
    {
        $this->checkRole('ROLE_SHOP_ADMIN');
        $this->checkMethod("POST");

        $c = $this->getRepo("ShopBundle:ProductCombination")->find($id);
        $this->assertTrue404($c, "ProductCombination", 'id', $id);
        $this->optParam("impact", null, null, $c, "setPriceImpact");

        Doctrine::flush();

        return new JsonResponse(
            [
                'productCombination' => Doctrine::toArray($c),
            ]
        );
    }

    function deleteCombinationAction(Request $request, $id)
    {
        $user = $this->checkUser("ROLE_SHOP_PROVIDER");
        $this->checkMethod("POST");

        $prodComb = $this->getRepo("ShopBundle:ProductCombination")->find($id);
        $this->assertTrue404($prodComb, "ProductCombination", 'id', $id);

        if (in_array("ROLE_SHOP_PROVIDER", $user->getRoles())) {
            $provider = $user;
            $providerBrands = $provider->getBrands()->toArray();
            $productBrand = $prodComb->getProduct()->getBrand();
            if (!in_array($productBrand, $providerBrands)) {
                $this->assertTrue401(false);
            }
        }

        Doctrine::remove($prodComb);
        Doctrine::flush();

        return new JsonResponse([]);
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiShopController extends BaseController
{

    /*
     * Route: "nakima_shop_api_shop_sid_product_pid"
     * Path: "/shop/{sId}/product/{pId}"
     */
    public function shopIdProductIdAction(Request $request, $sId, $pId)
    {
        $customer = $this->checkRole("ROLE_SHOP_SHOP_MANAGER");
        $this->checkMethod("POST");

        $shopId = intval($sId);
        $productId = intval($pId);

        $shop = $this->getRepo("ShopBundle:Shop")->findOneById($shopId);
        $this->assertTrue404($shop, 'Shop', 'id', $shopId);

        $product = $this->getRepo("ShopBundle:Product")->findOneById($productId);
        $this->assertTrue404($product, 'Product', 'id', $productId);

        $shopBrandId = $shop->getBrand()->getId();
        $productBrandId = $product->getBrand()->getId();

        if (!$this->hasRole("ROLE_SUPER_ADMIN")) {
            $this->assertTrue400($shopBrandId === $productBrandId, 'Brand');
        }

        //$this->validate($shop);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($shop);
        $manager->flush();

        $resp = new JsonResponse(
            [
                'shop' => Doctrine::toArray($shop),
            ]
        );

        return $resp;

    }

}

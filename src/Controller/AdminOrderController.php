<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\AdminBundle\Controller\BaseAdminController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

class AdminOrderController extends BaseAdminController
{


    public function exportAction(Request $request)
    {

        $status = $request->get('status', 'all');
        $hto = $request->get('hto', "0");


        if ($status == "all") {
            $orders = $this->getRepo("ShopBundle:Order")->findByIsHto($hto);
        } else {
            $orders = $this->getRepo("ShopBundle:Order")->findBy(
                [
                    'isHto' => $hto,
                    'status' => $this->getRepo("ShopBundle:OrderStatus")->findOneByName(strtoupper($status)),
                ]
            );
        }

        $path = tempnam(sys_get_temp_dir(), 'CSV');
        $file = fopen($path, "w");
        $orders = Doctrine::toArray($orders, ['csv' => 1]);

        $f = [
            "Referencia",
            "Nombre del Remitente",
            "Apellido del Remitente",
            "Empresa Remitente",
            "Direccion del Remitente 1",
            "Direccion del Remitente 2",
            "Codigo Postal Origen",
            "Ciudad Origen",
            "Estado Origen",
            "Pais Origen",
            "Telefono del Remitente",
            "Email del Remitente",
            "Nombre del Destinatario",
            "Apellido del Destinatario",
            "Empresa Destinataria",
            "Direccion Envio 1",
            "Direccion Envio 2",
            "Codigo Postal Destino",
            "Ciudad Destino",
            "Estado Destino",
            "Pais Destino",
            "Telefono Destinatario",
            "Email Destinatario",
            "Seguro",
            "Articulo",
            "Valor",
            "Ancho",
            "Largo",
            "Alto",
            "Peso",
        ];

        fputcsv($file, $f);
        foreach ($orders as $line) {
            fputcsv($file, $line);
        }

        fclose($file);

        $response = new BinaryFileResponse($path);
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s"', "order.csv"));

        return $response;

    }
}

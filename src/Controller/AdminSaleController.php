<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\AdminBundle\Controller\BaseAdminController;

class AdminSaleController extends BaseAdminController
{
}

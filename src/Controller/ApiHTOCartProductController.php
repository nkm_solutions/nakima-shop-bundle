<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiHTOCartProductController extends BaseController
{

    /*
     * Route: "nakima_shop_api_htocart_product_id_update"
     * Path: "/htocart/product/{id}/update"
     */
    public function productIdUpdateAction(Request $request, $id)
    {
        // Precondition
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $htoOrders = $this->getRepo("ShopBundle:Order")->findBy(
            ['customer' => $user, 'isHto' => true, 'inProgress' => true]
        );
        $this->assertFalse401($htoOrders);

        $cartProductId = intval($id);
        $cartProduct = false;

        foreach ($user->getHtoCart()->getCartProducts() as $value) {
            if ($cartProductId === $value->getId()) {
                $cartProduct = $value;
                break;
            }
        }

        $this->assertTrue400($cartProduct, 'CartProduct', 'id', $cartProductId);

        $product = $cartProduct->getProduct();
        $this->assertTrue401($product->getHto());
        // ! Precondition

        $qty = $this->optParam("qty", 0);

        $manager = $this->getDoctrine()->getManager();

        if ($qty) {
            $cartProduct->setQuantity(1);
        } else {
            $user->getHtoCart()->removeCartProduct($cartProduct);
            $manager->remove($cartProduct);
        }

        $manager->flush();

        return new JsonResponse(
            [
                'hto_cart' => Doctrine::toArray($user->getHtoCart()),
            ]
        );
    }
}

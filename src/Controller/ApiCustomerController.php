<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiCustomerController extends BaseController
{

    /*
     * Route: "nakima_shop_api_customer"
     * Path: "/customer"
     */
    public function indexAction(Request $request)
    {
        $this->checkMethod("GET", "POST", "PUT");
        if ($this->isGET()) {
            $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");

            return new JsonResponse(
                [
                    'customer' => Doctrine::toArray($customer),
                ]
            );
        }
        else {
            if ($this->isPOST()) {

                return $this->createCustomer();
            }
            else {
                if ($this->isPUT()) {

                    $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
                    $this->assertTrue401($customer->isEnabled());

                    $email = $this->optParam('email', null, null, $customer, 'setEmail');
                    $password = $this->optParam('password', false);
                    $newPassword = $this->optParam('new_password', false);
                    $name = $this->optParam('name', null, null, $customer, 'setName');
                    $surnames = $this->optParam('surnames', null, null, $customer, 'setSurnames');
                    $gender = $this->optParam('gender', false);

                    if ($newPassword) {
                        $this->assertTrue401($customer->checkPassword($password));
                        $this->assertTrue400($newPassword, 'new_password');
                        $customer->setPassword($newPassword);
                    }

                    if ($gender) {
                        $gender = strtoupper($gender);
                        $genderObj = \UserBundle\Entity\Gender::load($gender);
                        $this->assertTrue400($genderObj, 'gender');
                        $customer->setGender($genderObj);
                    }

                    $this->validate($customer);
                    Doctrine::flush();

                    $resp = new JsonResponse(
                        [
                            'customer' => Doctrine::toArray($customer),
                        ]
                    );

                    return $resp;
                }
            }
        }
    }

    /*
     * Route: "nakima_shop_api_customer_address"
     * Path: "/customer/address"
     */
    public function indexAddressAction(Request $request)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET", "POST", "PUT", "DELETE");

        if ($this->isGET()) {
            $ret = Doctrine::toArray($customer->getAddresses());

            return new JsonResponse(
                [
                    'addresses' => $ret,
                ]
            );
        }
        else {
            if ($this->isPOST()) {
                $name = $this->getParam('name');
                $contactName = $this->getParam('contactName');
                $contactNumber = $this->getParam('contactNumber');
                $line = $this->getParam('line');
                $zip = $this->getParam('zip');
                $city = $this->getParam('city');
                $province = $this->getParam('province');

                $address = new \AddressBundle\Entity\Address();
                $address->setName($name);
                $address->setContactName($contactName);
                $address->setContactNumber($contactNumber);
                $address->setLine($line);
                $address->setZip($zip);
                $address->setCity($city);
                $address->setProvince($province);

                $details = $this->optParam('details', false);
                if ($details) {
                    $address->setDetails($details);
                }
                $email = $this->optParam('email', false);
                if ($email) {
                    $address->setEmail($email);
                }

                $this->validate($address);
                $customer->addAddress($address);

                Doctrine::flush();

                $ret = Doctrine::toArray($customer->getAddresses());

                return new JsonResponse(
                    [
                        'addresses' => $ret,
                    ]
                );
            }
            else {
                if ($this->isPUT()) {
                    $addressId = intval($this->getParam('id'));
                    $targetAddress = false;
                    foreach ($customer->getAddresses() as $address) {
                        if ($addressId === $address->getId()) {
                            $targetAddress = $address;
                            break;
                        }
                    }
                    $this->assertTrue401($targetAddress);

                    $name = $this->optParam('name', false);
                    if ($name) {
                        $targetAddress->setName($name);
                    }

                    $contactName = $this->optParam('contactName', false);
                    if ($contactName) {
                        $targetAddress->setContactName($contactName);
                    }
                    $contactNumber = $this->optParam('contactNumber', false);
                    if ($contactNumber) {
                        $targetAddress->setContactNumber($contactNumber);
                    }
                    $line = $this->optParam('line', false);
                    if ($line) {
                        $targetAddress->setLine($line);
                    }
                    $zip = $this->optParam('zip', false);
                    if ($zip) {
                        $targetAddress->setZip($zip);
                    }
                    $city = $this->optParam('city', false);
                    if ($city) {
                        $targetAddress->setCity($city);
                    }
                    $province = $this->optParam('province', false);
                    if ($province) {
                        $targetAddress->setProvince($province);
                    }
                    $details = $this->optParam('details', false);
                    if ($details) {
                        $targetAddress->setDetails($details);
                    }
                    $email = $this->optParam('email', false);
                    if ($email) {
                        $targetAddress->setEmail($email);
                    }

                    $this->validate($targetAddress);
                    Doctrine::flush();

                    $ret = Doctrine::toArray($customer->getAddresses());

                    return new JsonResponse(
                        [
                            'addresses' => $ret,
                        ]
                    );
                }
                else {
                    if ($this->isDELETE()) {
                        $addressId = intval($this->getParam('id'));
                        $found = false;
                        foreach ($customer->getAddresses() as $address) {
                            if ($addressId === $address->getId()) {
                                Doctrine::remove($address);
                                $found = true;
                                break;
                            }
                        }
                        $this->assertTrue401($found);
                        Doctrine::flush();

                        $ret = Doctrine::toArray($customer->getAddresses());

                        return new JsonResponse(
                            [
                                'addresses' => $ret,
                            ]
                        );
                    }
                }
            }
        }
    }

    /**************************************************************************
     * Inner Functions                                                        *
     **************************************************************************/

    protected function createCustomer()
    {

        $email = $this->getParam('email', false);
        $password = $this->getParam('password', false);
        $password2 = $this->getParam('password2', false);
        $gender = strtoupper($this->getParam('gender', false));
        $name = $this->getParam('name', false);
        $surnames = $this->getParam('surnames', false);
        $referrer = $this->optParam('referrer', false);
        $enabled = $this->optParam('__enabled', false);

        $userRepo = $this->getRepo("UserBundle:User");
        $user = $userRepo->findOneByEmail($email);

        $this->addBadParamIfTrue($user, 'email');
        $this->addBadParamIfFalse($password === $password2, 'password2');
        $this->addBadParamIfFalse($password, 'password');

        $userReferrer = false;
        if ($referrer) {
            $userReferrer = $userRepo->findOneByEmail($referrer);
            $this->addBadParamIfFalse($userReferrer, 'referrer');
        }

        $this->checkMissingParams();

        // Check Gender
        //$genderObj = $this->getRepo("UserBundle:Gender")->findOneById($gender);
        $genderObj = \UserBundle\Entity\Gender::load($gender);
        $this->assertTrue400($genderObj, 'gender');

        $user = new \ShopBundle\Entity\Customer;

        $user->setEnabled($enabled);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setName($name);
        $user->setSurnames($surnames);
        $user->setGender($genderObj);

        if (!$enabled) {
            $user->setRecoverPasswordToken(Text::rstr(64));
        }

        $this->validate($user);
        Doctrine::persist($user);
        Doctrine::flush();

        if ($userReferrer) {
            $refRelship = new \ShopBundle\Entity\ReferrerRelationship;
            $refRelship->setReferrer($userReferrer);
            $refRelship->setReferent($user);

            $this->validate($refRelship);
            Doctrine::persist($refRelship);
            Doctrine::flush();
        }

        if (!$enabled) {
            /*$this->get('nakima_user.email.registration_validate')->send(
                $this,
                $user
            );*/
            Mailer::newInstance(
                "Registrado correctamente",
                "no-reply@showea.com",
                $user->getEmail(),
                'NakimaShopBundle:Emails:registration_customer.html.twig',
                [
                    'customer' => $user,
                ]
            );
        }

        /*Mailer::newInstance(
            "Registrado correctamente",
            "no-reply@showea.com",
            $user->getEmail(),
            'NakimaShopBundle:Emails:registration_seller.html.twig',
            [
                'target' => $user
            ]
        );*/

        $admins = Doctrine::getRepo("ShopBundle:Admin")->findAll();

        foreach ($admins as $key => $value) {
            Mailer::newInstance(
                "Nuevo usuario registrado correctamente",
                "no-reply@showea.com",
                $value->getEmail(),
                'NakimaShopBundle:Emails:registration_customer2.html.twig',
                [
                    'target' => $user,
                ]
            );
        }

        Mailer::send();

        $resp = new JsonResponse(
            [
                'customer' => Doctrine::toArray($user),
            ]
        );

        return $resp;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\AdminBundle\Controller\BaseAdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class MenuBlockController extends BaseAdminController
{

    /**
     * @Template()
     */
    public function settingsAction(Request $request)
    {
        return [];
    }

}

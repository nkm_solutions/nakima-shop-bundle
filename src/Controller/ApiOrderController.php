<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\Time\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiOrderController extends BaseController
{

    /*
     * Route: "nakima_shop_api_order_pay"
     * Path: "/order/pay"
     */
    public function payAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $ref = str_replace(".", "", $user->getId() . microtime(true));

        $force = $this->optParam('__force', false);
        $risk = $this->optParam('__risk', "low");
        $status = $this->optParam('__status', "succeed");
        $token = $this->getParam('token');

        $billAddress = $this->loadAddress("bill_");
        $shippingAddress = $this->loadAddress("shipping_");

        $userCart = $user->getCart();
        $this->assertTrue400(count($userCart->getCartProducts()), 'cart');

        $cartPromo = $userCart->getPromotion();
        if ($cartPromo) {

            if (!$cartPromo->isOnTime()) {
                $userCart->setPromotion(null);
                $this->validate($userCart);
                Doctrine::flush();
                $this->assertTrue400(false, 'cartPromotion');
            }

            $auxRepo = $this->getRepo("ShopBundle:Customer_CartPromotion");
            $auxEntity = $auxRepo->findOneBy(
                [
                    'customer'      => $user,
                    'cartPromotion' => $cartPromo,
                ]
            );

            $usagesDone = 0;
            if ($auxEntity) {
                $usagesDone = $auxEntity->getUsages();
            }
            $maxUsages = $cartPromo->getUsages();

            if ($usagesDone >= $maxUsages) {
                $userCart->setPromotion(null);
                $this->validate($userCart);
                Doctrine::flush();
                $this->assertTrue400(false, 'cartPromotion');
            }
        }

        $cartShippingMethod = $userCart->getShippingMethod();
        $choosenShops = [];
        foreach ($userCart->getCartProducts() as $prod) {
            $shops = $prod->getProduct()->getShops();

            $choosenShop = false;
            foreach ($shops as $shop) {
                if ($shop->getEnabled()) {
                    $sSMs = $shop->getShippingMethods();
                    foreach ($sSMs as $sSM) {
                        if ($sSM->getName() === $cartShippingMethod->getName()) {
                            if ($cartShippingMethod->getName() === "Express") {
                                if (strtolower($shippingAddress->getCity()) === strtolower($shop->getAddress()->getCity())) {
                                    $choosenShop = $shop;
                                    break;
                                }
                            }
                            else {
                                $choosenShop = $shop;
                                break;
                            }
                        }
                    }

                    if ($choosenShop) {
                        $choosenShops[] = $choosenShop;
                        break;
                    }
                }
            }
            $this->assertTrue400($choosenShop, 'cartShippingMethod');
        }

        // Si el metodo es Express, todas las tiendas deben ser de barcelona
        // y la dirección de envio tambien
        if ($cartShippingMethod->getName() === "Express") {
            $this->assertTrue400(
                strtolower($shippingAddress->getCity()) === "barcelona",
                'cartShippingMethodCity'
            );
            foreach ($choosenShops as $choosenShop) {
                $this->assertTrue400(
                    strtolower($choosenShop->getAddress()->getCity()) === "barcelona",
                    "choosenShopCity"
                );
            }
        }

        $orders = \ShopBundle\Entity\Order::create(
            $userCart,
            $ref,
            $billAddress,
            $shippingAddress
        );

        $price = 0;
        foreach ($orders as $order) {
            $price += $order->getPrice();
        }

        $x = round($price, 2, PHP_ROUND_HALF_UP);
        $y = round($userCart->getProductsPrice(), 2, PHP_ROUND_HALF_UP);
        $this->assertTrue400($x === $y, 'price');

        if (!$force || $this->isEnvProd()) {

            $stripe = \PaymentBundle\Entity\Stripe::pay(
                $token,
                $user->getEmail(),
                $userCart->getTotalPrice()
            );
            $stripe->setOrderReference($ref);
            $stripe->setBillAddress($billAddress);
            $stripe->setShippingAddress($shippingAddress);

            $stripe->setProductsPrice($userCart->getProductsPrice());
            $stripe->setShippingMethod($userCart->getShippingMethod());
            $stripe->setShippingPrice($userCart->getShippingPrice());
            $stripe->setTotalPrice($userCart->getTotalPrice());

            $stripe->setAmountPaid($userCart->getTotalPrice());
        }
        else {
            $stripe = new \PaymentBundle\Entity\Stripe();
            $stripe->setEmail($user->getEmail());
            $stripe->setCustomer($token);
            $stripe->setRisk($risk);
            $stripe->setCountry("ES");
            $stripe->setLastDigits("4242");
            $stripe->setStatus($status);
            $stripe->setRefunded(false);
            $stripe->setPurchasedAt(new DateTime);
            $stripe->setObservation("test");
            $stripe->setOrderReference($ref);

            $stripe->setProductsPrice($userCart->getProductsPrice());
            $stripe->setShippingMethod($userCart->getShippingMethod());
            $stripe->setShippingPrice($userCart->getShippingPrice());
            $stripe->setTotalPrice($userCart->getTotalPrice());

            $stripe->setAmountPaid($userCart->getTotalPrice());
        }

        foreach ($orders as $order) {
            $order->setStripe($stripe);
            Doctrine::persist($order);
        }
        Doctrine::persist($stripe);

        /*********************************************************************
         * Update the Customer's CartPromotions usage                        *
         *********************************************************************/
        if ($cartPromo) {
            $auxRepo = $this->getRepo("ShopBundle:Customer_CartPromotion");
            $auxEntity = $auxRepo->findOneBy(
                [
                    'customer'      => $user,
                    'cartPromotion' => $cartPromo,
                ]
            );

            if (!$auxEntity) {
                $auxEntity = new \ShopBundle\Entity\Customer_CartPromotion;
                $auxEntity->setCustomer($user);
                $auxEntity->setCartPromotion($cartPromo);
                Doctrine::persist($auxEntity);
            }

            $auxEntity->incUsages(1);
            $this->validate($auxEntity);
        }

        /*********************************************************************
         * Empty the User's Cart                                             *
         *********************************************************************/
        $userCart = $user->getCart();
        foreach ($userCart->getCartProducts() as $cartProduct) {
            Doctrine::remove($cartProduct);
        }
        //Doctrine::remove($userCart);
        //$user->setCart(new \ShopBundle\Entity\Cart);
        //Doctrine::persist($user);

        $userCart->setDefaults();

        /*********************************************************************
         * Time to flush                                                     *
         *********************************************************************/
        Doctrine::flush();

        return new JsonResponse(
            [
                'orders' => Doctrine::toArray($orders),
            ]
        );
    }

    /*
     * Route: "nakima_shop_api_order_try"
     * Path: "/order/try"
     */
    public function tryAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $htoOrders = $this->getRepo("ShopBundle:Order")->findBy(
            ['customer' => $user, 'isHto' => true, 'inProgress' => true]
        );
        $this->assertFalse401($htoOrders);

        $ref = str_replace(".", "", $user->getId() . microtime(true));

        $force = $this->optParam('__force', false);
        $risk = $this->optParam('__risk', "low");
        $status = $this->optParam('__status', "succeed");
        $token = $this->getParam('token');

        $billAddress = $this->loadAddress("bill_");
        $shippingAddress = $this->loadAddress("shipping_");

        $userHtoCart = $user->getHtoCart();
        $this->assertTrue400(count($userHtoCart->getCartProducts()), 'hto_cart');

        $orders = \ShopBundle\Entity\Order::create(
            $userHtoCart,
            $ref,
            $billAddress,
            $shippingAddress,
            true
        );

        $price = 1;

        if (!$force || $this->isEnvProd()) {

            $stripe = \PaymentBundle\Entity\Stripe::pay(
                $token,
                $user->getEmail(),
                $price
            );
            $stripe->setOrderReference($ref);
            $stripe->setBillAddress($billAddress);
            $stripe->setShippingAddress($shippingAddress);

            $stripe->setProductsPrice($userHtoCart->getProductsPrice());
            $stripe->setShippingMethod($userHtoCart->getShippingMethod());
            $stripe->setShippingPrice($userHtoCart->getShippingPrice());
            $stripe->setTotalPrice($userHtoCart->getTotalPrice());

            $stripe->setAmountPaid(1.0);
        }
        else {
            $stripe = new \PaymentBundle\Entity\Stripe();
            $stripe->setEmail($user->getEmail());
            $stripe->setCustomer($token);
            $stripe->setOrderReference($ref);

            $stripe->setProductsPrice($userHtoCart->getProductsPrice());
            $stripe->setShippingMethod($userHtoCart->getShippingMethod());
            $stripe->setShippingPrice($userHtoCart->getShippingPrice());
            $stripe->setTotalPrice($userHtoCart->getTotalPrice());
            $stripe->setStatus(\ShopBundle\Entity\OrderStatus::load("PENDING"));

            $stripe->setAmountPaid(1.0);
        }

        foreach ($orders as $order) {
            $order->setStripe($stripe);
            Doctrine::persist($order);
        }
        Doctrine::persist($stripe);

        /*********************************************************************
         * Empty the User's HTO Cart                                         *
         *********************************************************************/
        $userHtoCart = $user->getHtoCart();
        foreach ($userHtoCart->getCartProducts() as $cartProduct) {
            Doctrine::remove($cartProduct);
        }
        //Doctrine::remove($userHtoCart);
        //$user->setCart(new \ShopBundle\Entity\Cart);
        //Doctrine::persist($user);

        $userHtoCart->setDefaults();

        /*********************************************************************
         * Time to flush                                                     *
         *********************************************************************/
        Doctrine::flush();

        return new JsonResponse(
            [
                'orders' => Doctrine::toArray($orders),
            ]
        );
    }

    /*
     * Route: "nakima_shop_api_order_trypay"
     * Path: "/order/try/{ref}/pay"
     */
    public function tryPayAction(Request $request, $ref)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $orders = $this->getRepo("ShopBundle:Order")->findBy(
            ['ref' => $ref, 'isHto' => true, 'customer' => $user]
        );
        $this->assertTrue400(count($orders), 'ref');

        $keptProducts = [];
        $ordersKeep = [];
        $ordersRet = [];
        $stripe = false;

        foreach ($orders as $order) {
            $order->setStatus(\ShopBundle\Entity\OrderStatus::load("MOVED"));
            $order->setInProgress(false);
            $order->setRedundant(true);

            $stripe = $order->getStripe();

            $htoOrderKeep = new \ShopBundle\Entity\Order;
            $htoOrderRet = new \ShopBundle\Entity\Order;
            $htoOrderKeep->setStatus(\ShopBundle\Entity\OrderStatus::load("DELIVERED"));
            $htoOrderRet->setStatus(\ShopBundle\Entity\OrderStatus::load("RETURNING"));
            $htoOrderKeep->setStripe($stripe);
            $htoOrderRet->setStripe($stripe);
            $htoOrderKeep->setRef($order->getRef());
            $htoOrderRet->setRef($order->getRef());
            $htoOrderKeep->setBillAddress($order->getBillAddress());
            $htoOrderRet->setBillAddress($order->getBillAddress());
            $htoOrderKeep->setShippingAddress($order->getShippingAddress());
            $htoOrderRet->setShippingAddress($order->getShippingAddress());
            $htoOrderKeep->setShippingMethod($order->getShippingMethod());
            $htoOrderRet->setShippingMethod($order->getShippingMethod());
            $htoOrderKeep->setBrand($order->getBrand());
            $htoOrderRet->setBrand($order->getBrand());
            $htoOrderKeep->setIsHto($order->getIsHto());
            $htoOrderRet->setIsHto($order->getIsHto());
            $htoOrderKeep->setInProgress(false);
            $htoOrderKeep->setFinished(true);
            $htoOrderKeep->setDeliveryDate(new DateTime);

            $orderProducts = $order->getOrderProducts();
            foreach ($orderProducts as $orderProd) {
                $clonedOrderProd = $orderProd->clone();

                if ($orderProd->getIsKept()) {
                    $clonedOrderProd->setOrder($htoOrderKeep);
                    $htoOrderKeep->addOrderProduct($clonedOrderProd);
                    $keptProducts[] = $orderProd;
                }
                else {
                    $clonedOrderProd->setOrder($htoOrderRet);
                    $htoOrderRet->addOrderProduct($clonedOrderProd);
                }
            }

            if (count($htoOrderKeep->getOrderProducts()) > 0) {
                $this->validate($htoOrderKeep);
                Doctrine::persist($htoOrderKeep);
                $ordersKeep[] = $htoOrderKeep;
            }
            if (count($htoOrderRet->getOrderProducts()) > 0) {
                $htoOrderRet->setPrice(0);
                $this->validate($htoOrderRet);
                Doctrine::persist($htoOrderRet);
                $ordersRet[] = $htoOrderRet;
            }
        }

        $this->assertTrue400($stripe, 'stripe');

        // Update Stripe Price
        $shippingMethod = $stripe->getShippingMethod();
        $shippingBasePrice = $shippingMethod->getPrice();
        $shippingExtraPrice = $shippingMethod->getBrandExtraPrice();

        $productsPrice = 0.0;
        $brandMapping = [];
        foreach ($keptProducts as $productKept) {
            // Calculate shippingPrice
            $brandId = $productKept->getBrand()->getId();
            if (!array_key_exists($brandId, $brandMapping)) {
                $brandMapping[$brandId] = [];
            }
            $brandMapping[$brandId][] = $productKept->getId();

            // Update Prices
            $quantity = $productKept->getQuantity();
            $finalPrice = $productKept->getFinalPrice();

            $productsPrice += $finalPrice * $quantity;
        }
        $extraBrands = count($brandMapping) * $shippingExtraPrice;
        $shippingPrice = $shippingBasePrice + $extraBrands;

        if ($productsPrice > 69.0) {
            $shippingPrice = 0.0;
        }
        $totalPrice = $productsPrice + $shippingPrice;

        $amount = $totalPrice - $stripe->getAmountPaid();

        $force = $this->optParam('__force', false);
        $risk = $this->optParam('__risk', "low");
        $status = $this->optParam('__status', "succeed");

        if (!$force || $this->isEnvProd()) {
            $DUMMY = new \PaymentBundle\Entity\Stripe();
            $stripe->_pay($amount);

            $stripe->setProductsPrice($productsPrice);
            $stripe->setShippingPrice($shippingPrice);
            $stripe->setTotalPrice($totalPrice);

            $stripe->incAmountPaid($amount);
        }
        else {
            $stripe = new \PaymentBundle\Entity\Stripe();
            $stripe->setEmail($user->getEmail());
            $stripe->setCustomer($token);
            $stripe->setStatus(\ShopBundle\Entity\OrderStatus::load("DELIVERED"));
            $stripe->setRisk("low");
            $stripe->setCountry("ES");
            $stripe->setLastDigits("4242");
            $stripe->setRefunded(false);
            $stripe->setPurchasedAt(new DateTime);
            $stripe->setObservation("test");
            $stripe->setOrderReference($ref);

            $stripe->setShippingMethod($shippingMethod);
            $stripe->setProductsPrice($productsPrice);
            $stripe->setShippingPrice($shippingPrice);
            $stripe->setTotalPrice($totalPrice);

            $stripe->setAmountPaid($totalPrice);
        }

        /*********************************************************************
         * Time to flush                                                     *
         *********************************************************************/
        Doctrine::flush();

        return new JsonResponse(
            [
                'payOrders' => Doctrine::toArray($ordersKeep),
                'retOrders' => Doctrine::toArray($ordersRet),
            ]
        );
    }

    /*
     * Route: "nakima_shop_api_order_status_update"
     * Path: "/order/{oId}/status_update"
     */
    public function statusUpdateAction(Request $request, $oId)
    {
        $user = $this->checkRole("ROLE_SHOP_ADMIN");
        $this->checkMethod("POST");

        $order = $this->getRepo('ShopBundle:Order')->find($oId);
        $this->assertTrue404($order, 'Order', 'id', $oId);

        $statusId = $this->getParam('status_id');
        $status = $this->getRepo('ShopBundle:OrderStatus')->find($statusId);
        $this->assertTrue404($status, 'OrderStatus', 'id', $statusId);

        $oldStatus = $order->getStatus();
        $newStatusName = $status->getName();

        $this->assertFalse400($newStatusName === "MOVED", 'OrderStatus');
        $this->assertFalse400($newStatusName === "CANCELED", 'OrderStatus');

        if (!$order->getIsHto()) {
            $this->assertFalse400($newStatusName === "PREPARING", 'OrderStatus');
            $this->assertFalse400($newStatusName === "TRYING", 'OrderStatus');
            $this->assertFalse400($newStatusName === "RETURNING", 'OrderStatus');
            $this->assertFalse400($newStatusName === "RETURNED", 'OrderStatus');

            if ($newStatusName === "DELIVERED") {
                $order->setDeliveryDate(new DateTime);
                $order->setInProgress(false);
                $order->setFinished(true);
            }
        }
        else {
            if ($newStatusName === "TRYING") {
                $order->setDeliveryDate(new DateTime);
            }
            if ($newStatusName === "RETURNED") {
                $order->setDeliveryDate(new DateTime);
                $order->setInProgress(false);
                $order->setFinished(true);
            }
            if ($newStatusName === "DELIVERED") {
                $order->setInProgress(false);
                $order->setFinished(true);
            }
        }

        if ($newStatusName === "CANCELED") {
            $order->setInProgress(false);
            $order->setFinished(true);
        }

        $order->setStatus($status);

        $this->validate($order);
        Doctrine::flush();

        return new JsonResponse(
            [
                'order'  => Doctrine::toArray($order),
                'status' => Doctrine::toArray($status),
            ]
        );
        //$order = \ShopBundle\Entity\Order::create($user->getCart());
    }

    /*
     * Route: "nakima_shop_api_order_trackingNumber_update"
     * Path: "/order/{oId}/trackingNumber_update"
     */
    public function trackingNumberUpdateAction(Request $request, $oId)
    {
        $user = $this->checkRole("ROLE_SHOP_ADMIN");
        $this->checkMethod("POST");

        $order = $this->getRepo('ShopBundle:Order')->find($oId);

        $this->assertTrue404($order, 'Order', 'id', $oId);

        $trackingNumber = $this->getParam('trackingNumber');

        $order->setTrackingNumber($trackingNumber);

        $this->validate($order);
        Doctrine::flush();

        return new JsonResponse(
            [
                'order'          => Doctrine::toArray($order),
                'trackingNumber' => $trackingNumber,
            ]
        );
        //$order = \ShopBundle\Entity\Order::create($user->getCart());
    }

    /**************************************************************************
     * Inner Functions                                                        *
     **************************************************************************/

    private function loadAddress($prefix)
    {
        $address = new \AddressBundle\Entity\Address;
        $this->getParam($prefix . "name", false, null, $address, 'setName');
        $this->getParam($prefix . "contactname", false, null, $address, 'setContactName');
        $this->getParam($prefix . "contactnumber", false, null, $address, 'setContactNumber');
        $this->getParam($prefix . "email", false, null, $address, 'setEmail');
        $this->getParam($prefix . "line", false, null, $address, 'setLine');
        $this->getParam($prefix . "zip", false, null, $address, 'setZip');
        $this->getParam($prefix . "city", false, null, $address, 'setCity');
        $this->getParam($prefix . "province", false, null, $address, 'setProvince');
        $this->optParam($prefix . "details", "", null, $address, 'setDetails');

        $this->checkMissingParams();

        return $address;
    }

}

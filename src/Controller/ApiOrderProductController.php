<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiOrderProductController extends BaseController
{

    /*
     * Route: "nakima_shop_api_orderproduct_id_keep"
     * Path: "/orderproduct/{id}/keep"
     */
    public function keepAction(Request $request, $id)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("PUT");

        $orderProdRepo = $this->getRepo('ShopBundle:OrderProduct');
        $orderProd = $orderProdRepo->findOneById($id);
        $this->assertTrue404($orderProd, 'OrderProduct', 'id', $id);

        $orderProd->setIsKept(!$orderProd->getIsKept());

        $this->validate($orderProd);
        Doctrine::flush();

        $stripe = $orderProd->getOrder()->getStripe();
        $orders = $stripe->getOrders();

        $keptProducts = [];
        foreach ($orders as $order) {
            $orderProducts = $order->getOrderProducts();
            foreach ($orderProducts as $orderProd) {
                if ($orderProd->getIsKept()) {
                    $keptProducts[] = $orderProd;
                }
            }
        }

        // Update Stripe Price
        $shippingMethod = $stripe->getShippingMethod();
        $shippingBasePrice = $shippingMethod->getPrice();
        $shippingExtraPrice = $shippingMethod->getBrandExtraPrice();

        $productsPrice = 0.0;
        $brandMapping = [];
        foreach ($keptProducts as $productKept) {
            // Calculate shippingPrice
            $brandId = $productKept->getBrand()->getId();
            if (!array_key_exists($brandId, $brandMapping)) {
                $brandMapping[$brandId] = [];
            }
            $brandMapping[$brandId][] = $productKept->getId();

            // Update Prices
            $quantity = $productKept->getQuantity();
            $finalPrice = $productKept->getFinalPrice();

            $productsPrice += $finalPrice * $quantity;
        }
        $extraBrands = count($brandMapping) * $shippingExtraPrice;
        $shippingPrice = $shippingBasePrice + $extraBrands;

        if ($productsPrice > 69.0) {
            $shippingPrice = 0.0;
        }
        $totalPrice = $productsPrice + $shippingPrice;

        $stripe->setProductsPrice($productsPrice);
        $stripe->setShippingPrice($shippingPrice);
        $stripe->setTotalPrice($totalPrice);

        return new JsonResponse(
            [
                'orderProduct' => Doctrine::toArray($orderProd),
            ]
        );
    }

}

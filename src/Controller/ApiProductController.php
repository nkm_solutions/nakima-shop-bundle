<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiProductController extends BaseController
{

    /*
     * Route: "nakima_shop_api_product"
     * Path: "/product"
     */
    public function indexAction(Request $request)
    {
        $this->checkMethod("GET");

        $pageNumber = intval($this->optParam('page', "1"));
        $limitAmount = 10;
        $products = $this->getRepo("ShopBundle:Product")->findBy(
            ['sellable' => true],
            [],
            $limitAmount,
            $limitAmount * ($pageNumber - 1)
        );
        $resp = new JsonResponse(
            [
                'products' => Doctrine::toArray($products),
            ]
        );

        return $resp;
    }

    /*
     * Route: "nakima_shop_api_product_search"
     * Path: "/product/search"
     */
    public function indexSearchAction(Request $request)
    {
        $this->checkMethod("GET");

        $prodName = $this->getParam('name');
        $limitAmount = intval($this->optParam('limit', "10"));
        if ($limitAmount > 100) {
            $limitAmount = 100;
        }

        $prodRepo = $this->getRepo("ShopBundle:Product");
        $products = array_slice(
            $prodRepo->searchByName(
                $prodName,
                [],
                $limitAmount
            ),
            0,
            $limitAmount * 12
        );

        $resp = new JsonResponse(
            [
                'products' => Doctrine::toArray($products),
            ]
        );

        return $resp;
    }

    public function discountUpdateAction(Request $request, $id)
    {
        $user = $this->checkRole("ROLE_SHOP_PROVIDER");
        $this->checkMethod("POST");

        $product = $this->getRepo('ShopBundle:Product')->find($id);

        $this->assertTrue404($product, 'Product', 'id', $id);

        $trackingNumber = $this->getParam('trackingNumber');

        $product->getDiscount()->setDiscount($trackingNumber);

        $this->validate($product);
        Doctrine::flush();

        return new JsonResponse(
            [
                'product' => Doctrine::toArray($product),
                'trackingNumber' => $trackingNumber,
            ]
        );
    }

}

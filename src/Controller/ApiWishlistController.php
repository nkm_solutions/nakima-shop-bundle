<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiWishlistController extends BaseController
{

    /*
     * Route: "nakima_shop_api_wishlist"
     * Path: "/wishlist"
     */
    public function wishlistAction(Request $request)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET", "POST");

        if ($this->isGET()) {
            $wishlists = $customer->getWishlists();

            return new JsonResponse(
                [
                    'wishlists' => Doctrine::toArray($wishlists),
                ]
            );
        } else {
            if ($this->isPOST()) {
                $name = $this->optParam('name', false);
                $description = $this->optParam('description', false);

                $wishlist = new \ShopBundle\Entity\Wishlist;
                $wishlist->setName($name);
                $wishlist->setDescription($description);

                $this->validate($wishlist);
                $customer->addWishlist($wishlist);

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($customer);
                $manager->flush();

                $resp = new JsonResponse(
                    [
                        'wishlist' => Doctrine::toArray($wishlist),
                    ]
                );

                return $resp;
            }
        }
    }

    /*
     * Route: "nakima_shop_api_wishlist_wid"
     * Path: "/wishlist/{wId}"
     */
    public function wishlistIdAction(Request $request, $wId)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET", "PUT", "DELETE");

        $wishlistId = intval($wId);

        $c_wishlists = $customer->getWishlists();
        $wishlist = false;
        foreach ($c_wishlists as $c_wishlist) {
            if ($c_wishlist->getId() === $wishlistId) {
                $wishlist = $c_wishlist;
                break;
            }
        }
        $this->assertTrue404($wishlist, "Wishlist", "id", $wishlistId);

        if ($this->isGET()) {
            return new JsonResponse(
                [
                    'wishlist' => Doctrine::toArray($wishlist),
                ]
            );
        } else {
            if ($this->isPUT()) {
                $name = $this->optParam('name', false);
                $description = $this->optParam('description', false);

                $wishlist->setName($name);
                $wishlist->setDescription($description);

                $this->validate($wishlist);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($wishlist);
                $manager->flush();

                $resp = new JsonResponse(
                    [
                        'wishlist' => Doctrine::toArray($wishlist),
                    ]
                );

                return $resp;
            } else {
                if ($this->isDELETE()) {
                    $manager = $this->getDoctrine()->getManager();
                    $manager->remove($wishlist);
                    $manager->flush();

                    $resp = new JsonResponse(
                        [
                            'wishlist' => Doctrine::toArray($wishlist),
                        ]
                    );

                    return $resp;
                }
            }
        }
    }

    /*
     * Route: "nakima_shop_api_wishlist_wid_product"
     * Path: "/wishlist/{wId}/product"
     */
    public function wishlistIdProductAction(Request $request, $wId)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET");

        $wishlistId = intval($wId);

        $c_wishlists = $customer->getWishlists();
        $wishlist = false;
        foreach ($c_wishlists as $c_wishlist) {
            if ($c_wishlist->getId() === $wishlistId) {
                $wishlist = $c_wishlist;
                break;
            }
        }
        $this->assertTrue404($wishlist, "Wishlist", "id", $wishlistId);

        if ($this->isGET()) {
            return new JsonResponse(
                [
                    'products' => Doctrine::toArray($wishlist->getProducts()),
                ]
            );
        }

    }

    /*
     * Route: "nakima_shop_api_wishlist_wid_product_pid"
     * Path: "/wishlist/{wId}/product/{pId}"
     */
    public function wishlistIdProductIdAction(Request $request, $wId, $pId)
    {
        $customer = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST", "DELETE");

        $wishlistId = intval($wId);
        $productId = intval($pId);

        $c_wishlists = $customer->getWishlists();
        $wishlist = false;
        foreach ($c_wishlists as $c_wishlist) {
            if ($c_wishlist->getId() === $wishlistId) {
                $wishlist = $c_wishlist;
                break;
            }
        }
        $this->assertTrue404($wishlist, "Wishlist", "id", $wishlistId);

        $product = $this->getRepo("ShopBundle:Product")->findOneById($productId);
        $this->assertTrue404($product, 'Product', 'id', $productId);

        if ($this->isPOST()) {
            $productId = $product->getId();
            $wlProds = $wishlist->getProducts();
            $alreadyWithin = false;
            foreach ($wlProds as $wlProd) {
                if ($productId === $wlProd->getId()) {
                    $alreadyWithin = true;
                }
            }

            $this->assertFalse400($alreadyWithin, 'Product', 'id', $productId);

            $wishlist->addProduct($product);

            $this->validate($wishlist);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($wishlist);
            $manager->flush();

            $resp = new JsonResponse(
                [
                    'wishlist' => Doctrine::toArray($wishlist),
                ]
            );

            return $resp;
        } else {
            if ($this->isDELETE()) {
                $wishlist->removeProduct($product);

                $this->validate($wishlist);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($wishlist);
                $manager->flush();

                $resp = new JsonResponse(
                    [
                        'wishlist' => Doctrine::toArray($wishlist),
                    ]
                );

                return $resp;
            }
        }
    }

}

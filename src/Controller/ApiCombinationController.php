<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiCombinationController extends BaseController
{

    /*
     * Route: "nakima_shop_api_create_combinations"
     * Path: "/product/{pId}/combinations"
     */
    public function createCombinationsAction(Request $request, $pId)
    {
        // Security
        $this->checkUser();
        $this->checkRole("ROLE_SHOP_PROVIDER");

        $this->checkMethod("POST");

        // Get and check the product id and its attributes
        $productId = intval($pId);
        $product = $this->getRepo("ShopBundle:Product")->findOneById($productId);
        $this->assertTrue404($product, 'Product', 'id', $productId);
        $prodAttributes = $product->getAttributes();
        $this->assertTrue404(count($prodAttributes), 'ProductAttributes', 'count', count($prodAttributes));
        $countProdAttributes = count($prodAttributes);

        // Get optional params
        $priceImpact = intval($this->optParam('price_impact', '0'));
        $prodAttValueIds = $this->optParam('attributevaluesIds', []);

        // Define a initialized map
        $map = [];
        foreach ($prodAttributes as $prodAttribute) {
            $map[$prodAttribute->getId()] = [];
        }

        // Fill the map with defined Attribute Values
        $prodAttValueRepo = $this->getRepo("ShopBundle:ProductAttributeValue");
        foreach ($prodAttValueIds as $prodAttrValId) {
            $prodAttrValue = $prodAttValueRepo->findOneById($prodAttrValId);
            $this->assertTrue400($prodAttrValue, 'ProductAttribute', 'id', $prodAttrValue);

            $prodAttId = $prodAttrValue->getProductAttribute()->getId();
            $this->assertTrue400(array_key_exists($prodAttId, $map), 'ProductAttribute', 'id', $prodAttId);

            $map[$prodAttId][$prodAttrValue->getId()] = $prodAttrValue;
        }

        // Search the map for emty arrays and fill them with all the posible attribute values
        $prodAttRepo = $this->getRepo("ShopBundle:ProductAttribute");
        foreach ($map as $prodAttId => $prodAttrValuesArray) {
            if (count($prodAttrValuesArray) === 0) {
                $prodAttr = $prodAttRepo->findOneById($prodAttId);
                $prodAttrValues = $prodAttr->getAttributeValues();
                foreach ($prodAttrValues as $prodAttrValue) {
                    $map[$prodAttId][$prodAttrValue->getId()] = $prodAttrValue;
                }
            }
        }

        // Combine all attribute values
        $result = $this->cartesian($map);

        // Create the ProductCombinations
        $new = [];
        $modified = [];
        foreach ($result as $combination) {
            $productComb = new \ShopBundle\Entity\ProductCombination;
            $productComb->setProduct($product);
            $productComb->setupDiscount($priceImpact);

            $attrValues = [];
            foreach ($combination as $key => $value) {
                $productComb->addProductAttributeValue($value);
                $attrValues[] = $value->getId();
            }

            $this->assertTrue400(
                count($attrValues) === $countProdAttributes,
                'ProductAttributeValues',
                'count',
                count($attrValues)
            );

            $productCombIsValid = count($this->validate($productComb, true) === 0);

            if ($productCombIsValid) {
                $prodCombRepo = $this->getRepo("ShopBundle:ProductCombination");
                $prodCombs = $prodCombRepo->findByAttrbValues($productId, $attrValues);

                $prodCombExists = false;
                $auxProdComb = null;
                foreach ($prodCombs as $prodComb) {
                    $countPAV = count($prodComb->getProductAttributeValues());
                    $countAV = count($attrValues);
                    if ($countPAV === $countAV) {
                        $prodCombExists = true;
                        $auxProdComb = $prodComb;
                        break;
                    }
                }

                if (!$prodCombExists) {
                    $new[] = $productComb;
                } else {
                    $auxProdComb->setupDiscount($priceImpact);
                    $modified[] = $auxProdComb;
                }
            }
        }

        // Persistence
        $simulated = intval($this->optParam('__simulation', 'false'));

        if (! $simulated) {
            foreach ($new as $newProductComb) {
                Doctrine::persist($newProductComb);
            }
            Doctrine::flush();
        }

        $all = $product->getCombinations();
        // \IO\Out::var("return", $return);
        return new JsonResponse(
            [
                'all_combinations' => Doctrine::toArray($all),
                'new_combinations' => Doctrine::toArray($new),
                'mod_combinations' => Doctrine::toArray($modified),
            ]
        );
    }

    private function cartesian($input)
    {
        $result = [[]];
        foreach ($input as $key => $values) {
            $append = [];
            foreach ($result as $product) {
                foreach ($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }
            $result = $append;
        }

        return $result;
    }

}

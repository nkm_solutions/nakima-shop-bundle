<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiCartController extends BaseController
{

    /*
     * Route: "nakima_shop_api_cart"
     * Path: "/cart"
     */
    public function indexAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST", "GET");

        if ($this->isGET()) {
            return new JsonResponse(
                [
                    'cart' => Doctrine::toArray($user->getCart()),
                ]
            );
        } else {
            if ($this->isPOST()) {
                return new JsonResponse(
                    [
                        'status' => "Not Yet Implemented",
                    ]
                );
            }
        }
    }

    /*
     * Route: "nakima_shop_api_cart_product_id"
     * Path: "/cart/product/{id}"
     */
    public function productIdAction(Request $request, $id)
    {
        // Precondition
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $prodCombRepo = $this->getRepo("ShopBundle:ProductCombination");
        $prodComb = $prodCombRepo->findOneById($id);
        $this->assertTrue400($prodComb, 'ProductCombination', 'id', $id);

        $product = $prodComb->getProduct();

        $found = false;
        foreach ($product->getShops() as $shop) {
            $found = $shop->getEnabled();
            if ($found) {
                break;
            }
        }
        $this->assertTrue404($found, 'Shops enabled', 'id', $id);
        $this->assertTrue400($product->getSellable(), 'sellable');
        // ! Precondition

        $qty = $this->optParam("qty", 1);

        $cartProduct = new \ShopBundle\Entity\CartProduct;
        $cartProduct->setProductCombination($prodComb);
        $cartProduct->setQuantity($qty);
        $user->getCart()->addCartProduct($cartProduct);

        Doctrine::flush();

        return new JsonResponse(
            [
                'cart' => Doctrine::toArray($user->getCart()),
                'product' => Doctrine::toArray($cartProduct),
            ]
        );
    }

    /*
     * Route: "nakima_shop_api_cart_product_id_empty"
     * Path: "/cart/empty"
     */
    public function emptyCartAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        foreach ($user->getCart()->getCartProducts() as $key => $value) {
            Doctrine::remove($value);
        }

        Doctrine::flush();

        return new JsonResponse(
            [
                'cart' => Doctrine::toArray($user->getCart()),
            ]
        );
    }

    /*
     * Route: "nakima_shop_api_cart_shippingmethod"
     * Path: "/cart/shippingmethod"
     */
    public function setShippingmethodAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("GET", "PUT");

        if ($this->isGET()) {
            $userCart = $user->getCart();

            $shippingMethodRepo = $this->getRepo("ShopBundle:ShippingMethod");
            $shippingMethods = $shippingMethodRepo->findAll();

            $cartProducts = $userCart->getCartProducts();

            // 1. Check if express method is available
            $expressAvailable = False;
            $citiesAvailable = [];
            $citiesAvailable[] = "Barcelona";

            foreach ($cartProducts as $prod) {
                $shops = $prod->getProduct()->getShops();
                foreach ($shops as $shop) {
                    if ($shop->getEnabled()) {
                        $shopCity = $shop->getAddress()->getCity();
                        if (in_array($shopCity, $citiesAvailable)) {
                            $expressAvailable = true;
                        }
                    }
                }
            }
            if (! $expressAvailable) {
                $aux = [];
                foreach ($shippingMethods as $shippingMethod) {
                    if ($shippingMethod->getName() !== "Express") {
                        $aux[] = $shippingMethod;
                    }
                }
                $shippingMethods = $aux;
            }

            // 2. Bounding shipping methods
            foreach ($cartProducts as $prod) {
                $shops = $prod->getProduct()->getShops();

                $prodShMths = [];

                foreach ($shops as $shop) {
                    if ($shop->getEnabled()) {
                        $sSMs = $shop->getShippingMethods();
                        foreach ($sSMs as $sSM) {
                            if (!in_array($sSM, $prodShMths)) {
                                $prodShMths[] = $sSM;
                            }
                        }
                    }
                }

                $aux = [];
                foreach ($shippingMethods as $shippingMethod) {
                    if (in_array($shippingMethod, $prodShMths)) {
                        $aux[] = $shippingMethod;
                    }
                }
                $shippingMethods = $aux;
            }

            return new JsonResponse(
                [
                    'shippingMethods' => Doctrine::toArray($shippingMethods),
                ]
            );
        }
        else if ($this->isPUT()) {
            $shippingMethodName = $this->optParam("shippingMethodName", "Standard");
            $shippingMethod = \ShopBundle\Entity\ShippingMethod::load(
                $shippingMethodName
            );

            $this->assertTrue404(
                $shippingMethod,
                'ShippingMethod',
                'shippingMethodName',
                $shippingMethodName
            );

            $user->getCart()->setShippingMethod($shippingMethod);

            Doctrine::flush();

            return new JsonResponse(
                [
                    'cart' => Doctrine::toArray($user->getCart()),
                ]
            );
        }
    }

    /*
     * Route: "nakima_shop_api_cart_promotion_code"
     * Path: "/cart/promotion"
     */
    public function promotionCodeAction(Request $request)
    {
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST", "DELETE");

        $userCart = $user->getCart();
        $cartPromo = null;

        if ($this->isPOST()) {
            $code = $this->getParam('code');

            $cartPromoRepo = $this->getRepo("ShopBundle:CartPromotion");
            $cartPromo = $cartPromoRepo->findOneByCode($code);
            $this->assertTrue400(
                $cartPromo,
                'CartPromotion',
                'code',
                $code
            );
            if (!$cartPromo->isOnTime()) {
                $userCart->setPromotion(null);
                $this->validate($userCart);
                Doctrine::flush();
                $this->assertTrue400(false, 'CartPromotion', 'code', $code);
            }

            $auxRepo = $this->getRepo("ShopBundle:Customer_CartPromotion");
            $auxEntity = $auxRepo->findOneBy(
                [
                    'customer' => $user,
                    'cartPromotion' => $cartPromo,
                ]
            );

            $usagesDone = 0;
            if ($auxEntity) {
                $usagesDone = $auxEntity->getUsages();
            }
            $maxUsages = $cartPromo->getUsages();

            if ($usagesDone >= $maxUsages) {
                $userCart->setPromotion(null);
                $this->validate($userCart);
                Doctrine::flush();
                $this->assertTrue400(false, 'cartPromotion');
            }
        }

        $userCart->setPromotion($cartPromo);
        $this->validate($userCart);
        Doctrine::flush();

        return new JsonResponse(
            [
                'cart' => Doctrine::toArray($userCart),
            ]
        );

    }

    /**************************************************************************
     * Inner Functions                                                        *
     **************************************************************************/

    protected function getCart()
    {

        $user = $this->checkUser();

        return new JsonResponse(
            [
                'cart' => Doctrine::toArray($user->getCart()),
            ]
        );
    }

}

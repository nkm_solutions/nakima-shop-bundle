<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiCartProductController extends BaseController
{

    /*
     * Route: "nakima_shop_api_cart_product_id_update"
     * Path: "/cart/product/{id}/update"
     */
    public function productIdUpdateAction(Request $request, $id)
    {
        // Precondition
        $user = $this->checkRole("ROLE_SHOP_CUSTOMER");
        $this->checkMethod("POST");

        $cartProductId = intval($id);
        $cartProduct = false;

        foreach ($user->getCart()->getCartProducts() as $value) {
            if ($cartProductId === $value->getId()) {
                $cartProduct = $value;
                break;
            }
        }

        $this->assertTrue404($cartProduct, 'CartProduct', 'id', $cartProductId);
        // ! Precondition

        $qty = $this->optParam("qty", 0);

        $manager = $this->getDoctrine()->getManager();

        if ($qty) {
            $cartProduct->setQuantity($qty);
        } else {
            $user->getCart()->removeCartProduct($cartProduct);
            $manager->remove($cartProduct);
        }

        $user->getCart()->updateTotalPrice();

        $manager->flush();

        return new JsonResponse(
            [
                'cart' => Doctrine::toArray($user->getCart()),
            ]
        );
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Controller;

/**
 * @author Javier Gonzalez < xgonzalez@nakima.es >
 */

use Nakima\AdminBundle\Controller\BaseAdminController;
use Symfony\Component\HttpFoundation\Request;

class AdminProductCategoryController extends BaseAdminController
{

    public function createAction(Request $request, $entity = null)
    {

        $bundle = ucfirst($request->attributes->get('bundle'));
        $entity = ucfirst($request->attributes->get('entity'));

        $this->configure($bundle, $entity);

        $entityClass = $bundle."Bundle\\Entity\\$entity";
        $subject = new $entityClass;

        $this->admin->setSubject($subject);
        $this->admin->setAction('create');

        $form = $this->createFormBuilder($subject);
        $this->admin->createFields($form);
        $form = $form->getForm();

        $this->block->setAction('create');

        if ($this->isPost()) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $data = $form->getData();

                $manager = $this->getDoctrine()->getManager();

                $list = $this->optParam('subcategories', []);
                foreach ($list as $key => $name) {
                    $subcategory = new \ShopBundle\Entity\ProductSubcategory;
                    $subcategory->setName($name);
                    $subcategory->setAttribute($data);
                }

                $manager->persist($data);
                $manager->flush();

                $this->addFlash(
                    'success',
                    "Registro completado correctamente."
                );

                return $this->redirect(
                    $this->generateUrl(
                        'nakima_admin_action',
                        [
                            'bundle' => $bundle,
                            'entity' => $entity,
                            'action' => 'list',
                        ]
                    ),
                    301
                );
            }
        }

        return $this->render(
            $this->block->getTemplate(),
            [
                'form' => $form->createView(),
                'config_block' => $this->configBlock,
                'block' => $this->block,
                'admin' => $this->admin,
                'bundle_name' => $bundle,
                'entity_name' => $entity,
                'entity_fqn' => $this->entityFqn,
                'controller_fqn' => $this->controllerFqn,
                'settings' => $this->admin->getSettings(),
                'data' => $subject,
            ]
        );
    }
}

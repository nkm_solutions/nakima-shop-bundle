<?php
declare(strict_types=1);
namespace Nakima\ShopBundle;

/**
 * Javier Gonzalez < xgonzalez@nakima.es >
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaShopBundle extends Bundle
{
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Block;

use Nakima\AdminBundle\Block\EntityBlock;

class OrderBlock extends EntityBlock
{

    public function getTemplate()
    {

        if ($this->action == "list" || $this->action == "show") {
            return "NakimaShopBundle:Block:orders_$this->action.html.twig";
        }

        return parent::getTemplate();
    }
}

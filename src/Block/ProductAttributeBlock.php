<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Block;

use Nakima\AdminBundle\Block\EntityBlock;

class ProductAttributeBlock extends EntityBlock
{

    public function getTemplate()
    {

        if ($this->action == "edit") {
            return "NakimaShopBundle:Block:productattribute_$this->action.html.twig";
        }

        if ($this->action == "show") {
            return "NakimaShopBundle:Block:productattribute_$this->action.html.twig";
        }

        if ($this->action == "create") {
            return "NakimaShopBundle:Block:productattribute_$this->action.html.twig";
        }

        return parent::getTemplate();
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Block;

use Nakima\AdminBundle\Block\EntityBlock;

class ProductBlock extends EntityBlock
{

    public function getTemplate()
    {
        if ($this->action == "edit" || $this->action == "create" || $this->action == "list") {
            return "NakimaShopBundle:Block:product_$this->action.html.twig";
        }

        return parent::getTemplate();
    }
}

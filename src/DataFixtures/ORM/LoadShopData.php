<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use AddressBundle\Entity\Address;
use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\Shop;

class LoadShopData extends Fixture
{

    public function loadTest(): void
    {
        $address = new Address();
        $address->setName("Tienda Test Shop 1");
        $address->setContactName("Some address");
        $address->setContactNumber("93 123 45 67");
        $address->setLine("Carrer de Numància, 73");
        $address->setZip("08029");
        $address->setCity("Madrid");
        $address->setProvince("Madrid");

        $shop = new Shop();
        $shop->setName("test shop 1");
        $shop->setDescription("descripción de test shop 1");
        $shop->setSummary("descripción de test shop 1");
        $shop->setEnabled(false);
        $shop->setTelephone("123456789");
        $shop->setWeb("http://google.com");
        $shop->setEmail("shop1@nakima.es");
        $shop->setOnlineShop(false);
        $shop->setBrand($this->get('nakima-shop-brand1'));
        $shop->setAddress($address);
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-store'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-standard'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-express'));
        $this->persist($shop, 'nakima-shop-shop1');

        $address = new Address();
        $address->setName("Tienda Test Shop 2");
        $address->setContactName("Nakima S.L.");
        $address->setContactNumber("93 123 45 67");
        $address->setLine("Carrer de Valencia, 65");
        $address->setZip("08015");
        $address->setCity("Barcelona");
        $address->setProvince("Barcelona");

        $shop = new Shop();
        $shop->setName("test shop 2");
        $shop->setDescription("descripción de test shop 2");
        $shop->setSummary("descripción de test shop 2");
        $shop->setEnabled(true);
        $shop->setTelephone("123456789");
        $shop->setWeb("http://google.com");
        $shop->setEmail("shop2@nakima.es");
        $shop->setOnlineShop(true);
        $shop->setBrand($this->get('nakima-shop-brand2'));
        $shop->setAddress($address);
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-store'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-standard'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-express'));
        $this->persist($shop, 'nakima-shop-shop2');

        $address = new Address();
        $address->setName("Tienda Test Shop 3");
        $address->setContactName("Some address");
        $address->setContactNumber("93 123 45 67");
        $address->setLine("Carrer de Numància, 73");
        $address->setZip("08029");
        $address->setCity("Madrid");
        $address->setProvince("Madrid");

        $shop = new Shop();
        $shop->setName("test shop 3");
        $shop->setDescription("descripción de test shop 3");
        $shop->setSummary("descripción de test shop 3");
        $shop->setEnabled(true);
        $shop->setTelephone("123456789");
        $shop->setWeb("http://google.com");
        $shop->setEmail("shop1@nakima.es");
        $shop->setOnlineShop(false);
        $shop->setBrand($this->get('nakima-shop-brand1'));
        $shop->setAddress($address);
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-store'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-standard'));
        $shop->addShippingMethod($this->get('showea-shop-shippingmethod-express'));
        $this->persist($shop, 'nakima-shop-shop3');
    }

    public function getOrder(): int
    {
        return 2;
    }

}

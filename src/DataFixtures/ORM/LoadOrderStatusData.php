<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\OrderStatus;

class LoadOrderStatusData extends Fixture
{

    public function loadProd(): void
    {
        $this->persist(new OrderStatus("PENDING",    1), 'nakima-shop-status-pending');
        $this->persist(new OrderStatus("PREPARING",  2), 'nakima-shop-status-preparing');
        $this->persist(new OrderStatus("IN TRANSIT", 3), 'nakima-shop-status-intransit');
        $this->persist(new OrderStatus("TRYING",     4), 'nakima-shop-status-trying');
        $this->persist(new OrderStatus("MOVED",      5), 'nakima-shop-status-moved');
        $this->persist(new OrderStatus("RETURNING",  6), 'nakima-shop-status-returning');
        $this->persist(new OrderStatus("RETURNED",   7), 'nakima-shop-status-returned');
        $this->persist(new OrderStatus("CANCELED",   8), 'nakima-shop-status-canceled');
        $this->persist(new OrderStatus("DELIVERED",  9), 'nakima-shop-status-delivered');
    }
}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use UserBundle\Entity\Role;

class LoadRoleData extends Fixture
{

    public function loadProd(): void
    {
        $role = new Role();
        $role->setRole('ROLE_SHOP_ADMIN');
        $this->persist($role, 'role-shop-admin');

        $role = new Role();
        $role->setRole('ROLE_SHOP_PROVIDER');
        $this->persist($role, 'role-shop-provider');

        $role = new Role();
        $role->setRole('ROLE_SHOP_PROVIDER_ASSISTANT');
        $this->persist($role, 'role-shop-admin-assistant');

        $role = new Role();
        $role->setRole('ROLE_SHOP_SHOP_MANAGER');
        $this->persist($role, 'role-shop-shop-manager');

        $role = new Role();
        $role->setRole('ROLE_SHOP_SHOP_ASSISTANT');
        $this->persist($role, 'role-shop-shop-assistant');

        $role = new Role();
        $role->setRole('ROLE_SHOP_CUSTOMER');
        $this->persist($role, 'role-shop-customer');

        $role = new Role();
        $role->setRole('ROLE_SHOP_CREATE_BRAND');
        $this->persist($role, 'role-shop-create-brand');
    }
}

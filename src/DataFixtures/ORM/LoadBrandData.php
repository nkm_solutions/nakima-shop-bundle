<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\Brand;

class LoadBrandData extends Fixture
{

    public function loadTest(): void
    {
        $brand = new Brand();
        $brand->setName("test brand 1");
        $brand->setDescription("descripción de test brand 1");
        $brand->setEnabled(false);
        $this->persist($brand, 'nakima-shop-brand1');

        $brand = new Brand();
        $brand->setName("test brand 2");
        $brand->setDescription("descripción de test brand 2");
        $brand->setEnabled(true);
        $this->persist($brand, 'nakima-shop-brand2');

        $brand = new Brand();
        $brand->setName("test brand 3");
        $brand->setDescription("descripción de test brand 3");
        $brand->setEnabled(true);
        $this->persist($brand, 'nakima-shop-brand3');

    }
}

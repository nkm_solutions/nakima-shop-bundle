<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use Nakima\Utils\Time\DateTime;
use ShopBundle\Entity\Campaign;

class LoadCampaignData extends Fixture
{

    public function loadDev(): void
    {

        $campaign = new Campaign();
        $campaign->setName('Nakima');
        $campaign->setFromDate(DateTime::fromFormat("d-m-Y", "01-11-2016"));
        $campaign->setToDate(DateTime::fromFormat("d-m-Y", "01-03-2130"));
        $this->persist($campaign, 'nakima-shop-campaign1');

    }

    public function getOrder(): int
    {
        return 1;
    }

}

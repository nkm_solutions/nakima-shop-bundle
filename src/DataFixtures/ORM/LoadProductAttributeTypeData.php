<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\ProductAttributeType;

class LoadProductAttributeTypeData extends Fixture
{

    public function loadProd(): void
    {
        $this->persist(ProductAttributeType::create('STRING'));
        $this->persist(ProductAttributeType::create('RANGE'));
        $this->persist(ProductAttributeType::create('COLOR'));
    }
}

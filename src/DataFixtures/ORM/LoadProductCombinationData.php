<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\ProductCombination;

class LoadProductCombinationData extends Fixture
{

    public function loadTest(): void
    {
        /**********************************************************************
         * Product 3                                                          *
         **********************************************************************/
        $prodComb = new ProductCombination();
        $prodComb->setProduct($this->get('nakima-shop-product3'));
        $prodComb->setupDiscount(10.5);
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-xs'));
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-Negro'));
        $this->persist($prodComb);

        /**********************************************************************
         * Product 4                                                          *
         **********************************************************************/
        $prodComb = new ProductCombination();
        $prodComb->setProduct($this->get('nakima-shop-product4'));
        $prodComb->setupDiscount(10.5);
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-xs'));
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-Negro'));
        $this->persist($prodComb);

        /**********************************************************************
         * Product 5                                                          *
         **********************************************************************/
        $prodComb = new ProductCombination();
        $prodComb->setProduct($this->get('nakima-shop-product5'));
        $prodComb->setupDiscount(10.5);
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-xs'));
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-Negro'));
        $this->persist($prodComb);

        /**********************************************************************
         * Product 6                                                          *
         **********************************************************************/
        $prodComb = new ProductCombination();
        $prodComb->setProduct($this->get('nakima-shop-product6'));
        $prodComb->setupDiscount(10.5);
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-xs'));
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-Negro'));
        $this->persist($prodComb);

        /**********************************************************************
         * Product 7                                                          *
         **********************************************************************/
        $prodComb = new ProductCombination();
        $prodComb->setProduct($this->get('nakima-shop-product7'));
        $prodComb->setupDiscount(10.5);
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-xs'));
        $prodComb->addProductAttributeValue($this->get('nakima-shop-pav-Negro'));
        $this->persist($prodComb);
    }

    public function getOrder(): int
    {
        return 4;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\Product;

class LoadProductData extends Fixture
{

    public function loadTest(): void
    {

        $product = new Product();
        $product->setName("test product 1");
        $product->setSummary("resumen de test product 1");
        $product->setDescription("descripción de test product 1");
        $product->setPrice(99.99);
        $product->setEnabled(false);
        $product->setBrand($this->get('nakima-shop-brand1'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $this->persist($product, 'nakima-shop-product1');

        $product = new Product();
        $product->setName("test product 2");
        $product->setSummary("resumen de test product 2");
        $product->setDescription("descripción de test product 2");
        $product->setPrice(99.99);
        $product->setEnabled(true);
        $product->setBrand($this->get('nakima-shop-brand1'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $this->persist($product, 'nakima-shop-product2');

        $product = new Product();
        $product->setName("test product 3");
        $product->setSummary("resumen de test product 3");
        $product->setDescription("descripción de test product 3");
        $product->setPrice(99.99);
        $product->setEnabled(true);
        $product->setBrand($this->get('nakima-shop-brand1'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $product->addShop($this->get('nakima-shop-shop1'));
        $product->setMainCategory($this->get("nakima-shop-product-shcat32"));
        $product->addAttribute($this->get("nakima-shop-pa-color"));
        $this->persist($product, 'nakima-shop-product3');

        $product = new Product();
        $product->setName("test product 4");
        $product->setSummary("resumen de test product 4");
        $product->setDescription("descripción de test product 4");
        $product->setPrice(25.55);
        $product->setEnabled(true);
        $product->setHto(true);
        $product->setBrand($this->get('nakima-shop-brand2'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $product->addShop($this->get('nakima-shop-shop1'));
        $product->addShop($this->get('nakima-shop-shop2'));
        $product->addAttribute($this->get("nakima-shop-pa-color"));
        $product->addAttribute($this->get("nakima-shop-pa-talla"));
        $this->persist($product, 'nakima-shop-product4');

        $product = new Product();
        $product->setName("test product 5");
        $product->setSummary("resumen de test product 5");
        $product->setDescription("descripción de test product 5");
        $product->setPrice(12.25);
        $product->setupDiscount(2);
        $product->setEnabled(true);
        $product->setHto(true);
        $product->setBrand($this->get('nakima-shop-brand3'));
        $product->addShop($this->get('nakima-shop-shop3'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $this->persist($product, 'nakima-shop-product5');

        $product = new Product();
        $product->setName("test product 6");
        $product->setSummary("resumen de test product 6");
        $product->setDescription("descripción de test product 6");
        $product->setPrice(15.25);
        $product->setupDiscount(15, false);
        $product->setEnabled(true);
        $product->setBrand($this->get('nakima-shop-brand3'));
        $product->addShop($this->get('nakima-shop-shop2'));
        $product->addShop($this->get('nakima-shop-shop3'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $this->persist($product, 'nakima-shop-product6');

        $product = new Product();
        $product->setName("test product 7");
        $product->setSummary("resumen de test product 7");
        $product->setDescription("descripción de test product 7");
        $product->setPrice(15.25);
        $product->setupDiscount(15, false);
        $product->setEnabled(true);
        $product->setBrand($this->get('nakima-shop-brand3'));
        $product->addShop($this->get('nakima-shop-shop2'));
        $product->setMainCategory($this->get('nakima-shop-product-shcat32'));
        $this->persist($product, 'nakima-shop-product7');

    }

    public function getOrder(): int
    {
        return 3;
    }

}

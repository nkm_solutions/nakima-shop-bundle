<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\CartPromotion;

class LoadCartPromotionData extends Fixture
{

    public function loadDev(): void
    {

        $promotion = new CartPromotion();
        $promotion->setCampaign($this->get("nakima-shop-campaign1"));
        $promotion->setupDiscount(15, false);
        $promotion->setCode("sudo su");
        $promotion->setUsages(1);
        $this->persist($promotion, 'nakima-shop-cart_promotion_1');

    }

    public function getOrder(): int
    {
        return 2;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\ProductAttribute;
use ShopBundle\Entity\ProductAttributeType;
use ShopBundle\Entity\ProductAttributeValue;

class LoadProductAttributeData extends Fixture
{

    public function loadProd(): void
    {

        $pav_xxs = ProductAttributeValue::create('XXS', "4");
        $pav_xs = ProductAttributeValue::create('XS', "6");
        $pav_s = ProductAttributeValue::create('S', "8");
        $pav_m = ProductAttributeValue::create('M', "10");
        $pav_l = ProductAttributeValue::create('L', "12");
        $pav_xl = ProductAttributeValue::create('XL', "14");
        $pav_xxl = ProductAttributeValue::create('XXL', "16");

        $this->persist($pav_xxs, 'nakima-shop-pav-xxs');
        $this->persist($pav_xs, 'nakima-shop-pav-xs');
        $this->persist($pav_s, 'nakima-shop-pav-s');
        $this->persist($pav_m, 'nakima-shop-pav-m');
        $this->persist($pav_l, 'nakima-shop-pav-l');
        $this->persist($pav_xl, 'nakima-shop-pav-xl');
        $this->persist($pav_xxl, 'nakima-shop-pav-xxl');

        $pa = ProductAttribute::create(
            'Talla',
            [$pav_xxs, $pav_xs, $pav_s, $pav_m, $pav_l, $pav_xl, $pav_xxl],
            ProductAttributeType::load('STRING')
        );
        $this->persist($pa, 'nakima-shop-pa-talla');


        $pav_Negro = ProductAttributeValue::create('Negro', "#000000");
        $pav_Rojo = ProductAttributeValue::create('Rojo', "#FF0000");
        $pav_Verde = ProductAttributeValue::create('Verde', "#00FF00");
        $pav_Azul = ProductAttributeValue::create('Azul', "#0000FF");
        $pav_Cian = ProductAttributeValue::create('Cián', "#00FFFF");
        $pav_Magenta = ProductAttributeValue::create('Magenta', "#FF00FF");
        $pav_Amarillo = ProductAttributeValue::create('Amarillo', "#FF0000");
        $pav_Blanco = ProductAttributeValue::create('Blanco', "#FFFFFF");

        $this->persist($pav_Negro, 'nakima-shop-pav-Negro');
        $this->persist($pav_Rojo, 'nakima-shop-pav-Rojo');
        $this->persist($pav_Verde, 'nakima-shop-pav-Verde');
        $this->persist($pav_Azul, 'nakima-shop-pav-Azul');
        $this->persist($pav_Cian, 'nakima-shop-pav-Cian');
        $this->persist($pav_Magenta, 'nakima-shop-pav-Magenta');
        $this->persist($pav_Amarillo, 'nakima-shop-pav-Amarillo');
        $this->persist($pav_Blanco, 'nakima-shop-pav-Blanco');

        $pa = ProductAttribute::create(
            'Color',
            [
                $pav_Negro,
                $pav_Rojo,
                $pav_Verde,
                $pav_Azul,
                $pav_Cian,
                $pav_Magenta,
                $pav_Amarillo,
                $pav_Blanco,
            ],
            ProductAttributeType::load('COLOR')
        );
        $this->persist($pa, 'nakima-shop-pa-color');
    }

    public function getOrder(): int
    {
        return 3;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\ShippingMethod;

class LoadShippingMethodData extends Fixture
{

    public function loadProd(): void
    {
        /**********************************************************************
         * Shipping Methods                                                   *
         **********************************************************************/
        $sMethod = new ShippingMethod();
        $sMethod->setName('Free');
        $sMethod->setPrice(0);
        $sMethod->setInfo("recogida en tienda");
        $sMethod->setDescription("Recogida en tienda en 1-2 días laborables. Gratuito");
        $this->persist($sMethod, 'showea-shop-shippingmethod-store');

        $sMethod = new ShippingMethod();
        $sMethod->setName('Standard');
        $sMethod->setPrice(4.95);
        $sMethod->setInfo("de 3 a 5 días");
        $sMethod->setDescription(
            "Envío estándar en 3-5 días laborables a toda España (Península); 4,95€ + 1€ por marca."
        );
        $this->persist($sMethod, 'showea-shop-shippingmethod-standard');

        $sMethod = new ShippingMethod();
        $sMethod->setName('Express');
        $sMethod->setPrice(5.95);
        $sMethod->setInfo("de 1 a 2 días");
        $sMethod->setDescription(
            "Envío express en 2 a 24 horas en días laborables. (origen y entrega en Barcelona ciudad); 5,95€ + 1€ por marca."
        );
        $this->persist($sMethod, 'showea-shop-shippingmethod-express');
    }
}

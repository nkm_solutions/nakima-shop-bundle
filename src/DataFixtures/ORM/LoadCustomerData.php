<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\Customer;

class LoadCustomerData extends Fixture
{

    public function loadTest(): void
    {

        $user = new Customer();
        $user->setName("test");
        $user->setSurnames("admin-1");
        $user->setUsername('test-admin-1');
        $user->setEmail("test-admin-1@nakima.es");
        $user->setPassword('1234');
        $user->setGender($this->get('gender-male'));
        $user->addRole($this->get('role-super-admin'));
        $user->setEnabled(true);
        $this->persist($user);

        $user = new Customer();
        $user->setName("test");
        $user->setSurnames("shop_manager-1");
        $user->setUsername('test-shop_manager-1');
        $user->setEmail("test-shop_manager-1@nakima.es");
        $user->setPassword('1234');
        $user->setGender($this->get('gender-female'));
        $user->addRole($this->get('role-shop-shop-manager'));
        $user->setEnabled(true);
        $this->persist($user);

        $user = new Customer();
        $user->setName("test");
        $user->setSurnames("cust-1");
        $user->setUsername('test-cust-1');
        $user->setEmail("test-cust-1@nakima.es");
        $user->setPassword('1234');
        $user->setGender($this->get('gender-female'));
        $user->setEnabled(true);
        $this->persist($user);

        $user = new Customer();
        $user->setEmail('customer_test_1@showea.es');
        $user->setPassword('1234');
        $user->setName('test1');
        $user->setSurnames('test1 surnames');
        $user->setGender($this->get('gender-male'));
        $user->setEnabled(true);
        $this->persist($user);
    }

    public function getOrder(): int
    {
        return 4;
    }

}

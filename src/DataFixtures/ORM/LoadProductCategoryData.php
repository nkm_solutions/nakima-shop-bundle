<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use Nakima\CoreBundle\DataFixtures\ORM\Fixture;
use ShopBundle\Entity\ProductCategory;

class LoadProductCategoryData extends Fixture
{

    public function loadProd(): void
    {
        $shcat0 = null;

        $shcat1 = new ProductCategory();
        $shcat1->setName("Mujer");
        $shcat1->setDescription("Ropa y complementos de moda para mujer");
        $shcat1->setParent($shcat0);
        $this->persist($shcat1, "nakima-shop-product-shcat1");
        $this->flush();


        $shcat2 = new ProductCategory();
        $shcat2->setName("Ropa");
        $shcat2->setDescription("Ropa para mujer");
        $shcat2->setParent($shcat1);
        $this->persist($shcat2, "nakima-shop-product-shcat2");
        $this->flush();


        $shcat3 = new ProductCategory();
        $shcat3->setName("Abrigos y chaquetas");
        $shcat3->setDescription("Abrigos y chaquetas de mujer");
        $shcat3->setParent($shcat2);
        $this->persist($shcat3, "nakima-shop-product-shcat3");
        $this->flush();


        $shcat4 = new ProductCategory();
        $shcat4->setName("Blazers y chalecos");
        $shcat4->setDescription("Blazers y chalecos de mujer");
        $shcat4->setParent($shcat2);
        $this->persist($shcat4, "nakima-shop-product-shcat4");
        $this->flush();


        $shcat5 = new ProductCategory();
        $shcat5->setName("Jerséis y sudaderas");
        $shcat5->setDescription("Jerséis y sudaderas de mujer");
        $shcat5->setParent($shcat2);
        $this->persist($shcat5, "nakima-shop-product-shcat5");
        $this->flush();


        $shcat6 = new ProductCategory();
        $shcat6->setName("Vestidos");
        $shcat6->setDescription("Vestidos de mujer");
        $shcat6->setParent($shcat2);
        $this->persist($shcat6, "nakima-shop-product-shcat6");
        $this->flush();


        $shcat7 = new ProductCategory();
        $shcat7->setName("Faldas");
        $shcat7->setDescription("Faldas de mujer");
        $shcat7->setParent($shcat2);
        $this->persist($shcat7, "nakima-shop-product-shcat7");
        $this->flush();


        $shcat8 = new ProductCategory();
        $shcat8->setName("Pantalones");
        $shcat8->setDescription("Pantalones de mujer");
        $shcat8->setParent($shcat2);
        $this->persist($shcat8, "nakima-shop-product-shcat8");
        $this->flush();


        $shcat9 = new ProductCategory();
        $shcat9->setName("Jeans");
        $shcat9->setDescription("Jeans de mujer");
        $shcat9->setParent($shcat2);
        $this->persist($shcat9, "nakima-shop-product-shcat9");
        $this->flush();


        $shcat10 = new ProductCategory();
        $shcat10->setName("Shorts");
        $shcat10->setDescription("Shorts de mujer");
        $shcat10->setParent($shcat2);
        $this->persist($shcat10, "nakima-shop-product-shcat10");
        $this->flush();


        $shcat11 = new ProductCategory();
        $shcat11->setName("Camisetas y tops");
        $shcat11->setDescription("Camisetas y tops de mujer");
        $shcat11->setParent($shcat2);
        $this->persist($shcat11, "nakima-shop-product-shcat11");
        $this->flush();


        $shcat12 = new ProductCategory();
        $shcat12->setName("Camisas y blusas");
        $shcat12->setDescription("Camisas y blusas de mujer");
        $shcat12->setParent($shcat2);
        $this->persist($shcat12, "nakima-shop-product-shcat12");
        $this->flush();


        $shcat13 = new ProductCategory();
        $shcat13->setName("Ropa interior");
        $shcat13->setDescription("Lenceria y ropa íntima de mujer");
        $shcat13->setParent($shcat2);
        $this->persist($shcat13, "nakima-shop-product-shcat13");
        $this->flush();


        $shcat14 = new ProductCategory();
        $shcat14->setName("Ropa de baño");
        $shcat14->setDescription("Bikinis, bañadores y complementos de baño de mujer");
        $shcat14->setParent($shcat2);
        $this->persist($shcat14, "nakima-shop-product-shcat14");
        $this->flush();


        $shcat15 = new ProductCategory();
        $shcat15->setName("Homewear");
        $shcat15->setDescription("Ropa de mujer para el hogar");
        $shcat15->setParent($shcat2);
        $this->persist($shcat15, "nakima-shop-product-shcat15");
        $this->flush();


        $shcat16 = new ProductCategory();
        $shcat16->setName("Zapatos");
        $shcat16->setDescription("Zapatos de mujer");
        $shcat16->setParent($shcat1);
        $this->persist($shcat16, "nakima-shop-product-shcat16");
        $this->flush();


        $shcat17 = new ProductCategory();
        $shcat17->setName("Planos");
        $shcat17->setDescription("Zapatos planos de mujer");
        $shcat17->setParent($shcat16);
        $this->persist($shcat17, "nakima-shop-product-shcat17");
        $this->flush();


        $shcat18 = new ProductCategory();
        $shcat18->setName("Tacones y plataformas");
        $shcat18->setDescription("Zapatos de tacón y plataforma de mujer");
        $shcat18->setParent($shcat16);
        $this->persist($shcat18, "nakima-shop-product-shcat18");
        $this->flush();


        $shcat19 = new ProductCategory();
        $shcat19->setName("Botas y botines");
        $shcat19->setDescription("Botas y botines de mujer");
        $shcat19->setParent($shcat16);
        $this->persist($shcat19, "nakima-shop-product-shcat19");
        $this->flush();


        $shcat20 = new ProductCategory();
        $shcat20->setName("Sandalias");
        $shcat20->setDescription("Sandalias de mujer");
        $shcat20->setParent($shcat16);
        $this->persist($shcat20, "nakima-shop-product-shcat20");
        $this->flush();


        $shcat21 = new ProductCategory();
        $shcat21->setName("Zapatillas deportivas");
        $shcat21->setDescription("Zapatillas deportivas de mujer");
        $shcat21->setParent($shcat16);
        $this->persist($shcat21, "nakima-shop-product-shcat21");
        $this->flush();


        $shcat22 = new ProductCategory();
        $shcat22->setName("Accesorios y complementos");
        $shcat22->setDescription("Accesorios y complementos de moda para mujer");
        $shcat22->setParent($shcat1);
        $this->persist($shcat22, "nakima-shop-product-shcat22");
        $this->flush();


        $shcat23 = new ProductCategory();
        $shcat23->setName("Guantes");
        $shcat23->setDescription("Guantes de mujer");
        $shcat23->setParent($shcat22);
        $this->persist($shcat23, "nakima-shop-product-shcat23");
        $this->flush();


        $shcat24 = new ProductCategory();
        $shcat24->setName("Cinturones");
        $shcat24->setDescription("Cinturones de mujer");
        $shcat24->setParent($shcat22);
        $this->persist($shcat24, "nakima-shop-product-shcat24");
        $this->flush();


        $shcat25 = new ProductCategory();
        $shcat25->setName("Sombreros y gorros");
        $shcat25->setDescription("Sombreros y gorros de mujer");
        $shcat25->setParent($shcat22);
        $this->persist($shcat25, "nakima-shop-product-shcat25");
        $this->flush();


        $shcat26 = new ProductCategory();
        $shcat26->setName("Accesorios para el pelo");
        $shcat26->setDescription("Accesorios para el pelo de mujer");
        $shcat26->setParent($shcat22);
        $this->persist($shcat26, "nakima-shop-product-shcat26");
        $this->flush();


        $shcat27 = new ProductCategory();
        $shcat27->setName("Bufandas y pañuelos");
        $shcat27->setDescription("Bufandas y pañuelos de mujer");
        $shcat27->setParent($shcat22);
        $this->persist($shcat27, "nakima-shop-product-shcat27");
        $this->flush();


        $shcat28 = new ProductCategory();
        $shcat28->setName("Gafas");
        $shcat28->setDescription("Monturas para gafas de mujer");
        $shcat28->setParent($shcat22);
        $this->persist($shcat28, "nakima-shop-product-shcat28");
        $this->flush();


        $shcat29 = new ProductCategory();
        $shcat29->setName("Gafas de sol");
        $shcat29->setDescription("Gafas de sol para mujer");
        $shcat29->setParent($shcat22);
        $this->persist($shcat29, "nakima-shop-product-shcat29");
        $this->flush();


        $shcat30 = new ProductCategory();
        $shcat30->setName("Relojes");
        $shcat30->setDescription("Relojes de mujer");
        $shcat30->setParent($shcat22);
        $this->persist($shcat30, "nakima-shop-product-shcat30");
        $this->flush();


        $shcat31 = new ProductCategory();
        $shcat31->setName("Otros accesorios");
        $shcat31->setDescription("Otros accesorios de mujer");
        $shcat31->setParent($shcat22);
        $this->persist($shcat31, "nakima-shop-product-shcat31");
        $this->flush();


        $shcat32 = new ProductCategory();
        $shcat32->setName("Bolsos");
        $shcat32->setDescription("Bolsos de mujer");
        $shcat32->setParent($shcat1);
        $this->persist($shcat32, "nakima-shop-product-shcat32");
        $this->flush();


        $shcat33 = new ProductCategory();
        $shcat33->setName("Bandoleras");
        $shcat33->setDescription("Bandoleras de mujer");
        $shcat33->setParent($shcat32);
        $this->persist($shcat33, "nakima-shop-product-shcat33");
        $this->flush();


        $shcat34 = new ProductCategory();
        $shcat34->setName("Bolsos de mano");
        $shcat34->setDescription("Bolsos de mano de mujer");
        $shcat34->setParent($shcat32);
        $this->persist($shcat34, "nakima-shop-product-shcat34");
        $this->flush();


        $shcat35 = new ProductCategory();
        $shcat35->setName("Mochilas");
        $shcat35->setDescription("Mochilas de mujer");
        $shcat35->setParent($shcat32);
        $this->persist($shcat35, "nakima-shop-product-shcat35");
        $this->flush();


        $shcat36 = new ProductCategory();
        $shcat36->setName("Tote bag");
        $shcat36->setDescription("Tote bag de mujer");
        $shcat36->setParent($shcat32);
        $this->persist($shcat36, "nakima-shop-product-shcat36");
        $this->flush();


        $shcat37 = new ProductCategory();
        $shcat37->setName("Monederos");
        $shcat37->setDescription("Monederos, billeteras y tarjeteros de mujer");
        $shcat37->setParent($shcat32);
        $this->persist($shcat37, "nakima-shop-product-shcat37");
        $this->flush();


        $shcat38 = new ProductCategory();
        $shcat38->setName("Accesorios para bolsos");
        $shcat38->setDescription("Accesorios para bolsos de mujer");
        $shcat38->setParent($shcat32);
        $this->persist($shcat38, "nakima-shop-product-shcat38");
        $this->flush();


        $shcat39 = new ProductCategory();
        $shcat39->setName("Joyería");
        $shcat39->setDescription("Joyas y accesorios de mujer");
        $shcat39->setParent($shcat1);
        $this->persist($shcat39, "nakima-shop-product-shcat39");
        $this->flush();


        $shcat40 = new ProductCategory();
        $shcat40->setName("Pendientes");
        $shcat40->setDescription("Pendientes de mujer");
        $shcat40->setParent($shcat39);
        $this->persist($shcat40, "nakima-shop-product-shcat40");
        $this->flush();


        $shcat41 = new ProductCategory();
        $shcat41->setName("Pulseras");
        $shcat41->setDescription("Pulseras y brazaletes de mujer");
        $shcat41->setParent($shcat39);
        $this->persist($shcat41, "nakima-shop-product-shcat41");
        $this->flush();


        $shcat42 = new ProductCategory();
        $shcat42->setName("Collares");
        $shcat42->setDescription("Collares de mujer");
        $shcat42->setParent($shcat39);
        $this->persist($shcat42, "nakima-shop-product-shcat42");
        $this->flush();


        $shcat43 = new ProductCategory();
        $shcat43->setName("Anillos");
        $shcat43->setDescription("Anillos de mujer");
        $shcat43->setParent($shcat39);
        $this->persist($shcat43, "nakima-shop-product-shcat43");
        $this->flush();


        $shcat44 = new ProductCategory();
        $shcat44->setName("Broches");
        $shcat44->setDescription("Broches de mujer");
        $shcat44->setParent($shcat39);
        $this->persist($shcat44, "nakima-shop-product-shcat44");
        $this->flush();


        $shcat45 = new ProductCategory();
        $shcat45->setName("Hombre ");
        $shcat45->setDescription("Moda y complementos para hombre");
        $shcat45->setParent($shcat0);
        $this->persist($shcat45, "nakima-shop-product-shcat45");
        $this->flush();


        $shcat46 = new ProductCategory();
        $shcat46->setName("Ropa");
        $shcat46->setDescription("Ropa para hombre");
        $shcat46->setParent($shcat45);
        $this->persist($shcat46, "nakima-shop-product-shcat46");
        $this->flush();


        $shcat47 = new ProductCategory();
        $shcat47->setName("Abrigos y chaquetas");
        $shcat47->setDescription("Abrigos y chaquetas de hombre");
        $shcat47->setParent($shcat46);
        $this->persist($shcat47, "nakima-shop-product-shcat47");
        $this->flush();


        $shcat48 = new ProductCategory();
        $shcat48->setName("Blazers y chalecos");
        $shcat48->setDescription("Blazers y chalecos de hombre");
        $shcat48->setParent($shcat46);
        $this->persist($shcat48, "nakima-shop-product-shcat48");
        $this->flush();


        $shcat49 = new ProductCategory();
        $shcat49->setName("Trajes");
        $shcat49->setDescription("Trajes de hombre");
        $shcat49->setParent($shcat46);
        $this->persist($shcat49, "nakima-shop-product-shcat49");
        $this->flush();


        $shcat50 = new ProductCategory();
        $shcat50->setName("Jerséis y sudaderas");
        $shcat50->setDescription("Jerséis y sudaderas de hombre");
        $shcat50->setParent($shcat46);
        $this->persist($shcat50, "nakima-shop-product-shcat50");
        $this->flush();


        $shcat51 = new ProductCategory();
        $shcat51->setName("Pantalones");
        $shcat51->setDescription("Pantalones de hombre");
        $shcat51->setParent($shcat46);
        $this->persist($shcat51, "nakima-shop-product-shcat51");
        $this->flush();


        $shcat52 = new ProductCategory();
        $shcat52->setName("Jeans");
        $shcat52->setDescription("Jeans de hombre");
        $shcat52->setParent($shcat46);
        $this->persist($shcat52, "nakima-shop-product-shcat52");
        $this->flush();


        $shcat53 = new ProductCategory();
        $shcat53->setName("Camisetas y polos");
        $shcat53->setDescription("Camisetas y polos de hombre");
        $shcat53->setParent($shcat46);
        $this->persist($shcat53, "nakima-shop-product-shcat53");
        $this->flush();


        $shcat54 = new ProductCategory();
        $shcat54->setName("Camisas");
        $shcat54->setDescription("Camisas de hombre");
        $shcat54->setParent($shcat46);
        $this->persist($shcat54, "nakima-shop-product-shcat54");
        $this->flush();


        $shcat55 = new ProductCategory();
        $shcat55->setName("Ropa interior");
        $shcat55->setDescription("Calcetines y ropa ínterior de hombre");
        $shcat55->setParent($shcat46);
        $this->persist($shcat55, "nakima-shop-product-shcat55");
        $this->flush();


        $shcat56 = new ProductCategory();
        $shcat56->setName("Ropa de baño");
        $shcat56->setDescription("Bañadores y complementos de baño de hombre");
        $shcat56->setParent($shcat46);
        $this->persist($shcat56, "nakima-shop-product-shcat56");
        $this->flush();


        $shcat57 = new ProductCategory();
        $shcat57->setName("Homewear");
        $shcat57->setDescription("Ropa de hombre para el hogar");
        $shcat57->setParent($shcat46);
        $this->persist($shcat57, "nakima-shop-product-shcat57");
        $this->flush();


        $shcat58 = new ProductCategory();
        $shcat58->setName("Zapatos");
        $shcat58->setDescription("Zapatos de hombre");
        $shcat58->setParent($shcat45);
        $this->persist($shcat58, "nakima-shop-product-shcat58");
        $this->flush();


        $shcat59 = new ProductCategory();
        $shcat59->setName("Zapatos de vestir");
        $shcat59->setDescription("Zapatos de vestir para hombre");
        $shcat59->setParent($shcat58);
        $this->persist($shcat59, "nakima-shop-product-shcat59");
        $this->flush();


        $shcat60 = new ProductCategory();
        $shcat60->setName("Botas");
        $shcat60->setDescription("Botas y botines de hombre");
        $shcat60->setParent($shcat58);
        $this->persist($shcat60, "nakima-shop-product-shcat60");
        $this->flush();


        $shcat61 = new ProductCategory();
        $shcat61->setName("Sandalias");
        $shcat61->setDescription("Sandalias de hombre");
        $shcat61->setParent($shcat58);
        $this->persist($shcat61, "nakima-shop-product-shcat61");
        $this->flush();


        $shcat62 = new ProductCategory();
        $shcat62->setName("Zapatillas deportivas");
        $shcat62->setDescription("Zapatillas deportivas de hombre");
        $shcat62->setParent($shcat58);
        $this->persist($shcat62, "nakima-shop-product-shcat62");
        $this->flush();


        $shcat63 = new ProductCategory();
        $shcat63->setName("Accesorios y complementos");
        $shcat63->setDescription("Accesorios y complementos de moda para hombre");
        $shcat63->setParent($shcat45);
        $this->persist($shcat63, "nakima-shop-product-shcat63");
        $this->flush();


        $shcat64 = new ProductCategory();
        $shcat64->setName("Guantes");
        $shcat64->setDescription("Guantes de hombre");
        $shcat64->setParent($shcat63);
        $this->persist($shcat64, "nakima-shop-product-shcat64");
        $this->flush();


        $shcat65 = new ProductCategory();
        $shcat65->setName("Cinturones y tirantes");
        $shcat65->setDescription("Cinturones y tirantes de hombre");
        $shcat65->setParent($shcat63);
        $this->persist($shcat65, "nakima-shop-product-shcat65");
        $this->flush();


        $shcat66 = new ProductCategory();
        $shcat66->setName("Sombreros y gorros");
        $shcat66->setDescription("Sombreros y gorros de hombre");
        $shcat66->setParent($shcat63);
        $this->persist($shcat66, "nakima-shop-product-shcat66");
        $this->flush();


        $shcat67 = new ProductCategory();
        $shcat67->setName("Corbatas y pajaritas");
        $shcat67->setDescription("Corbatas y pajaritas de hombre");
        $shcat67->setParent($shcat63);
        $this->persist($shcat67, "nakima-shop-product-shcat67");
        $this->flush();


        $shcat68 = new ProductCategory();
        $shcat68->setName("Bufandas y pañuelos");
        $shcat68->setDescription("Bufandas y pañuelos de hombre");
        $shcat68->setParent($shcat63);
        $this->persist($shcat68, "nakima-shop-product-shcat68");
        $this->flush();


        $shcat69 = new ProductCategory();
        $shcat69->setName("Gafas");
        $shcat69->setDescription("Monturas para gafas de hombre");
        $shcat69->setParent($shcat63);
        $this->persist($shcat69, "nakima-shop-product-shcat69");
        $this->flush();


        $shcat70 = new ProductCategory();
        $shcat70->setName("Gafas de sol");
        $shcat70->setDescription("Gafas de sol para hombre");
        $shcat70->setParent($shcat63);
        $this->persist($shcat70, "nakima-shop-product-shcat70");
        $this->flush();


        $shcat71 = new ProductCategory();
        $shcat71->setName("Relojes");
        $shcat71->setDescription("Relojes de hombre");
        $shcat71->setParent($shcat63);
        $this->persist($shcat71, "nakima-shop-product-shcat71");
        $this->flush();


        $shcat72 = new ProductCategory();
        $shcat72->setName("Bolsos y mochilas");
        $shcat72->setDescription("Bolsos de hombre");
        $shcat72->setParent($shcat63);
        $this->persist($shcat72, "nakima-shop-product-shcat72");
        $this->flush();


        $shcat73 = new ProductCategory();
        $shcat73->setName("Bandoleras");
        $shcat73->setDescription("Bandoleras de hombre");
        $shcat73->setParent($shcat72);
        $this->persist($shcat73, "nakima-shop-product-shcat73");
        $this->flush();


        $shcat74 = new ProductCategory();
        $shcat74->setName("Mochilas");
        $shcat74->setDescription("Mochilas de hombre");
        $shcat74->setParent($shcat72);
        $this->persist($shcat74, "nakima-shop-product-shcat74");
        $this->flush();


        $shcat75 = new ProductCategory();
        $shcat75->setName("Monederos");
        $shcat75->setDescription("Monederos, billeteras y tarjeteros de hombre");
        $shcat75->setParent($shcat72);
        $this->persist($shcat75, "nakima-shop-product-shcat75");
        $this->flush();


        $shcat76 = new ProductCategory();
        $shcat76->setName("Joyería");
        $shcat76->setDescription("Joyas y accesorios de hombre");
        $shcat76->setParent($shcat63);
        $this->persist($shcat76, "nakima-shop-product-shcat76");
        $this->flush();


        $shcat77 = new ProductCategory();
        $shcat77->setName("Pulseras");
        $shcat77->setDescription("Pulseras y brazaletes de hombre");
        $shcat77->setParent($shcat76);
        $this->persist($shcat77, "nakima-shop-product-shcat77");
        $this->flush();


        $shcat78 = new ProductCategory();
        $shcat78->setName("Collares");
        $shcat78->setDescription("Collares de hombre");
        $shcat78->setParent($shcat76);
        $this->persist($shcat78, "nakima-shop-product-shcat78");
        $this->flush();


        $shcat79 = new ProductCategory();
        $shcat79->setName("Anillos");
        $shcat79->setDescription("Anillos de hombre");
        $shcat79->setParent($shcat76);
        $this->persist($shcat79, "nakima-shop-product-shcat79");
        $this->flush();


        $shcat80 = new ProductCategory();
        $shcat80->setName("Otros accesorios");
        $shcat80->setDescription("Otros accesorios de hombre");
        $shcat80->setParent($shcat63);
        $this->persist($shcat80, "nakima-shop-product-shcat80");
        $this->flush();


        $shcat81 = new ProductCategory();
        $shcat81->setName("Niño");
        $shcat81->setDescription("Moda, complementos y accesorios para niños");
        $shcat81->setParent($shcat0);
        $this->persist($shcat81, "nakima-shop-product-shcat81");
        $this->flush();


        $shcat82 = new ProductCategory();
        $shcat82->setName("Bebes y niños pequeños");
        $shcat82->setDescription("Moda, complementos y accesorios para niños pequeños");
        $shcat82->setParent($shcat81);
        $this->persist($shcat82, "nakima-shop-product-shcat82");
        $this->flush();


        $shcat83 = new ProductCategory();
        $shcat83->setName("Bebes y niñas pequeñas");
        $shcat83->setDescription("Moda, complementos y accesorios para bebes y niñas pequeñas");
        $shcat83->setParent($shcat82);
        $this->persist($shcat83, "nakima-shop-product-shcat83");
        $this->flush();


        $shcat84 = new ProductCategory();
        $shcat84->setName("Bebes y niños pequeños");
        $shcat84->setDescription("Moda, complementos y accesorios para bebes y niños pequeños");
        $shcat84->setParent($shcat82);
        $this->persist($shcat84, "nakima-shop-product-shcat84");
        $this->flush();


        $shcat85 = new ProductCategory();
        $shcat85->setName("Niños");
        $shcat85->setDescription("Moda, complementos y accesorios para niños");
        $shcat85->setParent($shcat81);
        $this->persist($shcat85, "nakima-shop-product-shcat85");
        $this->flush();


        $shcat86 = new ProductCategory();
        $shcat86->setName("Ropa para niños");
        $shcat86->setDescription("Ropa para niños");
        $shcat86->setParent($shcat85);
        $this->persist($shcat86, "nakima-shop-product-shcat86");
        $this->flush();


        $shcat87 = new ProductCategory();
        $shcat87->setName("Accesorios para niños");
        $shcat87->setDescription("Accesorios y complementos para niños");
        $shcat87->setParent($shcat85);
        $this->persist($shcat87, "nakima-shop-product-shcat87");
        $this->flush();


        $shcat88 = new ProductCategory();
        $shcat88->setName("Niñas");
        $shcat88->setDescription("Moda, complementos y accesorios para niñas");
        $shcat88->setParent($shcat81);
        $this->persist($shcat88, "nakima-shop-product-shcat88");
        $this->flush();


        $shcat89 = new ProductCategory();
        $shcat89->setName("Ropa para niñas");
        $shcat89->setDescription("Ropa para niñas");
        $shcat89->setParent($shcat88);
        $this->persist($shcat89, "nakima-shop-product-shcat89");
        $this->flush();


        $shcat90 = new ProductCategory();
        $shcat90->setName("Accesorios para niñas");
        $shcat90->setDescription("Accesorios y complementos para niñas");
        $shcat90->setParent($shcat88);
        $this->persist($shcat90, "nakima-shop-product-shcat90");
        $this->flush();


        $shcat91 = new ProductCategory();
        $shcat91->setName("En casa");
        $shcat91->setDescription("Cosas de casa para los niños");
        $shcat91->setParent($shcat81);
        $this->persist($shcat91, "nakima-shop-product-shcat91");
        $this->flush();


        $shcat92 = new ProductCategory();
        $shcat92->setName("Decoración");
        $shcat92->setDescription("Decoración para niños");
        $shcat92->setParent($shcat91);
        $this->persist($shcat92, "nakima-shop-product-shcat92");
        $this->flush();


        $shcat93 = new ProductCategory();
        $shcat93->setName("Cuidado");
        $shcat93->setDescription("Cuidado y salud para niños");
        $shcat93->setParent($shcat91);
        $this->persist($shcat93, "nakima-shop-product-shcat93");
        $this->flush();


        $shcat94 = new ProductCategory();
        $shcat94->setName("Juego");
        $shcat94->setDescription("Juegos y juguetes para niños");
        $shcat94->setParent($shcat91);
        $this->persist($shcat94, "nakima-shop-product-shcat94");
        $this->flush();


        $shcat95 = new ProductCategory();
        $shcat95->setName("Lifestyle");
        $shcat95->setDescription("Productos adaptados a tu estilo de vida");
        $shcat95->setParent($shcat0);
        $this->persist($shcat95, "nakima-shop-product-shcat95");
        $this->flush();


        $shcat96 = new ProductCategory();
        $shcat96->setName("Hogar");
        $shcat96->setDescription("Decoración y mobiliaro para el hogar");
        $shcat96->setParent($shcat95);
        $this->persist($shcat96, "nakima-shop-product-shcat96");
        $this->flush();


        $shcat97 = new ProductCategory();
        $shcat97->setName("Decoración");
        $shcat97->setDescription("Decoración para el hogar");
        $shcat97->setParent($shcat96);
        $this->persist($shcat97, "nakima-shop-product-shcat97");
        $this->flush();


        $shcat98 = new ProductCategory();
        $shcat98->setName("Muebles");
        $shcat98->setDescription("Muebles para el hogar");
        $shcat98->setParent($shcat96);
        $this->persist($shcat98, "nakima-shop-product-shcat98");
        $this->flush();


        $shcat99 = new ProductCategory();
        $shcat99->setName("Téxtil");
        $shcat99->setDescription("Téxtil para el hogar");
        $shcat99->setParent($shcat96);
        $this->persist($shcat99, "nakima-shop-product-shcat99");
        $this->flush();


        $shcat100 = new ProductCategory();
        $shcat100->setName("Menaje");
        $shcat100->setDescription("Menaje para comer y cocinar");
        $shcat100->setParent($shcat96);
        $this->persist($shcat100, "nakima-shop-product-shcat100");
        $this->flush();


        $shcat101 = new ProductCategory();
        $shcat101->setName("Iluminación");
        $shcat101->setDescription("Iluminación para el hogar");
        $shcat101->setParent($shcat96);
        $this->persist($shcat101, "nakima-shop-product-shcat101");
        $this->flush();


        $shcat102 = new ProductCategory();
        $shcat102->setName("Orden");
        $shcat102->setDescription("Organizacion y almacenaje para el hogar");
        $shcat102->setParent($shcat96);
        $this->persist($shcat102, "nakima-shop-product-shcat102");
        $this->flush();


        $shcat103 = new ProductCategory();
        $shcat103->setName("Oficina");
        $shcat103->setDescription("Materiales y accesorios para la oficina");
        $shcat103->setParent($shcat96);
        $this->persist($shcat103, "nakima-shop-product-shcat103");
        $this->flush();


        $shcat104 = new ProductCategory();
        $shcat104->setName("Tecnología");
        $shcat104->setDescription("Productos y accesorios de tecnología");
        $shcat104->setParent($shcat95);
        $this->persist($shcat104, "nakima-shop-product-shcat104");
        $this->flush();


        $shcat105 = new ProductCategory();
        $shcat105->setName("Imagen y sonido");
        $shcat105->setDescription("Dispositivos y accesorios de imagen y sonido");
        $shcat105->setParent($shcat104);
        $this->persist($shcat105, "nakima-shop-product-shcat105");
        $this->flush();


        $shcat106 = new ProductCategory();
        $shcat106->setName("Gadgets y accesorios");
        $shcat106->setDescription("Dispostivos y accesorios tecnológicos");
        $shcat106->setParent($shcat104);
        $this->persist($shcat106, "nakima-shop-product-shcat106");
        $this->flush();

        $shcat000 = new ProductCategory();
        $shcat000->setName("Otros");
        $shcat000->setDescription("Otros");
        $this->persist($shcat000, "nakima-shop-product-shcat000");
        $this->flush();


    }

}

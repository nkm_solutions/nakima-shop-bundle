<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adriá Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass(
 *     repositoryClass="Nakima\ShopBundle\Repository\ProductCombinationRepository"
 * )
 */
class ProductCombination extends BaseEntity
{

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Discount",
     *     cascade={"persist"}
     * )
     * @JoinColumn(name="discount_id", referencedColumnName="id")
     */
    private $discount;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Product",
     *     inversedBy="combinations"
     * )
     * @JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ManyToMany(
     *     targetEntity="ProductAttributeValue",
     *     inversedBy="productCombinations"
     * )
     * @JoinTable(name="_productcombinations_productattributevalues")
     */
    protected $productAttributeValues;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="logo_id", referencedColumnName="id")
     */
    protected $logo;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="icon_id", referencedColumnName="id")
     */
    protected $icon;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductCombinationPromotion",
     *     mappedBy="productCombination"
     * )
     */
    protected $promotions;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $haveBetterOffer;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $saleDiscount;

    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __toString()
    {
        return "C";
    }

    public function __toArray(array $options = []): array
    {
        $prodCombArray = [
            'id' => $this->getId(),
            'discount' => $this->getDiscount(),
            'saleDiscount' => $this->getSaleDiscount(),
            'haveBetterOffer' => $this->getHaveBetterOffer(),
            //'logo' => Doctrine::toArray($this->getLogo()),
            //'icon' => Doctrine::toArray($this->getIcon()),
        ];

        if ($options['children'] ?? true) {
            $prodCombArray['product'] = Doctrine::toArray(
                $this->getProduct(),
                ['children' => false]
            );
            $prodCombArray['attributeValues'] = Doctrine::toArray(
                $this->getProductAttributeValues()
            );
            $prodCombArray['promotions'] = Doctrine::toArray($this->getPromotions());
        }

        return $prodCombArray;
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {

        parent::__construct();
        $this->productAttributeValues = new ArrayCollection();
        $this->logo = new \MediaBundle\Entity\Media;
        $this->icon = new \MediaBundle\Entity\Media;
        $this->promotions = new ArrayCollection;
        $this->setDiscount(new \ShopBundle\Entity\Discount());
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product)
    {
        $this->product = $product;
        $this->calculateSaleDiscount();

        return $this;
    }

    public function getProductAttributeValues()
    {
        return $this->productAttributeValues;
    }

    public function addProductAttributeValue($prodAttbValue)
    {
        $this->productAttributeValues[] = $prodAttbValue;

        return $this;
        $paId = $prodAttbValue->getProductAttribute()->getId();
        foreach ($this->getProduct()->getAttributes() as $attribute) {
            if ($paId === $attribute->getId()) {
                $this->productAttributeValues[] = $prodAttbValue;
                break;
            }
        }

        return $this;
    }

    public function removeProductAttributeValue($prodAttbValue)
    {
        $this->productAttributeValues->removeElement($prodAttbValue);

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        $this->calculateSaleDiscount();

        return $this;
    }

    public function setupDiscount($amount, $isAbsolute = true)
    {
        $discount = $this->getDiscount();
        $discount->setAbsolute($isAbsolute);
        $discount->setDiscount($amount);
        $this->calculateSaleDiscount();

        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo(\MediaBundle\Entity\Media $logo)
    {
        $this->logo = $logo;

        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon(\MediaBundle\Entity\Media $icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function addPromotion($promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    public function removePromotion($promotion)
    {
        $this->promotions->removeElement($promotion);

        return $this;
    }

    public function getSaleDiscount()
    {
        return $this->saleDiscount;
    }

    public function setSaleDiscount($saleDiscount)
    {
        $this->saleDiscount = $saleDiscount;

        return $this;
    }

    public function getHaveBetterOffer()
    {
        return $this->haveBetterOffer;
    }

    public function setHaveBetterOffer($haveBetterOffer)
    {
        $this->haveBetterOffer = $haveBetterOffer;

        return $this;
    }

    /**************************************************************************
     * Private Functions                                                      *
     **************************************************************************/

    protected function calculateSaleDiscount()
    {
        $prod = $this->getProduct();
        if ($prod) {
            $basePrice = $prod->getPrice();
            $prodDisc = $prod->getDiscount();
            $combDisc = $this->getDiscount();

            $prodDiscAbs = 0;
            if ($prodDisc) {
                $prodDiscAbs = $prodDisc->applyDiscount($basePrice);
            }
            $combDiscAbs = 0;
            if ($combDisc) {
                $combDiscAbs = $combDisc->applyDiscount($basePrice);
            }

            if ($combDiscAbs > $prodDiscAbs) {
                $this->setSaleDiscount($combDiscAbs);
                $this->setHaveBetterOffer(true);
            } else {
                $this->setSaleDiscount($prodDiscAbs);
                $this->setHaveBetterOffer(false);
            }
        }
    }
}

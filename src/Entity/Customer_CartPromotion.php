<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @MappedSuperclass
 * @UniqueEntity(
 *     fields={"customer", "cartPromotion"}
 * )
 */
class Customer_CartPromotion extends BaseEntity
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Customer"
     * )
     * @JoinColumn(
     *     name="customer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customer;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\CartPromotion"
     * )
     * @JoinColumn(
     *     name="cartpromotion_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $cartPromotion;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $usages;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::toArray($options);
        $retArray["customer"] = Doctrine::toArray(
            $this->getCustomer(),
            ['children' => false]
        );
        $retArray["cartPromotion"] = Doctrine::toArray(
            $this->getCartPromotion(),
            ['children' => false]
        );
        $retArray["usages"] = $this->getUsages();

        return $retArray;
    }

    public function incUsages($num = false)
    {
        if (!$num) {
            $num = 1;
        }
        $newValue = $this->getUsages() + $num;
        $this->setUsages($newValue);

        return $this;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setUsages(0);
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCartPromotion()
    {
        return $this->cartPromotion;
    }

    public function setCartPromotion($cartPromotion)
    {
        $this->cartPromotion = $cartPromotion;

        return $this;
    }

    public function getUsages()
    {
        return $this->usages;
    }

    public function setUsages($usages)
    {
        $this->usages = $usages;

        return $this;
    }

}

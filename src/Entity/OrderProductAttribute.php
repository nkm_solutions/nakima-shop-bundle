<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass(repositoryClass="Nakima\ShopBundle\Repository\ProductAttributeValueRepository")
 */
class OrderProductAttribute extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Column(type="string", length=64)
     */
    protected $value;

    /**
     * @Column(type="string", length=64)
     */
    protected $alternativeValue;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\ProductAttributeValue")
     * @JoinColumn(onDelete="SET NULL")
     */
    protected $original;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString()
    {
        return "$this->name: $this->value";
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'alternativeValue' => $this->getAlternativeValue(),
            'name' => $this->getName(),
            'original' => Doctrine::toArray($this->getOriginal())
        ];
    }

    function __clone()
    {
        $this->id = null;
    }

    public static function create(ProductAttributeValue $pav): OrderProductAttribute
    {
        $newPav = new \ShopBundle\Entity\OrderProductAttribute();
        $newPav->setAlternativeValue($pav->getAlternativeValue() ?? $pav->getValue());
        $newPav->setName($pav->getProductAttribute()->getName());
        $newPav->setValue($pav->getValue());
        $newPav->setOriginal($pav);

        return $newPav;

    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }


    public function getOriginal(): ProductAttributeValue
    {
        return $this->original;
    }

    public function setOriginal(ProductAttributeValue $original): self
    {
        $this->original = $original;
        return $this;
    }

    public function getAlternativeValue(): string
    {
        return $this->alternativeValue;
    }

    public function setAlternativeValue(string $alternativeValue): self
    {
        $this->alternativeValue = $alternativeValue;
        return $this;
    }
}

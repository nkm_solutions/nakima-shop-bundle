<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\Time\DateTime;

/**
 * @MappedSuperclass
 */
abstract class Promotion extends BaseEntity
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Campaign",
     *     inversedBy="promotions"
     * )
     * @JoinColumn(
     *     name="campaign_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $campaign;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Discount",
     *     cascade={"persist"}
     * )
     * @JoinColumn(
     *     name="discount_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $discount;

    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setDiscount(new \ShopBundle\Entity\Discount());
    }

    public function __toArray(array $options = []): array
    {

        return [
            'id'       => $this->getId(),
            'campaign' => Doctrine::toArray(
                $this->getCampaign(),
                ['children' => false]
            ),
            'discount' => Doctrine::toArray($this->getDiscount()),
        ];
    }


    /**************************************************************************
     *                                                                        *
     *   LifecycleCallbacks                                                   *
     *                                                                        *
     **************************************************************************/

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getCampaign()
    {
        return $this->campaign;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        if (!$this->discount) {
            $this->setDiscount(new \ShopBundle\Entity\Discount);
        }
    }

    public function isOnTime()
    {
        $now = new DateTime;

        return $this->getCampaign()->isOnTime();
    }

    public function setupDiscount($amount, $isAbsolute = true)
    {
        $discount = $this->getDiscount();
        $discount->setAbsolute($isAbsolute);
        $discount->setDiscount($amount);

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;

/**
 * @MappedSuperclass
 */
class Discount extends BaseEntity
{

    /**
     * @Column(type="boolean")
     */
    protected $absolute;

    /**
     * @Column(type="float")
     */
    protected $discount;

    /**
     * @Column(type="string", length=128, nullable=false)
     */
    protected $display;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->getDisplay();
    }

    public function __toArray(array $options = []): array
    {

        return [
            'id' => $this->getId(),
            'absolute' => $this->getAbsolute(),
            'discount' => $this->getDiscount(),
            'display' => $this->getDisplay(),
        ];
    }

    public function __construct()
    {
        parent::__construct();
        $this->setDisplay("0€");
        $this->setAbsolute(true);
        $this->setDiscount(0.0);
    }

    public function applyDiscount($price)
    {
        $newPrice = 0.0;
        if ($this->getAbsolute()) {
            $newPrice = $this->getDiscount();
        } else {
            $newPrice = $price * $this->getDiscount() / 100;
        }

        return $newPrice;
    }


    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getAbsolute()
    {
        return $this->absolute;
    }

    public function setAbsolute($absolute)
    {
        $this->absolute = $absolute;
        $this->updateDisplay();

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        $this->updateDisplay();

        return $this;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setDisplay($display)
    {
        if ($display !== null) {
            $this->display = $display;
        }

        return $this;
    }

    /**************************************************************************
     * Private Functions                                                      *
     **************************************************************************/

    protected function updateDisplay()
    {
        $absolute = $this->getAbsolute();
        $discount = $this->getDiscount();

        if ($discount) {
            $display = strval(round($discount * 100) / 100);
            if ($absolute) {
                $display = $display."€";
            } else {
                $display = $display."%";
            }
            $this->setDisplay($display);
        }
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\Time\DateTime;

/**
 * @MappedSuperclass
 * _AdminAll(role | ownable | type | editable | required)
 * _AdminEdit(role | ownable | type | editable | required)
 * _AdminDelete(role | ownable | type | editable | required)
 * _AdminList(role | ownable | type | editable | required)
 * _AdminShow(role | ownable | type | editable | required)
 * _Admin(action | role | ownable | type | editable | required)
 */
class Campaign extends BaseEntity
{

    /**
     * @Column(type="string", length=128, unique=true, nullable=false)
     * _AdminAll(role | ownable | type | editable | required)
     * _AdminEdit(role | ownable | type | editable | required)
     * _AdminList(role | ownable | type | editable | required)
     * _AdminShow(role | ownable | type | editable | required)
     * _Admin(action | role | ownable | type | editable | required)
     */
    protected $name;

    /**
     * @Column(type="nakima_datetime", nullable=false)
     */
    protected $fromDate;

    /**
     * @Column(type="nakima_datetime", nullable=false)
     */
    protected $toDate;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\Promotion",
     *     mappedBy="campaign"
     * )
     */
    protected $promotions;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->getName();
    }

    public function __toArray(array $options = []): array
    {

        $campaignArray = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'fromDate' => $this->getFromDate(),
            'toDate' => $this->getToDate(),
            'enabled' => $this->getEnabled(),
        ];

        if ($options['children'] ?? true) {
            $campaignArray['code'] = Doctrine::toArray($this->getCode());
            $campaignArray['promotions'] = Doctrine::toArray($this->getPromotions());
        }

        return $campaignArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(false);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFromDate()
    {
        return $this->fromDate;
    }

    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getToDate()
    {
        return $this->toDate;
    }

    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function isOnTime()
    {
        $now = new DateTime;

        return $now->isBetween($this->getFromDate(), $this->getToDate());
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function addPromotion($promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    public function removePromotion($promotion)
    {
        $this->promotions->removeElement($promotion);

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
class HTOCart extends BaseEntity
{

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Customer",
     *     inversedBy="htoCart"
     * )
     * @JoinColumn(
     *     name="customer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customer;

    /**
     * @ManyToMany(
     *     targetEntity="ShopBundle\Entity\CartProduct",
     *     cascade={"all"}
     * )
     * @JoinTable(
     *     name="_htocarts_products",
     *     joinColumns={
     *         @JoinColumn(
     *             name="cart_id",
     *             referencedColumnName="id"
     *         )
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(
     *             name="cartproduct_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     },
     * )
     */
    protected $cartProducts;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ShippingMethod"
     * )
     * @JoinColumn(
     *     name="shippingmethod_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $shippingMethod;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $shippingPrice;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $productsPrice;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $totalPrice;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->cartProducts = new ArrayCollection;
        $this->setDefaults();
    }

    public function setDefaults()
    {
        $this->setShippingMethod(
            \ShopBundle\Entity\ShippingMethod::load('Standard')
        );
        $this->setShippingPrice(0.0);
        $this->setProductsPrice(0.0);
        $this->setTotalPrice(0.0);

        return $this;
    }

    public function __toArray(array $options = []): array
    {
        $cartProducts = $this->getCartProducts();

        $total = 0;
        foreach ($cartProducts as $cartProd) {
            $total += $cartProd->getQuantity();
        }

        $cartArray = [
            'id' => $this->getId(),
            'total' => $total,
            'unique' => count($cartProducts),
            'productsPrice' => $this->getProductsPrice(),
            'totalPrice' => $this->getTotalPrice(),
            'shippingMethod' => Doctrine::toArray($this->getShippingMethod()),
            'shippingPrice' => $this->getShippingPrice(),
        ];

        if ($options['children'] ?? true) {
            $cartArray['cartProducts'] = Doctrine::toArray($cartProducts);
        }

        return $cartArray;
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCartProducts()
    {
        return $this->cartProducts;
    }

    public function addCartProduct($cartProduct)
    {
        $cartProducts = $this->getCartProducts();
        $customer = $this->getCustomer();

        if (count($cartProducts) < $customer->getHtoCartMaxProd()) {
            $prodCombId = $cartProduct->getProductCombination()->getId();

            $found = false;
            foreach ($cartProducts as $cartProd) {
                if ($prodCombId == $cartProd->getProductCombination()->getId()) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $this->cartProducts[] = $cartProduct;
            }
        }

        $this->updatePrices();

        return $this;
    }

    public function removeCartProduct($cartProduct)
    {
        $this->cartProducts->removeElement($cartProduct);
        $this->updatePrices();

        return $this;
    }

    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
        $this->updatePrices();

        return $this;
    }

    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;

        return $this;
    }

    public function getProductsPrice()
    {
        return $this->productsPrice;
    }

    public function setProductsPrice($productsPrice)
    {
        $this->productsPrice = $productsPrice;

        return $this;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**************************************************************************
     * More Custom Functions                                                  *
     **************************************************************************/

    public function updatePrices()
    {
        $shippingMethod = $this->getShippingMethod();
        $shippingBasePrice = $shippingMethod->getPrice();
        $shippingExtraPrice = $shippingMethod->getBrandExtraPrice();

        $productsPrice = 0.0;
        $brandMapping = [];
        foreach ($this->getCartProducts() as $cartProduct) {
            // Calculate shippingPrice
            $brandId = $cartProduct->getProduct()->getBrand()->getId();
            if (!array_key_exists($brandId, $brandMapping)) {
                $brandMapping[$brandId] = [];
            }
            $brandMapping[$brandId][] = $cartProduct->getId();

            // Update Prices
            $quantity = $cartProduct->getQuantity();
            $finalPrice = $cartProduct->getFinalPrice();

            $productsPrice += $finalPrice * $quantity;
        }
        $this->setProductsPrice($productsPrice);

        $diffBrandsNumb = count($brandMapping);
        if ($productsPrice > 69.0) {
            $this->setShippingPrice(0.0);
        } else {
            $extraBrands = $diffBrandsNumb * $shippingExtraPrice;
            $shippingPrice = $shippingBasePrice + $extraBrands;
            $this->setShippingPrice($shippingPrice);
        }

        $this->setTotalPrice($productsPrice + $this->getShippingPrice());
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Utils\Doctrine;
use ShopBundle\Entity\Promotion as BasePromotion;

/**
 * @MappedSuperclass
 */
class ProductPromotion extends BasePromotion
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Product",
     *     inversedBy="promotions"
     * )
     * @JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id"
     * )
     */
    protected $product;

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::__toArray($options);
        $retArray["product"] = Doctrine::toArray(
            $this->getProduct(),
            ['children' => false]
        );

        return $retArray;
    }

    public function __toString()
    {
        $code = $this->product->getName();
        $campaign = $this->getCampaign()->getName();

        return "$code ($campaign)";
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

}

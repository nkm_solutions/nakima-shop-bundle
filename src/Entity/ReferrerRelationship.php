<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @MappedSuperclass
 * @UniqueEntity(
 *     fields={"referrer", "referent"}
 * )
 */
class ReferrerRelationship extends BaseEntity
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Customer"
     * )
     * @JoinColumn(
     *     name="referrer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $referrer;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Customer"
     * )
     * @JoinColumn(
     *     name="referent_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $referent;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::toArray($options);

        $retArray["referrer"] = Doctrine::toArray(
            $this->getReferrer(),
            ['children' => false]
        );
        $retArray["referent"] = Doctrine::toArray(
            $this->getReferent(),
            ['children' => false]
        );

        return $retArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function getReferrer()
    {
        return $this->referrer;
    }

    public function setReferrer(\ShopBundle\Entity\Customer $referrer = null)
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getReferent()
    {
        return $this->referent;
    }

    public function setReferent(\ShopBundle\Entity\Customer $referent = null)
    {
        $this->referent = $referent;

        return $this;
    }

}

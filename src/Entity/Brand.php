<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;

/**
 * @MappedSuperclass(
 *     repositoryClass="Nakima\ShopBundle\Repository\BrandRepository"
 * )
 * @HasLifecycleCallbacks
 */
class Brand extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Column(type="string", length=64, unique=true)
     */
    protected $slug;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="logo_id", referencedColumnName="id")
     */
    protected $logo;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="icon_id", referencedColumnName="id")
     */
    protected $icon;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="cover_id", referencedColumnName="id")
     */
    protected $cover;

    /**
     * @OneToMany(targetEntity="ShopBundle\Entity\Shop", mappedBy="brand")
     */
    protected $shops;

    /**
     * @OneToMany(targetEntity="ShopBundle\Entity\Product", mappedBy="brand")
     */
    protected $products;

    /**
     * @Column(type="boolean")
     */
    protected $enabled;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\BrandPromotion",
     *     mappedBy="brand"
     * )
     */
    protected $promotions;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Provider",
     *     inversedBy="brands"
     * )
     * @JoinColumn(
     *     name="provider_id",
     *     referencedColumnName="id"
     * )
     */
    protected $provider;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->promotions = new ArrayCollection;
        $this->products = new ArrayCollection;
        $this->shops = new ArrayCollection;
        $this->setEnabled(true);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        $brandArray = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
            'description' => $this->getDescription(),
            'enabled' => $this->getEnabled(),
            'position' => $this->getPosition(),
            'shops' => Doctrine::toArray($this->getShops(), ['children' => false]),
            'logo' => Doctrine::toArray($this->getLogo()),
            'icon' => Doctrine::toArray($this->getIcon()),
            'cover' => Doctrine::toArray($this->getCover()),
        ];

        if ($options['children'] ?? true) {
            $brandArray['promotions'] = Doctrine::toArray($this->getPromotions());
        }

        return $brandArray;
    }

    /**************************************************************************
     *                                                                        *
     *   LifecycleCallbacks                                                   *
     *                                                                        *
     **************************************************************************/

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        if (!$this->logo) {
            $this->logo = new \MediaBundle\Entity\Media;
        }
        if (!$this->icon) {
            $this->icon = new \MediaBundle\Entity\Media;
        }
        if (!$this->cover) {
            $this->cover = new \MediaBundle\Entity\Media;
        }
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->setSlug($name);

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = Text::slugify($slug);

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getCover()
    {
        return $this->cover;
    }

    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function addShop(\ShopBundle\Entity\Shop $shop)
    {
        $this->shops[] = $shop;

        return $this;
    }

    public function removeShop(\ShopBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
    }

    public function getShops()
    {
        return $this->shops;
    }

    public function addProduct(\ShopBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    public function removeProduct(\ShopBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function addPromotion($promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    public function removePromotion($promotion)
    {
        $this->promotions->removeElement($promotion);

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider($provider)
    {
        if (!$provider || $provider == $this->provider) {
            return;
        }
        $this->provider = $provider;
        $provider->addBrand($this);

        return $this;
    }

}

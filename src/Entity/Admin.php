<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use UserBundle\Entity\Role;

/**
 * @MappedSuperclass
 */
class Admin extends \ShopBundle\Entity\Customer
{

    public function __construct()
    {
        parent::__construct();
        $this->addRole(Role::loadRole('ROLE_SHOP_ADMIN'));
        $this->setName("admin");
        $this->setSurnames("admin");
    }
}

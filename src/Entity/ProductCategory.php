<?php
declare(strict_types=1);

namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Entity\BaseStatusSlugEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @MappedSuperclass(
 *     repositoryClass="Nakima\ShopBundle\Repository\ProductCategoryRepository"
 * )
 * @HasLifecycleCallbacks()
 */
class ProductCategory extends BaseStatusSlugEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductCategory",
     *     mappedBy="parent"
     * )
     */
    protected $subcategories;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ProductCategory",
     *     inversedBy="subcategories",
     *     cascade={"all"}
     * )
     * @JoinColumn(
     *     name="parent_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
    protected $parent;

    /**
     * @OneToOne(targetEntity="MediaBundle\Entity\Media", cascade={"all"})
     * @JoinColumn(name="smallimage_id", referencedColumnName="id")
     */
    protected $smallImage;

    /**
     * @OneToOne(targetEntity="MediaBundle\Entity\Media", cascade={"all"})
     * @JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductCategoryPromotion",
     *     mappedBy="category"
     * )
     */
    protected $promotions;

    /******************************************************************************************************************
     *                                                                                                                *
     * Custom Functions                                                                                               *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function __toString()
    {

        if ($this->getParent()) {
            return $this->getParent()->__toString()." > ".$this->name;
        }

        return "$this->name";
    }

    public function __toArray(array $options = []): array
    {
        $ctgArray = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'slug' => $this->getSlug(),
            'description' => $this->getDescription(),
            'position' => $this->getPosition(),
            'image' => Doctrine::toArray($this->getImage()),
            'smallImage' => Doctrine::toArray($this->getSmallImage()),
            'subcategories' => Doctrine::toArray(
                $this->getSubcategories(),
                ['children' => false]
            ),
        ];

        if ($options['children'] ?? true) {
            $ctgArray['promotions'] = Doctrine::toArray($this->getPromotions());
        }

        return $ctgArray;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $catgId = $this->getId();

        $parent = $this->getParent();
        while ($parent) {
            if ($parent->getId() === $catgId) {
                $context
                    ->buildViolation('Parent already content inside subcategories')
                    ->atPath('parent')
                    ->addViolation();
                break;
            }
            $parent = $parent->getParent();
        }
    }

    public function __construct()
    {
        parent::__construct();
        $this->subcategories = new ArrayCollection();
        $this->setDescription("");
        $this->promotions = new ArrayCollection;
    }


    /******************************************************************************************************************
     *                                                                                                                *
     * Getters & Setters                                                                                              *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = ucfirst(strtolower($name));

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        if ($description == null) {
            $description = "";
        }
        $this->description = $description;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getSubcategories()
    {
        return $this->subcategories;
    }

    public function addSubcategory($subcaregory)
    {
        $this->subcategories[] = $subcaregory;

        return $this;
    }

    public function removeSubcategory($subcaregory)
    {
        $this->subcategories->removeElement($subcaregory);

        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        if ($parent) {
            $parent->addSubcategory($this);
        }

        return $this;
    }

    public function getImage()
    {

        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getSmallImage()
    {
        return $this->smallImage;
    }

    public function setSmallImage($smallImage)
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function addPromotion($promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    public function removePromotion($promotion)
    {
        $this->promotions->removeElement($promotion);

        return $this;
    }

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        parent::preCreate();
        if (!$this->image) {
            $this->image = new \MediaBundle\Entity\Media;
        }
    }

}

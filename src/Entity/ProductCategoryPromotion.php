<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Utils\Doctrine;
use ShopBundle\Entity\Promotion as BasePromotion;

/**
 * @MappedSuperclass
 */
class ProductCategoryPromotion extends BasePromotion
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ProductCategory",
     *     inversedBy="promotions"
     * )
     * @JoinColumn(
     *     name="category_id",
     *     referencedColumnName="id"
     * )
     */
    protected $category;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::toArray($options);
        $retArray["category"] = Doctrine::toArray(
            $this->getCategory(),
            ['children' => false]
        );

        return $retArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
class CampaignCode extends BaseEntity
{

    /**
     * @Column(type="string", length=128, unique=true, nullable=false)
     */
    protected $code;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $isLimited;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Campaign",
     *     mappedBy="code"
     * )
     */
    private $campaign;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->getCode();
    }

    public function __toArray(array $options = []): array
    {

        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'isLimited' => $this->getIsLimited(),
            'campaign' => Doctrine::toArray(
                $this->getCampaign(),
                ['children' => false]
            ),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setIsLimited(true);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getIsLimited()
    {
        return $this->isLimited;
    }

    public function setIsLimited($isLimited)
    {
        $this->isLimited = $isLimited;

        return $this;
    }

    public function getCampaign()
    {
        return $this->campaign;
    }

    public function setCampaign($campaign)
    {
        $this->campaign = $campaign;

        return $this;
    }

}

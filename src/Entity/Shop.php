<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PostPersist;
use Doctrine\ORM\Mapping\PrePersist;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\Utils\String\Text;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 */
class Shop extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @Column(type="text")
     */
    protected $summary;

    /**
     * @ManyToOne(
     *     targetEntity="AddressBundle\Entity\Address", cascade={"persist"}
     * )
     * @JoinColumn(
     *     name="address_id",
     *     referencedColumnName="id",
     *     nullable=false
     *
     * )
     */
    protected $address;

    /**
     * @Column(type="string", length=64)
     */
    protected $telephone;

    /**
     * @Column(type="string", length=64)
     * @Assert\Url()
     */
    protected $web;

    /**
     * @Column(type="string", length=1024)
     */
    protected $schedule;

    /**
     * @Column(type="string", length=64)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="logo_id", referencedColumnName="id")
     */
    protected $logo;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="icon_id", referencedColumnName="id")
     */
    protected $icon;

    /**
     * @OneToOne(targetEntity="MediaBundle\Entity\Gallery", cascade={"all"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    protected $gallery;

    /**
     * @ManyToMany(targetEntity="ShopBundle\Entity\ShippingMethod")
     * @JoinTable(
     *     name="_shops_shippingmethods",
     *     joinColumns={
     *         @JoinColumn(name="shop_id",referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="shippingmethod_id",referencedColumnName="id")
     *     }
     * )
     */
    protected $shippingMethods;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Brand",
     *     inversedBy="shops"
     * )
     * @JoinColumn(
     *     name="brand_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $brand;

    /**
     * @ManyToMany(
     *     targetEntity="ShopBundle\Entity\Product",
     *     mappedBy="shops",
     *     cascade={"all"}
     * )
     */
    protected $products;

    /**
     * @Column(type="boolean")
     */
    protected $enabled;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\ShopManager",
     *     inversedBy="shop"
     * )
     * @JoinColumn(
     *     name="shopmanager_id",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $shopManager;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Provider",
     *     inversedBy="shops"
     * )
     * @JoinColumn(
     *     name="provider_id",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    protected $provider;

    /**
     * @Column(type="string", length=8, unique=true)
     */
    protected $showeaCode;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        $shopArray = parent::__toArray($options);

        $shopArray['description'] = $this->getDescription();
        $shopArray['email'] = $this->getEmail();
        $shopArray['enabled'] = $this->getEnabled();
        $shopArray['name'] = $this->getName();
        $shopArray['telephone'] = $this->getTelephone();
        $shopArray['web'] = $this->getWeb();

        if ($options['children'] ?? true) {
            $shopArray['address'] = Doctrine::toArray($this->getAddress());
            $shopArray['brand'] = Doctrine::toArray($this->getBrand());
            $shopArray['gallery'] = Doctrine::toArray($this->getGallery());
            $shopArray['icon'] = Doctrine::toArray($this->getIcon());
            $shopArray['logo'] = Doctrine::toArray($this->getLogo());
            $shopArray['shippingMethods'] = Doctrine::toArray($this->getShippingMethods());
        }

        return $shopArray;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setShoweaCode(Text::rstr(8));
        $this->products = new ArrayCollection;
        $this->setEnabled(true);
        $this->setSummary("");
        $this->setSchedule("");
        $this->shippingMethods = new ArrayCollection;
        $this->addShippingMethod(\ShopBundle\Entity\ShippingMethod::load("Standard"));
        $this->addShippingMethod(\ShopBundle\Entity\ShippingMethod::load("Express"));
    }

    /**************************************************************************
     *                                                                        *
     *   LifecycleCallbacks                                                   *
     *                                                                        *
     **************************************************************************/

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        if (!$this->gallery) {
            $this->gallery = new \MediaBundle\Entity\Gallery;
        }
        if (!$this->icon) {
            $this->icon = new \MediaBundle\Entity\Media;
        }
        if (!$this->logo) {
            $this->logo = new \MediaBundle\Entity\Media;
        }
    }

    /**
     * @PostPersist
     */
    public function postPersist()
    {

        $admins = Doctrine::getRepo("ShopBundle:Admin")->findAll();

        foreach ($admins as $key => $value) {
            Mailer::newInstance(
                "Modificación sobre el pedido realizado",
                "no-reply@showea.com",
                $value->getEmail(),
                'NakimaShopBundle:Emails:new_shop.html.twig',
                [
                    'shop' => $this,
                ]
            );
        }

        Mailer::send();
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress(\AddressBundle\Entity\Address $address)
    {
        $this->address = $address;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getWeb()
    {
        return $this->web;
    }

    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo(\MediaBundle\Entity\Media $logo)
    {
        $this->logo = $logo;

        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon(\MediaBundle\Entity\Media $icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function setGallery(\MediaBundle\Entity\Gallery $gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function addShippingMethod(?\ShopBundle\Entity\ShippingMethod $shippingMethod)
    {
        if (!$shippingMethod) {
            return $this;
        }

        if (!$this->shippingMethods->contains($shippingMethod)) {
            $this->shippingMethods[] = $shippingMethod;
        }

        return $this;
    }

    public function removeShippingMethod(\ShopBundle\Entity\ShippingMethod $shippingMethod)
    {
        $this->shippingMethods->removeElement($shippingMethod);
    }

    public function getShippingMethods()
    {
        return $this->shippingMethods;
    }

    public function addProduct(\ShopBundle\Entity\Product $product)
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(\ShopBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getShopManager()
    {
        return $this->shopManager;
    }

    public function setShopManager($shopManager)
    {
        $this->shopManager = $shopManager;

        return $this;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider($provider)
    {
        if (!$provider) {
            return;
        }
        $this->provider = $provider;
        $provider->addShop($this);

        return $this;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }

    public function setSchedule($schedule)
    {
        if ($schedule === null) {
            return;
        }
        $this->schedule = $schedule;

        return $this;
    }

    public function getShoweaCode()
    {
        return $this->showeaCode;
    }

    public function setShoweaCode($showeaCode)
    {
        $this->showeaCode = $showeaCode;

        return $this;
    }
}

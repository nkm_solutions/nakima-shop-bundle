<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass(repositoryClass="Nakima\ShopBundle\Repository\ProductAttributeValueRepository")
 */
class ProductAttributeValue extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $value;

    /**
     * @Column(type="string", length=64)
     */
    protected $alternativeValue;

    /**
     * @Column(type="integer")
     */
    protected $position;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ProductAttribute",
     *     inversedBy="attributeValues",
     *     cascade={"all"}
     * )
     * @JoinColumn(
     *     name="productattribute_id",
     *     referencedColumnName="id",
     *     nullable=false,
     *     onDelete="CASCADE"
     * )
     */
    protected $productAttribute;

    /**
     * @ManyToMany(
     *     targetEntity="ProductCombination",
     *     mappedBy="productAttributeValues"
     * )
     */
    protected $productCombinations;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->position = 0;
    }

    public function __toString()
    {
        return $this->getProductAttribute()->__toString().":".$this->value;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'alternativeValue' => $this->getAlternativeValue(),
            'fqn' => $this->getProductAttribute()
                    ->getName().":".$this->getValue(),
            'productAttribute' => Doctrine::toArray(
                $this->getProductAttribute(),
                ['novalues' => true]
            ),
        ];
    }

    public static function create($value, $alternativeValue, $position = 0)
    {
        $ret = new \ShopBundle\Entity\ProductAttributeValue;
        $ret->setValue($value);
        $ret->setAlternativeValue($alternativeValue);
        $ret->setPosition($position);

        return $ret;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function getProductAttribute()
    {
        return $this->productAttribute;
    }

    public function setProductAttribute($productAttribute)
    {
        $this->productAttribute = $productAttribute;

        //$productAttribute->addAttributeValue($this);
        return $this;
    }

    public function getAlternativeValue()
    {
        return $this->alternativeValue;
    }

    public function setAlternativeValue($alternativeValue)
    {
        $this->alternativeValue = $alternativeValue;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getProductCombinations()
    {
        return $this->productCombinations;
    }

    public function addProductCombination($productCombination)
    {
        $this->productCombinations[] = $productCombination;

        return $this;
    }

    public function removeProductCombination($productCombination)
    {
        $this->productCombinations->removeElement($productCombination);

        return $this;
    }
}

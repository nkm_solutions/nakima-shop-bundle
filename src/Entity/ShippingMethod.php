<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseStatusEntity;

/**
 * @MappedSuperclass
 */
class ShippingMethod extends BaseStatusEntity
{

    /**
     * @Column(type="float", nullable=false)
     */
    protected $price;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $brandExtraPrice;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $info;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setBrandExtraPrice(1.0);
        $this->setEnabled(true);
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'brandExtraPrice' => $this->getBrandExtraPrice(),
            'enabled' => $this->getEnabled(),
            'description' => $this->getDescription(),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getBrandExtraPrice()
    {
        return $this->brandExtraPrice;
    }

    public function setBrandExtraPrice($brandExtraPrice)
    {
        $this->brandExtraPrice = $brandExtraPrice;

        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        if ($description !== null) {
            $this->description = $description;
        }

        return $this;
    }
}

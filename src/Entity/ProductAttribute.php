<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks()
 */
class ProductAttribute extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductAttributeValue",
     *     mappedBy="productAttribute",
     *     cascade={"all"}
     * )
     */
    protected $attributeValues;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\ProductAttributeType")
     * @JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->attributeValues = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        if ($options['novalues'] ?? false) {
            return [
                'id' => $this->getId(),
                'name' => $this->getName(),
                'type' => Doctrine::toArray($this->getType()),
            ];
        }

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'values' => Doctrine::toArray($this->getAttributeValues()),
            'type' => Doctrine::toArray($this->getType()),
        ];
    }

    public static function create(
        $name,
        array $attributeValues = [],
        \ShopBundle\Entity\ProductAttributeType $type = null
    ) {

        if (!$type) {
            ProductAttribute::load('STRING');
        }
        $ret = new \ShopBundle\Entity\ProductAttribute;
        $ret->setName($name);
        $ret->setType($type);
        foreach ($attributeValues as $key => $value) {
            $ret->addAttributeValue($value);
        }

        return $ret;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = ucfirst(strtolower($name));

        return $this;
    }

    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    public function removeAttributeValue($attValue)
    {
        $this->attributeValues->removeElement($attValue);

        return $this;
    }

    public function addAttributeValue($attValue)
    {
        $this->attributeValues[] = $attValue;
        $attValue->setProductAttribute($this);

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @PreUpdate
     * @PrePersist
     */
    public function prePersist()
    {
        $i = 0;
        $attrs = $this->getAttributeValues();

        $this->attributeValues = new ArrayCollection;

        foreach ($attrs as $key => $value) {
            if ($value->getValue() != null) {
                $this->addAttributeValue($value);
            }
        }

        foreach ($this->getAttributeValues() as $key => $value) {
            $value->setProductAttribute($this);
            $value->setPosition($i);
            $i++;
        }
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adriá Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
class ShippingCompany extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;

    /**
     * @ManyToMany(targetEntity="ShopBundle\Entity\ShippingMethod")
     * @JoinTable(
     *     name="_shippingcompany_shippingmethods",
     *     joinColumns={
     *         @JoinColumn(
     *             name="shippingcompany_id", referencedColumnName="id"
     *         )
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(
     *             name="shippingmethod_id",
     *             referencedColumnName="id",
     *             unique=true
     *         )
     *     }
     * )
     */
    protected $shippingMethods;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'shippingMethods' => Doctrine::toArray($this->getShippingMethods()),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->shippingMethods = new ArrayCollection;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getShippingMethods()
    {
        return $this->shippingMethods;
    }

    public function removeShippingMethod($shippingMethod)
    {
        $this->shippingMethods->removeElement($shippingMethod);

        return $this;
    }

    public function addShippingMethod($shippingMethod)
    {
        $this->shippingMethods[] = $shippingMethod;

        return $this;
    }

}

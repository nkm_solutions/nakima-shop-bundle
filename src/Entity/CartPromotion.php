<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use ShopBundle\Entity\Promotion as BasePromotion;

/**
 * @MappedSuperclass
 */
class CartPromotion extends BasePromotion
{

    /**
     * @Column(type="string", length=128, unique=true, nullable=false)
     */
    protected $code;

    /**
     * @Column(type="integer", nullable=false)
     */
    protected $usages;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::__toArray($options);
        $retArray["code"] = $this->getCode();
        $retArray["maxUsages"] = $this->getUsages();

        return $retArray;
    }

    public function __toString()
    {
        $code = $this->code;
        $campaign = $this->getCampaign()->getName();

        return "$code ($campaign)";
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setUsages(1);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getUsages()
    {
        return $this->usages;
    }

    public function setUsages($usages)
    {
        $this->usages = $usages;

        return $this;
    }

}

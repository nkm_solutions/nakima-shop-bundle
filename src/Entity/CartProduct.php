<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
class CartProduct extends BaseEntity
{

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\ProductCombination")
     * @JoinColumn(name="productcombination_id", referencedColumnName="id")
     */
    protected $productCombination;

    /**
     * @Column(type="integer")
     */
    protected $quantity;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $finalPrice;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setQuantity(1);
    }

    public function __toArray(array $options = []): array
    {
        $cartProdArray = [
            'id' => $this->getId(),
            'quantity' => $this->getQuantity(),
            'finalPrice' => $this->getFinalPrice(),
            'product' => Doctrine::toArray($this->getProduct(), ['children' => false]),
        ];

        return $cartProdArray;
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product)
    {
        $this->product = $product;
        $this->updateFinalPrice();

        return $this;
    }

    public function getProductCombination()
    {
        return $this->productCombination;
    }

    public function setProductCombination($productCombination)
    {
        $this->productCombination = $productCombination;
        $this->setProduct($productCombination->getProduct());

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    public function setFinalPrice($finalPrice)
    {
        $roundedPrice = round($finalPrice, 2);
        $this->finalPrice = $roundedPrice;

        return $this;
    }

    public function updateFinalPrice()
    {
        $product = $this->getProduct();

        $basePrice = $product->getPrice();
        $productDisc = $product->getDiscount();
        $discountAbs = 0;
        if ($productDisc) {
            $discountAbs = $productDisc->applyDiscount($basePrice);
        }

        $priceWithDiscount = $basePrice;
        if ($discountAbs > 0) {
            $priceWithDiscount -= $discountAbs;
        }

        $this->setFinalPrice($priceWithDiscount);
    }

}

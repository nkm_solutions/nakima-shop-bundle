<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass
 */
class Wishlist extends BaseEntity
{

    /**
     * @Column(type="string", length=128)
     */
    protected $name;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @ManyToMany(targetEntity="ShopBundle\Entity\Product")
     * @JoinTable(
     *     name="_wishlists_products",
     *     joinColumns={
     *         @JoinColumn(name="wishlist_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="product_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $products;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'products' => Doctrine::toArray($this->getProducts()),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->products = new ArrayCollection;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function addProduct($product)
    {
        $this->products[] = $product;

        return $this;
    }

    public function removeProduct($product)
    {
        $this->products->removeElement($product);

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\Utils\Time\DateTime;

/**
 * @MappedSuperclass
 */
class ShoppingOrder extends BaseEntity
{

    /**
     * @Column(type="nakima_datetime")
     */
    protected $createdAt;

    public function _construct()
    {
        $this->setCreatedAt(new DateTime);
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

}

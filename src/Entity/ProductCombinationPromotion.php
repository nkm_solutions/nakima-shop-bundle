<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Utils\Doctrine;
use ShopBundle\Entity\Promotion as BasePromotion;

/**
 * @MappedSuperclass
 */
class ProductCombinationPromotion extends BasePromotion
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ProductCombination",
     *     inversedBy="promotions"
     * )
     * @JoinColumn(
     *     name="productcombination_id",
     *     referencedColumnName="id"
     * )
     */
    protected $productCombination;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::toArray($options);
        $retArray["productCombination"] = Doctrine::toArray(
            $this->getProductCombination(),
            ['children' => false]
        );

        return $retArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductCombination()
    {
        return $this->productCombination;
    }

    public function setProductCombination($productCombination)
    {
        $this->productCombination = $productCombination;

        return $this;
    }

}

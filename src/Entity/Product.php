<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\Table;
use Nakima\CoreBundle\Entity\BaseStatusSlugEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;

/**
 * @MappedSuperclass(
 *     repositoryClass="Nakima\ShopBundle\Repository\ProductRepository"
 * )
 * @Table(
 *     indexes={
 *         @Index(
 *             name="search_prod_idx",
 *             columns={"name", "description", "summary"},
 *             flags={"fulltext"}
 *         )
 *     }
 * )
 * @HasLifecycleCallbacks
 */
class Product extends BaseStatusSlugEntity
{
    /**
     * @Column(type="string", length=128, nullable=false)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @OneToOne(targetEntity="MediaBundle\Entity\Gallery", cascade={"persist"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    protected $gallery;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @Column(type="text")
     */
    protected $summary;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Discount",
     *     cascade={"persist"}
     * )
     * @JoinColumn(
     *     name="discount_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $discount;

    /**
     * @ManyToMany(
     *     targetEntity="ShopBundle\Entity\ProductCategory",
     *     cascade={"all"}
     * )
     * @JoinTable(
     *     name="_products_categories",
     *     joinColumns={
     *         @JoinColumn(name="product_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="category_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $categories;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\ProductCategory")
     * @JoinColumn(referencedColumnName="id", nullable=false)
     */
    protected $mainCategory;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Brand",
     *     inversedBy="products"
     * )
     * @JoinColumn(name="brand_id", referencedColumnName="id", nullable=false)
     */
    protected $brand;

    /**
     * @ManyToMany(
     *     targetEntity="ShopBundle\Entity\Shop",
     *     inversedBy="products",
     *     cascade={"all"}
     * )
     * @JoinTable(name="_shops_products")
     */
    protected $shops;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $price;

    /**
     * @ManyToMany(targetEntity="ShopBundle\Entity\ProductAttribute")
     * @JoinTable(
     *     name="_products_productattributes",
     *     joinColumns={
     *         @JoinColumn(name="product_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="attribute_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $attributes;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductCombination",
     *     mappedBy="product",
     *     cascade={"all"}
     * )
     */
    protected $combinations;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $sellable;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $hto;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $sto;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\ProductPromotion",
     *     mappedBy="product"
     * )
     */
    protected $promotions;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $saleDiscount;

    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $featured;

    /**
     * @ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    protected $seller;

    /**
     * @Column(type="float", nullable=false)
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $finalPrice;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $relevanceOrder;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();

        $this->categories = new ArrayCollection;
        $this->attributes = new ArrayCollection;
        $this->shops = new ArrayCollection;
        $this->promotions = new ArrayCollection;

        $this->setDiscount(new \ShopBundle\Entity\Discount);
        $this->setGallery(new \MediaBundle\Entity\Gallery);

        $this->setEnabled(false);
        $this->setHto(false);
        $this->setSto(false);

        $this->setFeatured(false);
        $this->setFinalPrice(0);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        $prodArray = [
            'id'             => $this->getId(),
            'name'           => $this->getName(),
            'slug'           => $this->getSlug(),
            'description'    => $this->getDescription(),
            'summary'        => $this->getSummary(),
            'sellable'       => $this->getSellable(),
            'enabled'        => $this->getEnabled(),
            'price'          => $this->getPrice(),
            'saleDiscount'   => $this->getSaleDiscount(),
            'featured'       => $this->getFeatured(),
            'finalPrice'     => $this->getFinalPrice(),
            'relevanceOrder' => $this->getRelevanceOrder(),
        ];

        if ($options['children'] ?? true) {
            $prodArray['gallery'] = Doctrine::toArray($this->getGallery());
            $prodArray['brand'] = Doctrine::toArray($this->getBrand());
            $prodArray['categories'] = Doctrine::toArray($this->getCategories());
            $prodArray['attributes'] = Doctrine::toArray($this->getAttributes());
            $prodArray['combinations'] = Doctrine::toArray($this->getCombinations());
            $prodArray['promotions'] = Doctrine::toArray($this->getPromotions());
        }

        return $prodArray;
    }

    /**************************************************************************
     *                                                                        *
     *   LifecycleCallbacks                                                   *
     *                                                                        *
     **************************************************************************/

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        parent::preCreate();
        if (!$this->gallery) {
            $this->gallery = new \MediaBundle\Entity\Gallery;
        }

        if (!$this->discount) {
            $this->setDiscount(new \ShopBundle\Entity\Discount);
        }

        $this->computeSellable();
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if ($name !== null) {
            $this->name = $name;
        }

        return $this;
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        $this->updateSaleDiscount();

        return $this;
    }

    public function setupDiscount($amount, $isAbsolute = true)
    {
        $discount = $this->getDiscount();
        if (!$discount) {
            $discount = new \ShopBundle\Entity\Discount;
            $this->setDiscount($discount);
        }
        $discount->setAbsolute($isAbsolute);
        $discount->setDiscount($amount);
        $this->updateSaleDiscount();

        return $this;
    }

    public function getCombinations()
    {
        return $this->combinations;
    }

    public function addCombination($combination)
    {
        $this->combinations[] = $combination;

        return $this;
    }

    public function removeCombination($combination)
    {
        $this->combinations->removeElement($combination);

        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function addCategory($category, $child = false)
    {
        if (!$category) {
            return;
        }

        if ($this->categories->contains($category)) {
            return;
        }

        $this->categories[] = $category;

        $this->addCategory($category->getParent());

        if ($child) {
            foreach ($category->getSubcategories() as $key => $value) {
                $this->addCategory($value, true);
            }
        }

        if (!$this->mainCategory) {
            $this->setMainCategory($category);
        }

        return $this;
    }

    public function removeCategory($category)
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function addAttribute($attribute)
    {
        if (count($this->getCombinations()) === 0) {
            $this->attributes[] = $attribute;
        }

        return $this;
    }

    public function removeAttribute($attribute)
    {
        if (count($this->getCombinations()) === 0) {
            $this->attributes->removeElement($attribute);
        }

        return $this;
    }

    public function getShops()
    {
        return $this->shops;
    }

    public function addShop(\ShopBundle\Entity\Shop $shop)
    {
        if (!$this->shops->contains($shop)) {
            $shop->addProduct($this);
            $this->shops[] = $shop;
            $this->computeSellable();
        }

        return $this;
    }

    public function removeShop(\ShopBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
        $this->computeSellable();
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        $this->updateSaleDiscount();

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        $this->computeSellable();

        return $this;
    }

    public function getSellable()
    {
        return $this->sellable;
    }

    public function setSellable($sellable)
    {
        $this->sellable = $sellable;

        return $this;
    }

    public function computeSellable()
    {
        // Check Product
        $prod = $this->getEnabled();
        if (!$prod) {
            $this->setSellable(false);

            return false;
        }
        // Check Brand
        $brand = $this->getBrand();
        if (!$brand) {
            $this->setSellable(false);

            return false;
        }
        if (!$brand->getEnabled()) {
            $this->setSellable(false);

            return false;
        }
        // Check Shops
        $shops = $this->getShops();
        if (!$shops) {
            $this->setSellable(false);

            return false;
        }
        $isOneShopEnabled = false;
        foreach ($shops as $shop) {
            if ($shop->getEnabled()) {
                $isOneShopEnabled = true;
                break;
            }
        }
        $this->setSellable($isOneShopEnabled);

        return $isOneShopEnabled;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        $this->computeSellable();

        return $this;
    }

    public function getMainCategory()
    {
        return $this->mainCategory;
    }

    public function setMainCategory($mainCategory)
    {
        $this->mainCategory = $mainCategory;
        $this->addCategory($mainCategory, true);

        return $this;
    }

    public function getHto()
    {
        return $this->hto;
    }

    public function setHto($hto)
    {
        $this->hto = $hto;

        return $this;
    }

    public function getSto()
    {
        return $this->sto;
    }

    public function setSto($sto)
    {
        if ($sto !== null) {
            $this->sto = $sto;
        }

        return $this;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function addPromotion($promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    public function removePromotion($promotion)
    {
        $this->promotions->removeElement($promotion);

        return $this;
    }

    public function getSaleDiscount()
    {
        return $this->saleDiscount;
    }

    public function setSaleDiscount($saleDiscount)
    {
        $this->saleDiscount = $saleDiscount;
        $this->updateFinalPrice();

        return $this;
    }

    public function updateSaleDiscount()
    {
        $basePrice = $this->getPrice();
        $discount = $this->getDiscount();
        if ($discount) {
            $this->setSaleDiscount($discount->applyDiscount($basePrice));
        } else {
            $this->setSaleDiscount(0.0);
        }

        return $this;
    }

    public function getFeatured()
    {
        return $this->featured;
    }

    public function setFeatured($featured)
    {
        if ($this->seller) {
            $this->seller->decreaseFeatured();
            $this->featured = false;
            $this->seller = null;
        }
        if ($featured) {
            $user = Symfony::getUser();
            $this->featured = $featured;
            $this->seller = $user;
            if ($user->getFeatured() < 15) {
                $this->seller->increaseFeatured();
            } else {
                return false;
            }
        }

        return $this;
    }

    public function getSeller()
    {
        return $this->seller;
    }

    public function setSeller($seller)
    {
        if ($seller !== null) {
            $this->seller = $seller;
        }

        return $this;
    }

    /**
     * @PreUpdate
     */
    public function prePersist()
    {
        $this->computeSellable();
    }

    public function getRelevanceOrder(): ?int
    {
        return $this->relevanceOrder;
    }

    public function setRelevanceOrder(int $relevanceOrder)
    {
        $this->relevanceOrder = $relevanceOrder;
    }

    public function getFinalPrice(): ?float
    {
        return $this->finalPrice;
    }

    public function setFinalPrice(float $finalPrice)
    {
        $this->finalPrice = $finalPrice;
    }

    public function updateFinalPrice()
    {
        $this->setFinalPrice($this->getPrice() - $this->getSaleDiscount());
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adriá Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseStatusEntity;

/**
 * @MappedSuperclass
 */
class OrderStatus extends BaseStatusEntity
{

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $lifeCycle;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct($name, $lifeCycle)
    {
        parent::__construct($name);
        $this->setLifeCycle($lifeCycle);
    }

    public function __toArray(array $options = []): array
    {
        $orderStatusArray = parent::__toArray($options = []);
        $orderStatusArray['lifeCycle'] = $this->getLifeCycle();

        return $orderStatusArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getLifeCycle()
    {
        return $this->lifeCycle;
    }

    public function setLifeCycle($lifeCycle)
    {
        $this->lifeCycle = $lifeCycle;

        return $this;
    }

}

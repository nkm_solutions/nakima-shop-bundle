<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use UserBundle\Entity\Role;

/**
 * @MappedSuperclass
 */
class ShopManager extends \ShopBundle\Entity\Customer
{

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Shop",
     *     mappedBy="shopManager"
     * )
     */
    protected $shop;

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->addRole(Role::loadRole('ROLE_SHOP_SHOP_MANAGER'));
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function setShop($shop)
    {
        $this->shop = $shop;

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use AddressBundle\Entity\Address;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\Role;
use UserBundle\Entity\User;

/**
 * @MappedSuperclass
 */
class Customer extends User
{

    /**
     * @Column(type="string", length=50)
     * @Assert\Length(min=1,max=50)
     * @Assert\NotNull()
     */
    protected $name;

    /**
     * @Column(type="string", length=50)
     * @Assert\Length(min=1,max=50)
     * @Assert\NotNull()
     */
    protected $surnames;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\Cart",
     *     mappedBy="customer",
     *     cascade={"persist"}
     * )
     */
    protected $cart;

    /**
     * @OneToOne(
     *     targetEntity="ShopBundle\Entity\HTOCart",
     *     mappedBy="customer",
     *     cascade={"persist"}
     * )
     */
    protected $htoCart;

    /**
     * @Column(type="integer")
     * @Assert\Range(
     *     min = 4,
     *     max = 10,
     *     minMessage = "You always can add at least {{ limit }} products",
     *     maxMessage = "You cannot have more than {{ limit }} products"
     * )
     */
    protected $htoCartMaxProd;

    /**
     * @ManyToMany(
     *     targetEntity="ShopBundle\Entity\Wishlist",
     *     cascade={"all"}
     * )
     * @JoinTable(
     *     name="_customers_wishlists",
     *     joinColumns={
     *         @JoinColumn(
     *             name="customer_id",
     *             referencedColumnName="id"
     *         )
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(
     *             name="wishlist_id",
     *             referencedColumnName="id",
     *             unique=true
     *         )
     *     }
     * )
     */
    protected $wishlists;

    /**
     * OneToMany Relation writted as a ManyToMany with a unique column.
     *
     * @ManyToMany(
     *     targetEntity="AddressBundle\Entity\Address",
     *     cascade={"all"}
     * )
     * @JoinTable(
     *     name="_customers_shippingaddresses",
     *     joinColumns={
     *         @JoinColumn(name="customer_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(
     *             name="address_id",
     *             referencedColumnName="id",
     *             unique=true,
     *             onDelete="CASCADE"
     *         )
     *     }
     * )
     */
    protected $addresses;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->addRole(Role::loadRole('ROLE_SHOP_CUSTOMER'));
        $this->setCart(new \ShopBundle\Entity\Cart);
        $this->setHtoCart(new \ShopBundle\Entity\HTOCart);
        $this->addresses = new ArrayCollection;
        $this->setHtoCartMaxProd(4);

        $this->wishlists = new ArrayCollection();
        $wishlist = new \ShopBundle\Entity\Wishlist;
        $wishlist->setName("Default");
        $wishlist->setDescription("Default Wishlist");
        $this->addWishlist($wishlist);

        $this->setGender(\UserBundle\Entity\Gender::load("MALE"));
    }

    public function __toArray(array $options = []): array
    {
        $custArray = [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'gender' => $this->getGender()->getName(),
            'name' => $this->getName(),
            'surnames' => $this->getSurnames(),
            'cart' => $this->getCart(),
            'htoCartMaxProd' => $this->getHtoCartMaxProd(),
        ];

        if ($options['children'] ?? true) {
            $custArray['wishlists'] = Doctrine::toArray($this->getWishlists());
            $custArray['addresses'] = Doctrine::toArray($this->getAddresses());
        }

        return $custArray;
    }


    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        if (!$name) {
            return;
        }
        $this->name = $name;

        return $this;
    }

    public function getSurnames()
    {
        return $this->surnames;
    }

    public function setSurnames($surnames)
    {
        if (!$surnames) {
            return;
        }
        $this->surnames = $surnames;

        return $this;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function setCart($cart)
    {
        $cart->setCustomer($this);
        $this->cart = $cart;

        return $this;
    }

    public function getWishlists()
    {
        return $this->wishlists;
    }

    public function addWishlist($wishlist)
    {
        $this->wishlists[] = $wishlist;

        return $this;
    }

    public function removeWishlist($wishlist)
    {
        $this->wishlists->removeElement($wishlist);

        return $this;
    }

    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    public function getAddresses()
    {
        return $this->addresses;
    }

    public function getHtoCart(): HTOCart
    {
        return $this->htoCart;
    }

    public function setHtoCart($htoCart)
    {
        $htoCart->setCustomer($this);
        $this->htoCart = $htoCart;

        return $this;
    }

    public function getHtoCartMaxProd()
    {
        return $this->htoCartMaxProd;
    }

    public function setHtoCartMaxProd($htoCartMaxProd)
    {
        $this->htoCartMaxProd = $htoCartMaxProd;

        return $this;
    }

}

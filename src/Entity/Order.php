<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adriá Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PostPersist;
use Doctrine\ORM\Mapping\PostUpdate;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\PaymentBundle\Entity\Stripe;
use Nakima\Utils\Time\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks
 */
class Order extends BaseEntity
{

    /**
     * @Column(type="nakima_datetime")
     */
    protected $createdAt;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\OrderProduct",
     *     mappedBy="order",
     *     cascade={"all"}
     * )
     */
    protected $orderProducts;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Customer"
     * )
     * @JoinColumn(nullable=false)
     */
    protected $customer;

    /**
     * @Column(type="float")
     */
    protected $price;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $ref;

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $trackingNumber;

    /**
     * @ManyToOne(
     *     targetEntity="PaymentBundle\Entity\Stripe",
     *     inversedBy="orders",
     *     cascade={"all"}
     * )
     * @JoinColumn(
     *     name="stripe_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $stripe;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\OrderStatus")
     */
    protected $status;

    /**
     * @ManyToOne(
     *     targetEntity="AddressBundle\Entity\Address",
     *     cascade={"all"}
     * )
     * @JoinColumn(
     *     nullable=false
     * )
     */
    protected $billAddress;

    /**
     * @ManyToOne(
     *     targetEntity="AddressBundle\Entity\Address",
     *     cascade={"all"}
     * )
     * @JoinColumn(
     *     nullable=false
     * )
     */
    protected $shippingAddress;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\Brand")
     */
    protected $brand;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $isHto;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\ShippingMethod"
     * )
     * @JoinColumn(
     *     name="shippingmethod_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $shippingMethod;

    /**
     * @Column(type="float", nullable=false)
     */
    protected $shippingPrice;

    /**
     * @Column(type="nakima_datetime", nullable=false)
     */
    protected $lastUpdated;

    /**
     * @Column(type="nakima_datetime", nullable=true)
     */
    protected $deliveryDate;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $inProgress;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $finished;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $redundant;

    /**
     * @Column(type="string", length=128, nullable=true)
     */
    protected $promotionCode;

    /**
     * @Column(type="float", nullable=true)
     */
    protected $promotionDiscount;


    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __toString()
    {
        $ord = $this->getOrderNumber();

        return "[$ord] $this->ref";
    }

    public function __construct()
    {
        parent::__construct();
        $this->setPrice(0);
        $this->setCustomer(Symfony::getUser());
        $this->orderProducts = new ArrayCollection;
        $this->setStatus(\ShopBundle\Entity\OrderStatus::load("PENDING"));
        $this->setCreatedAt(new DateTime);
        $this->setLastUpdated(new DateTime);
        $this->setIsHto(false);
        $this->setInProgress(true);
        $this->setFinished(false);
        $this->setRedundant(false);
        $this->setShippingPrice(0);
    }

    public function __toArray(array $options = []): array
    {

        if ($options['csv'] ?? false) {

            $products = [];
            foreach ($this->getOrderProducts() as $key => $value) {
                $products[] = $value->getOriginalProduct()->getName();
            }

            return [
                "SH-$this->ref",
                "$this->brand by Showea",
                "$this->brand",
                "$this->brand",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                $this->customer->getName(),
                $this->customer->getSurnames(),
                "",
                $this->getShippingAddress()->getLine(),
                "",
                $this->getShippingAddress()->getZip(),
                $this->getShippingAddress()->getCity(),
                $this->getShippingAddress()->getCountry()->getCode(),
                $this->getShippingAddress()->getCountry()->getCode(),
                $this->getShippingAddress()->getContactNumber(),
                $this->customer->getEmail(),
                "",
                join(", ", $products),
                "",
                "",
                "",
                "",
                "",
            ];
        }

        $orderArray = [
            'id' => $this->getId(),
            'number' => $this->getNumber(),
            'createdAt' => $this->getCreatedAt()->__toString(),
            'shippingMethod' => Doctrine::toArray($this->getShippingMethod()),
        ];

        if ($options['children'] ?? true) {
            $orderArray['orderProducts'] = Doctrine::toArray($this->getOrderProducts());
            $orderArray['customer'] = Doctrine::toArray($this->getCustomer());
            $orderArray['payment'] = Doctrine::toArray($this->getStripe());
            $orderArray['status'] = Doctrine::toArray($this->getStatus());
        }

        return $orderArray;
    }

    /**
     * @PostPersist
     */
    public function postPersist()
    {

        Mailer::newInstance(
            "Aviso sobre pedido realizado",
            "no-reply@showea.com",
            $this->getCustomer()->getEmail(),
            'NakimaShopBundle:Emails:new_order_customer.html.twig',
            [
                'target' => $this->getCustomer(),
                'order' => $this,
                'new' => true,
            ]
        );

        /*Mailer::newInstance(
            "Aviso sobre pedido realizado",
            "no-reply@showea.com",
            $this->email, // <--
            'NakimaShopBundle:Emails:new_order_seller.html.twig',
            [
                'target' => $this->getBrand()->getProvider(),
                'order' => $this,
                'new' => true
            ]
        );*/

        $admins = Doctrine::getRepo("ShopBundle:Admin")->findAll();

        foreach ($admins as $key => $value) {
            Mailer::newInstance(
                "Aviso sobre pedido realizado",
                "no-reply@showea.com",
                $value->getEmail(),
                'NakimaShopBundle:Emails:new_order_admin.html.twig',
                [
                    'target' => $this->getCustomer(),
                    'order' => $this,
                    'new' => true,
                ]
            );
        }

        Mailer::send();
    }

    /**
     * @PostUpdate
     */
    public function PostUpdate()
    {
    }

    public function getNumber()
    {
        if ($this->getId()) {
            return $this->getId() + 79284;
        }

        return -1;
    }

    public function pay($token)
    {
        $stripe = \Nakima\PaymentBundle\Entity\Stripe::create();
    }

    public static function create($cart, $ref, $billAddress, $shippingAddress, $isHto = false)
    {

        $user = Symfony::getUser();
        $shippingMethod = $cart->getShippingMethod();
        if (!$shippingMethod) {
            $shippingMethod = \ShopBundle\Entity\ShippingMethod::load('Standard');
        }
        $shippingPrice = $cart->getShippingPrice();

        $cartPromotion = false;
        $cartPromotionDiscount = 0.0;
        if (!$isHto) {
            $cartPromotion = $cart->getPromotion();
            $cartPromotionDiscount = $cart->getPromotionDiscount();
        }

        $brandMapping = [];

        foreach ($cart->getCartProducts() as $cartProduct) {
            $product = $cartProduct->getProduct();

            $brand = $product->getBrand();
            $brandId = $brand->getId();

            if (!array_key_exists($brandId, $brandMapping)) {
                $brandMapping[$brandId] = new \ShopBundle\Entity\Order;
                $brandMapping[$brandId]->setCustomer($user);
                $brandMapping[$brandId]->setBillAddress($billAddress);
                $brandMapping[$brandId]->setShippingAddress($shippingAddress);
                $brandMapping[$brandId]->setShippingMethod($shippingMethod);
                $brandMapping[$brandId]->setShippingPrice($shippingPrice);
                $brandMapping[$brandId]->setRef($ref);
                $brandMapping[$brandId]->setIsHto($isHto);
                $brandMapping[$brandId]->setBrand($brand);
                $brandMapping[$brandId]->setPrice(0);
                $brandMapping[$brandId]->setPromotionDiscount($cartPromotionDiscount);
                if ($cartPromotion) {
                    $brandMapping[$brandId]->setPromotionCode($cartPromotion->getCode());
                }
            }

            // Éste create YA ACTUALIZA el proceio de la Brand !!!
            $orderProduct = \ShopBundle\Entity\OrderProduct::create(
                $cartProduct,
                $brandMapping[$brandId]
            );
        }

        return $brandMapping;
    }

    public function getOrderNumber()
    {
        if ($this->getId()) {
            return $this->getId() + 79284;
        }

        return -1;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
    }

    public function get($field)
    {
        if ($field == "orderNumber") {
            return $this->getOrderNumber();
        }

        return parent::get($field);
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function addOrderProduct(
        \ShopBundle\Entity\OrderProduct $orderProduct
    ) {
        $this->orderProducts[] = $orderProduct;

        $orderProductPrice = $orderProduct->getFinalPrice();
        $orderProductQuant = $orderProduct->getQuantity();
        $this->price += $orderProductPrice * $orderProductQuant;

        return $this;
    }

    public function removeOrderProduct(
        \ShopBundle\Entity\OrderProduct $orderProduct
    ) {
        $this->orderProducts->removeElement($orderProduct);
    }

    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    public function setCustomer(\ShopBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): OrderStatus
    {
        return $this->status;
    }

    public function setStatus($status)
    {

        $this->oldStatus = $this->status;
        $this->status = $status;

        $this->setLastUpdated(new DateTime);

        if ($status->getName() == "PREPARING" || $status->getName() == "IN TRANSIT") {
            Mailer::newInstance(
                "Modificación sobre el pedido realizado",
                "no-reply@showea.com",
                $this->getCustomer()->getEmail(),
                'NakimaShopBundle:Emails:new_order_customer.html.twig',
                [
                    'target' => $this->getCustomer(),
                    'order' => $this,
                    'new' => false,
                ]
            );

            $provider = $this->getBrand()->getProvider();
            Mailer::newInstance(
                "Modificación sobre el pedido realizado",
                "no-reply@showea.com",
                $provider->getEmail(),
                'NakimaShopBundle:Emails:new_order_seller.html.twig',
                [
                    'target' => $this->getBrand()->getProvider(),
                    'order' => $this,
                    'new' => false,
                ]
            );

            $admins = Doctrine::getRepo("ShopBundle:Admin")->findAll();

            foreach ($admins as $key => $value) {
                Mailer::newInstance(
                    "Modificación sobre el pedido realizado",
                    "no-reply@showea.com",
                    $value->getEmail(),
                    'NakimaShopBundle:Emails:new_order_admin.html.twig',
                    [
                        'target' => $this->getCustomer(),
                        'order' => $this,
                        'new' => false,
                    ]
                );
            }

            Mailer::send();
        }

        return $this;
    }

    public function getBillAddress()
    {
        return $this->billAddress;
    }

    public function setBillAddress($billAddress)
    {
        $this->billAddress = $billAddress;

        return $this;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getStripe(): Stripe
    {
        return $this->stripe;
    }

    public function setStripe($stripe)
    {
        $this->stripe = $stripe;

        return $this;
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    public function getIsHto()
    {
        return $this->isHto;
    }

    public function setIsHto($isHto)
    {
        $this->isHto = $isHto;

        return $this;
    }

    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    public function getInProgress()
    {
        return $this->inProgress;
    }

    public function setInProgress($inProgress)
    {
        $this->inProgress = $inProgress;

        return $this;
    }

    public function getFinished()
    {
        return $this->finished;
    }

    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    public function getRedundant()
    {
        return $this->redundant;
    }

    public function setRedundant($redundant)
    {
        $this->redundant = $redundant;

        return $this;
    }

    public function getPromotionCode()
    {
        return $this->promotionCode;
    }

    public function setPromotionCode($promotionCode)
    {
        $this->promotionCode = $promotionCode;

        return $this;
    }

    public function getPromotionDiscount()
    {
        return $this->promotionDiscount;
    }

    public function setPromotionDiscount($promotionDiscount)
    {
        $this->promotionDiscount = $promotionDiscount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    /**
     * @param mixed $shippingPrice
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;
        return $this;
    }

}

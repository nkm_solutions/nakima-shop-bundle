<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Mailer\Mailer;
use Nakima\CoreBundle\Utils\Doctrine;
use UserBundle\Entity\Role;

/**
 * @MappedSuperclass
 */
class Provider extends \ShopBundle\Entity\Customer
{

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\Shop",
     *     mappedBy="provider"
     * )
     */
    protected $shops;

    /**
     * @OneToMany(
     *     targetEntity="ShopBundle\Entity\Brand",
     *     mappedBy="provider"
     * )
     */
    protected $brands;

    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();

        $this->shops = new ArrayCollection;
        $this->brands = new ArrayCollection;
        $this->avatar = null;
        $this->addRole(Role::loadRole('ROLE_SHOP_PROVIDER'));
    }

    public function __postPersist()
    {

        Mailer::newInstance(
            "Nueva cuenta registrada",
            "no-reply@showea.com",
            $this->email,
            'NakimaShopBundle:Emails:registration_seller.html.twig',
            ['target' => $this]
        );

        $admins = Doctrine::getRepo("ShopBundle:Admin")->findAll();

        foreach ($admins as $key => $value) {
            Mailer::newInstance(
                "Nueva cuenta registrada",
                "no-reply@showea.com",
                $value->getEmail(),
                'NakimaShopBundle:Emails:registration_seller2.html.twig',
                ['target' => $this]
            );
        }

        Mailer::send();
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function addShop(\ShopBundle\Entity\Shop $shop)
    {
        if (!$shop) {
            return;
        }
        if ($this->shops->contains($shop)) {
            return;
        }
        $this->shops[] = $shop;
        $shop->setProvider($this);

        return $this;
    }

    public function removeShop(\ShopBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
    }

    public function getShops()
    {
        return $this->shops;
    }

    public function addBrand(\ShopBundle\Entity\Brand $brand)
    {

        if (!$brand) {
            return;
        }
        if ($this->brands->contains($brand)) {
            $brand->setProvider($this);

            return;
        }
        $this->brands[] = $brand;
        $brand->setProvider($this);

        return $this;
    }

    public function removeBrand(\ShopBundle\Entity\Brand $brand)
    {
        $this->brands->removeElement($brand);
    }

    public function getBrands()
    {
        return $this->brands;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Adrià Llaudet Planas <allaudet@nakima.es>
 */

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Utils\Doctrine;
use ShopBundle\Entity\Promotion as BasePromotion;

/**
 * @MappedSuperclass
 */
class BrandPromotion extends BasePromotion
{

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Brand",
     *     inversedBy="promotions"
     * )
     * @JoinColumn(
     *     name="brand_id",
     *     referencedColumnName="id"
     * )
     */
    protected $brand;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $retArray = parent::toArray($options);
        $retArray["brand"] = Doctrine::toArray(
            $this->getBrand(),
            ['children' => false]
        );

        return $retArray;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostFlush;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Nakima\CoreBundle\Entity\BaseEntity;

/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks()
 */
class OrderProduct extends BaseEntity
{

    /**
     * @Column(type="string", length=128)
     */
    protected $name;

    /**
     * @ManyToOne(targetEntity="MediaBundle\Entity\Media", cascade={"persist"})
     * @JoinColumn(name="image_id", referencedColumnName="id")
     */
    protected $image;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @Column(type="text")
     */
    protected $summary;

    /**
     * @Column(type="integer")
     */
    protected $quantity;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Promotion"
     * )
     * @JoinColumn(name="promotion_id", referencedColumnName="id")
     */
    protected $promotion;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Shop"
     * )
     * @JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;

    /**
     * @Column(type="float")
     */
    protected $price;

    /**
     * @Column(type="float")
     */
    protected $finalPrice;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Brand"
     * )
     * @JoinColumn(
     *     name="brand_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $brand;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\Product")
     * @JoinColumn(
     *     name="originalproduct_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
    protected $originalProduct;

    /**
     * @ManyToOne(
     *     targetEntity="ShopBundle\Entity\Order",
     *     inversedBy="orderProducts"
     * )
     * @JoinColumn(
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $order;

    /**
     * @ManyToOne(targetEntity="ShopBundle\Entity\OrderStatus")
     */
    protected $status;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $isKept;

    /**
     * @Column(type="string", length=66)
     */
    protected $slug;

    /**
     * @ManyToMany(targetEntity="ShopBundle\Entity\OrderProductAttribute",cascade={"all"})
     * @JoinTable(
     *      joinColumns={@JoinColumn()},
     *      inverseJoinColumns={@JoinColumn(unique=true)}
     *      )
     */
    protected $productAttributes;

    /**************************************************************************
     *                                                                        *
     *   Custom Functions                                                     *
     *                                                                        *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setIsKept(true);
        $this->productAttributes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public static function create($cartProduct, $order)
    {
        $product = $cartProduct->getProduct();
        // Create new
        $orderProduct = new \ShopBundle\Entity\OrderProduct;
        // Set info
        $orderProduct->setOriginalProduct($product);
        $orderProduct->setName($product->getName());
        $orderProduct->setBrand($product->getBrand());
        $orderProduct->setSummary($product->getSummary());
        $orderProduct->setDescription($product->getDescription());
        $orderProduct->setSlug($product->getSlug());
        // Set price
        $orderProduct->setPrice($product->getPrice());
        $orderProduct->setFinalPrice($cartProduct->getFinalPrice());
        $orderProduct->setQuantity($cartProduct->getQuantity());

        $orderProduct->setOrder($order);
        $orderProduct->setStatus(\ShopBundle\Entity\OrderStatus::load("PENDING"));

        foreach ($cartProduct->getProductCombination()->getProductAttributeValues() as $pav) {
            $newPav = OrderProductAttribute::create($pav);
            $orderProduct->addProductAttribute($newPav);
        }

        return $orderProduct;
    }

    public function clone()
    {
        $orderProduct = new \ShopBundle\Entity\OrderProduct;
        $orderProduct->setName($this->getName());
        $orderProduct->setDescription($this->getDescription());
        $orderProduct->setSummary($this->getSummary());
        $orderProduct->setPrice($this->getPrice());
        $orderProduct->setFinalPrice($this->getFinalPrice());
        $orderProduct->setImage($this->getImage());
        $orderProduct->setPromotion($this->getPromotion());
        $orderProduct->setShop($this->getShop());
        $orderProduct->setBrand($this->getBrand());
        $orderProduct->setOriginalProduct($this->getOriginalProduct());
        $orderProduct->setQuantity($this->getQuantity());
        $orderProduct->setStatus($this->getStatus());
        $orderProduct->setIsKept($this->getIsKept());
        $orderProduct->setSlug($this->getSlug());

        foreach ($this->getProductAttributes() as $attr) {
            $orderProduct->addProductAttribute(clone $attr);
        }

        return $orderProduct;
    }

    /**************************************************************************
     *                                                                        *
     *   LifecycleCallbacks                                                   *
     *                                                                        *
     **************************************************************************/

    /**
     * @PrePersist()
     */
    public function preCreate()
    {
        if (!$this->image) {
            $this->image = new \MediaBundle\Entity\Media;
        }
    }

    /**************************************************************************
     *                                                                        *
     *   Getters & Setters                                                    *
     *                                                                        *
     **************************************************************************/

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;

        return $this;
    }

    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    public function setImage(\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setPromotion(
        \ShopBundle\Entity\Promotion $promotion = null
    ) {
        $this->promotion = $promotion;

        return $this;
    }

    public function getPromotion()
    {
        return $this->promotion;
    }

    public function setShop(\ShopBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    public function getShop()
    {
        return $this->shop;
    }

    public function setBrand(\ShopBundle\Entity\Brand $brand)
    {
        $this->brand = $brand;

        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setOriginalProduct(
        \ShopBundle\Entity\Product $originalProduct = null
    ) {
        $this->originalProduct = $originalProduct;

        return $this;
    }

    public function getOriginalProduct()
    {
        return $this->originalProduct;
    }

    public function setOrder(\ShopBundle\Entity\Order $order = null)
    {
        $this->order = $order;
        $order->addOrderProduct($this);

        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function setStatus(\ShopBundle\Entity\OrderStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getIsKept()
    {
        return $this->isKept;
    }

    public function setIsKept($isKept)
    {
        $this->isKept = $isKept;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getProductAttributes(): Collection
    {
        return $this->productAttributes;
    }

    public function addProductAttribute(\ShopBundle\Entity\OrderProductAttribute $attr): self
    {
        $this->productAttributes->add($attr);
        return $this;
    }

    public function removeProductAttribute(\ShopBundle\Entity\OrderProductAttribute $attr): self
    {
        $this->productAttributes->remove($attr);
        return $this;
    }


}

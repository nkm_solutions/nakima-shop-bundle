<?php
declare(strict_types=1);
namespace Nakima\ShopBundle\Entity;

/**
 * @author Javier González Cuadrado < xgonzalez@nakima.es >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PostFlush;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Symfony;

/**
 * @MappedSuperclass
 */
class ProductAttributeType extends BaseEntity
{

    /**
     * @Column(type="string", length=64)
     */
    protected $name;


    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toString()
    {
        return $this->name;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

    public static function create($name)
    {
        $ret = new \ShopBundle\Entity\ProductAttributeType;
        $ret->setName($name);

        return $ret;
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = ucfirst(strtolower($name));

        return $this;
    }

    public static $map = [];

    public static function load($key)
    {

        if (!isset($map[$key])) {
            $map[$key] = Symfony::getDoctrine()
                ->getRepository("ShopBundle:ProductAttributeType")
                ->findOneByName($key);
        }

        return $map[$key];
    }
}
